﻿Shader "Hidden/NightVisionShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Contrast ("Contrast", Range(0,4)) = 2
        _Brightness ("Brightness", Range(0,2)) = 1
        _Hue ("Hue", Color) = (1,1,1,1)
        _ScanlineTex ("Scanline Texture", 2D) = "white" {}
        _ScanlineMultiplier ("Scanline Multiplier", Float) = 1.0
        _ScanlineOpacity ("Scanline Opacity", Float) = 1.0
        _NoiseTex ("Noise Texture", 2D) = "white" {}
        _NoiseSpeed ("NoiseSpeed", Float) = 100
        _NoiseOpacity ("Noise Opacity", Float) = 1.0
        _OverlayTex ("Overlay Texture", 2D) = "white" {}
        _Distortion ("Distortion", Float) = 0.2
        _Quality ("Quality", Float) = 0
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag

            #include "UnityCG.cginc"

            sampler2D _MainTex;
            sampler2D _OverlayTex;
            sampler2D _ScanlineTex;
            sampler2D _NoiseTex;
            
            fixed4 _Hue;
            fixed _Contrast;
            fixed _Brightness;
            fixed _ScanlineMultiplier;
            fixed _ScanlineOpacity;
            fixed _NoiseSpeed;
            fixed _Distortion;
            fixed _NoiseOpacity;
            fixed _Quality;
            
            float2 DistortUV(float2 uv) {
                float2 lPoint = uv.xy - float2(0.5,0.5);
                float sqDist = lPoint.x*lPoint.x + lPoint.y*lPoint.y;
                float distortFact = 1.0 + sqDist * (_Distortion*sqrt(sqDist));
                
                return (lPoint * distortFact) + 0.5;
            }

            fixed4 frag (v2f_img i) : SV_Target {
                // Sample MainTex with distorted UVs
                float2 distortedUV = DistortUV(i.uv);
                fixed4 mainCol = tex2D(_MainTex, distortedUV);
                
                // Calculate Luminance
                fixed luminance = dot(mainCol.rgb, fixed3(0.299, 0.587, 0.144));
                // Add Brightness
                luminance += _Brightness;
                // Tint with Hue
                fixed4 finalColor = luminance*2 + _Hue;
                // Apply contrast 
                finalColor = pow(finalColor, _Contrast);
                
                float2 scanlineUV = float2(i.uv.x, i.uv.y*_ScanlineMultiplier + _Time.x);
                fixed4 scanlineCol = tex2D(_ScanlineTex, scanlineUV);
                
                float2 noiseUV = float2(i.uv.x + _SinTime.z * _NoiseSpeed, i.uv.y + _Time.x * _NoiseSpeed);
                fixed4 noiseTex = tex2D(_NoiseTex, noiseUV);
                
                fixed4 scanColor = finalColor * (1 - scanlineCol.a);
                finalColor = lerp(finalColor, scanColor, _ScanlineOpacity);
                
                fixed4 noiseColor = finalColor * noiseTex;
                finalColor = lerp(finalColor, noiseColor, _NoiseOpacity);
                finalColor = lerp(noiseTex * noiseTex + _Hue, finalColor, _Quality);
                
                fixed4 overlayCol = tex2D(_OverlayTex, i.uv);
                finalColor = lerp(finalColor, overlayCol, overlayCol.a);
                
                return finalColor;
            }
            ENDCG
        }
    }
}
