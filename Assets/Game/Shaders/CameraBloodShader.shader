﻿Shader "Hidden/CameraBloodShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _BloodTex ("Blood texture", 2D) = "white" {}
        _BloodNorm ("Blood normal", 2D) = "bump" {}
        _Distortion ("Blood distortion", Range(0,2)) = 1
        _BloodAmount ("Blood amount", Range(0,1)) = 0
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            sampler2D _MainTex;
            sampler2D _BloodTex;
            sampler2D _BloodNorm;
            
            float _Distortion;
            float _BloodAmount;

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the blood texture
                fixed4 bloodCol = tex2D(_BloodTex, i.uv);
                // change the alpha channel based on the blood amount variable
                bloodCol.a = saturate(bloodCol.a + (_BloodAmount*2 - 1));
                
                half2 bump = UnpackNormal(tex2D(_BloodNorm, i.uv)).xy;
                fixed4 srcCol = tex2D(_MainTex, i.uv + bump * bloodCol.a * _Distortion);
                
                // multiply the blood col with the source color, so the blood is never opaque 
                // multiply by 2 so the blood is brighter
                fixed4 overlayCol = srcCol * bloodCol * 2;
                // lerp with srcCol, and the alpha value so we can control how transparent is the blood
                overlayCol = lerp(srcCol, overlayCol, 0.85);
                
                fixed4 output = lerp(srcCol, overlayCol, bloodCol.a);
                return output;
            }
            ENDCG
        }
    }
}
