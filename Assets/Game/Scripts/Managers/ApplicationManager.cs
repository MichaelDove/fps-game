﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

[System.Serializable]
public class GameState {
    public string Key = null;
    public string Value = null;
}

public class ApplicationManager : MonoBehaviour {
    // Inspector assigned
    // a list of states for us to assign in the inspector
    [SerializeField] private List<GameState> _startingGameStates = new List<GameState>();
    
    // Private variables
    private Dictionary<string, string> _gameStateDictionary = new Dictionary<string, string>();
     
    // singleton design
    private static ApplicationManager _instance = null;

    public static ApplicationManager instance {
        get {
            if (_instance == null)
                _instance = FindObjectOfType<ApplicationManager>();
            return _instance;
        }
    }
    
    public UnityEvent OnStateChanged = new UnityEvent();

    private void Awake() {
        DontDestroyOnLoad(gameObject);

        ResetGameStates();
    }

    private void ResetGameStates() {
        _gameStateDictionary.Clear();
        
        // fill up the game state dictionary with the assigned game states
        for (int i = 0; i < _startingGameStates.Count; i++) {
            GameState gs = _startingGameStates[i];
            _gameStateDictionary[gs.Key] = gs.Value;
        }
        
        OnStateChanged.Invoke();
    }

    public bool AreStatesSet(List<GameState> requiredStates) {
        foreach (var st in requiredStates) {
            string currState = GetGameState(st.Key);
            if (string.IsNullOrEmpty(currState) || !currState.Equals(st.Value))
                return false;
        }

        return true;
    }
    
    // Public methods
    public string GetGameState(string key) {
        string result;
        _gameStateDictionary.TryGetValue(key, out result);
        return result;
    }

    public bool SetGameState(string key, string value) {
        if (key == null || value == null) return false;
        _gameStateDictionary[key] = value;
        
        OnStateChanged.Invoke();
        return true;
    }

    public void LoadMainMenu() {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene("Main Menu");
    }

    public void LoadGame() {
        SceneManager.LoadScene("The game");
    }

    public void QuitGame() {
        #if UNITY_EDITOR
            //UnityEditor.EditorApplication.isPlaying = false;
        #else 
            Application.Quit();
        #endif
    }
}
