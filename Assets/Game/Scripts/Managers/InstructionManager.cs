﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class TriggerInstruction {
    public GameState State;
    [TextArea(3,10)] public string Instruction;
}

public class InstructionManager : MonoBehaviour {
    [Header("Notification Queue")] 
    [SerializeField] private SharedTimeStringQueue _notificationQueue;
    
    [Header("Messages")]
    [SerializeField] [TextArea(3,10)] private List<string> _startupMessages = new List<string>();
    [SerializeField] private List<TriggerInstruction> _triggerInstructions = new List<TriggerInstruction>();

    private void OnEnable() {
        ApplicationManager.instance.OnStateChanged.AddListener(OnGameStateChanged);
    }

    private void OnDisable() {
        ApplicationManager.instance.OnStateChanged.RemoveListener(OnGameStateChanged);
    }

    private void Start() {
        foreach (var s in _startupMessages) {
            _notificationQueue.Enqueue(s);
        }
    }

    private void OnGameStateChanged() {
        var appMan = ApplicationManager.instance;
        foreach (var instr in _triggerInstructions) {
            if (appMan.GetGameState(instr.State.Key) == instr.State.Value) {
                _notificationQueue.Enqueue(instr.Instruction);
                _triggerInstructions.Remove(instr);
                break;
            }
        }
    }
}
