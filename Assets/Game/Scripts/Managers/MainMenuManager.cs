﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Cursor = UnityEngine.Cursor;

public class MainMenuManager : MonoBehaviour {
    // Inspector assigned
    [SerializeField] private GameObject _howToWindow;

    private void Start() {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public void LoadGame() {
        ApplicationManager.instance.LoadGame();
    }

    public void QuitGame() {
        ApplicationManager.instance.QuitGame();
    }

    public void HowToEnable(bool enable) {
        _howToWindow.SetActive(enable);
    }
}
