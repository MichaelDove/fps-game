﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class ZombieSpawnManager : MonoBehaviour {
    // Inspector assigned
    [Header("General settings")]
    [SerializeField] private bool _spawnerEnabled;
    [SerializeField] [Range(1, 20)] private int _minAmount = 10;
    [SerializeField] [Range(1f, 40f)] private float _minDistanceFromPlayer = 15f;
    
    [Header("Shared Variables")]
    [SerializeField] private SharedInt _zombieCount;
    [SerializeField] private SharedVector3 _playerPos;

    [Header("Spawning settings")] 
    [SerializeField] private Transform _parent;
    [SerializeField] private List<GameObject> _zombiePrefabs;
    [SerializeField] private List<Transform> _spawnPoints;
    
    // Internals
    private AIWaypointNetwork[] _waypointNetworks;

    private void Start() {
        _waypointNetworks = GameObject.Find("Waypoint Networks").GetComponentsInChildren<AIWaypointNetwork>();
    }

    private void Update() {
        if (!_spawnerEnabled) return;
        if (_zombieCount.Value >= _minAmount) return;

        var i = Random.Range(0, _spawnPoints.Count-1);
        var dist = Vector3.Distance(_playerPos.Value, _spawnPoints[i].position);
        while (dist < _minDistanceFromPlayer) {
            i = Random.Range(0, _spawnPoints.Count-1);
            dist = Vector3.Distance(_playerPos.Value, _spawnPoints[i].position);
        }
        var zombieType = Random.Range(0, _zombiePrefabs.Count);
        // create zombie and assign its parent
        var zombie = Instantiate(_zombiePrefabs[zombieType], _spawnPoints[i]);
        zombie.transform.parent = _parent;

        // apply a waypoint network to the zombie
        var network = _waypointNetworks[Random.Range(0, _waypointNetworks.Length - 1)];
        zombie.GetComponentInChildren<AIZombieStateMachine>().WaypointNetwork = network;
    }
}
