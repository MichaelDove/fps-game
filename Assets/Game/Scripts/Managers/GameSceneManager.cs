﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo {
    public Collider Collider;
    public CharacterManager CharacterManager;
    public Camera Camera;
    public CapsuleCollider MeleeTrigger;
}

public class GameSceneManager : MonoBehaviour {
    // Inspector assigned
    [SerializeField] private ParticleSystem _bloodParticles;
    
    // Statics
    private static GameSceneManager _instance = null; 
    // a static instance that every script can access
    public static GameSceneManager instance {
        get {
            if (_instance == null)
                _instance = FindObjectOfType<GameSceneManager>();
            return _instance;
        }
    }
    
    // Private
    // a dictionary of all state machines, that we can access with the unity's intance ID
    private Dictionary<int, AIStateMachine> _stateMachines = new Dictionary<int, AIStateMachine>();
    // a dictionary with information about all the players (currently about only one player of course)
    private Dictionary<int, PlayerInfo> _playerInfos = new Dictionary<int, PlayerInfo>();
    // a dictionary of all the interactive items
    private Dictionary<int, InteractiveItem> _interactiveItems = new Dictionary<int, InteractiveItem>();
    // a dictionary of all the material controllers
    private Dictionary<int, MaterialController> _materialControllers = new Dictionary<int, MaterialController>();

    // Public
    public ParticleSystem BloodParticles => _bloodParticles;

    // stores the state machine in the dictionary with the passed key
    public void RegisterStateMachine(int key, AIStateMachine stateMachine) {
        if (!_stateMachines.ContainsKey(key)) {
            _stateMachines[key] = stateMachine;
        }
    }

    // return the state machine, with the passed key
    public AIStateMachine GetAIStateMachine(int key) {
        if (_stateMachines.ContainsKey(key)) {
            return _stateMachines[key];
        }

        return null;
    }
    
    // stores the player info in the dictionary with the passed key
    public void RegisterPlayerInfo(int key, PlayerInfo pi) {
        if (!_playerInfos.ContainsKey(key)) {
            _playerInfos[key] = pi;
        }
    }

    // return the player, with the passed key
    public PlayerInfo GetPlayerInfo(int key) {
        if (_playerInfos.ContainsKey(key)) {
            return _playerInfos[key];
        }

        return null;
    }

    public void RegisterInteractiveItem(int key, InteractiveItem ii) { 
        if (!_interactiveItems.ContainsKey(key)) {
            _interactiveItems[key] = ii;
        }
    }

    public InteractiveItem GetInteractiveItem(int key) {
        InteractiveItem item;
        _interactiveItems.TryGetValue(key, out item);
        return item;
    }

    public void RegisterMaterialController(int key, MaterialController mc) {
        if (!_materialControllers.ContainsKey(key)) {
            _materialControllers[key] = mc;
        }
    }

    public MaterialController GetMaterialController(int key) {
        MaterialController mc;
        _materialControllers.TryGetValue(key, out mc);
        return mc;
    }

    protected void OnDestroy() {
        foreach (var matCon in _materialControllers) {
            matCon.Value.OnReset();
        }
    }
}
