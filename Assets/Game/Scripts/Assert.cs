using System;
using System.Diagnostics;

namespace Game.Scripts {
    public static class Assert {
        public static void IsTrue(bool condition, string message) {
            if (!condition) {
                throw new Exception(message);
            }
        }
        
        public static void NotNull<T>(T objectToCheck, string message) where T : class {
            if (objectToCheck == null) {
                throw new NullReferenceException(message);
            }
        }
    }
}