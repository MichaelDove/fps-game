﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestruct : MonoBehaviour {
    [SerializeField] private float _time = 10;

    private void Awake() {
        Destroy(gameObject, _time);
    }
}
