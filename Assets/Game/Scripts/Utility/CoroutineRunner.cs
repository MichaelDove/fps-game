﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineRunner : MonoBehaviour {
    public static CoroutineRunner instance = null;

    private void Awake() {
        instance = this;
    }
}
