﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabSpawner : MonoBehaviour {
    // Inspector assigned
    [SerializeField] private GameObject _prefab;
    [SerializeField] private List<Transform> _spawnPoints = new List<Transform>();

    private void Awake() {
        Transform spawnPoint = _spawnPoints[Random.Range(0, _spawnPoints.Count)];
        Instantiate(_prefab, spawnPoint.position, spawnPoint.rotation);
    }
}
