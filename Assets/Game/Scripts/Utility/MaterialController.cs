﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MaterialController {
    // Inspector assigned
    [SerializeField] protected Material _material;
    [SerializeField] protected Texture _diffuseTexture;
    [SerializeField] protected Color _diffuseColor = Color.white;
    [SerializeField] protected Texture _normalMap;
    [SerializeField] protected float _normalStrength = 1.0f;

    [SerializeField] protected Texture _emissiveTexture;
    [SerializeField] protected Color _emissiveColor = Color.black;
    [SerializeField] protected float _emissionScale = 1.0f;
    
    
    // Private / Protected
    protected MaterialController _backup;
    protected bool _started;
    
    // Getters/Setters
    public Material Material => _material;
    public Color EmissiveColor => _emissiveColor;
    public float EmissionScale => _emissionScale;

    public void OnStart() {
        if (_started) return;
        _started = true;
        
        // backup settings
        _backup = new MaterialController();
        
        _backup._diffuseColor = _material.GetColor("_Color");
        _backup._diffuseTexture = _material.GetTexture("_MainTex");
        _backup._emissiveColor = _material.GetColor("_EmissionColor");
        _backup._emissionScale = 1.0f;
        _backup._emissiveTexture = _material.GetTexture("_EmissionMap");
        _backup._normalMap = _material.GetTexture("_BumpMap");
        _backup._normalStrength = _material.GetFloat("_BumpScale");

        GameSceneManager.instance.RegisterMaterialController(_material.GetInstanceID(), this);
    }

    public void Activate(bool activate) {
        if (!_started || _material == null) return;

        if (activate) {
            _material.SetColor("_Color", _diffuseColor);
            _material.SetTexture("_MainTex", _diffuseTexture);
            _material.SetColor("_EmissionColor", _emissionScale * _emissiveColor);
            _material.SetTexture("_EmissionMap", _emissiveTexture);
            _material.SetTexture("_BumpMap", _normalMap);
            _material.SetFloat("_BumpScale", _normalStrength);
        }
        else {
            _material.SetColor("_Color", _backup._diffuseColor);
            _material.SetTexture("_MainTex", _backup._diffuseTexture);
            _material.SetColor("_EmissionColor", _backup._emissionScale * _backup._emissiveColor);
            _material.SetTexture("_EmissionMap", _backup._emissiveTexture);
            _material.SetTexture("_BumpMap", _backup._normalMap);
            _material.SetFloat("_BumpScale", _backup._normalStrength);
        }
    }

    public void OnReset() {
        _material.SetColor("_Color", _backup._diffuseColor);
        _material.SetTexture("_MainTex", _backup._diffuseTexture);
        _material.SetColor("_EmissionColor", _backup._emissionScale * _backup._emissiveColor);
        _material.SetTexture("_EmissionMap", _backup._emissiveTexture);
        _material.SetTexture("_BumpMap", _backup._normalMap);
        _material.SetFloat("_BumpScale", _backup._normalStrength);
    }

    public int GetInstanceID() {
        return _material.GetInstanceID();
    }
}
