﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;

public class PVSAgent : MonoBehaviour {
    // Inspector assigned
    [SerializeField] List<GameObject> _renderers = new List<GameObject>();
    
    // Internals
    private List<Renderer> _rendererBucket = new List<Renderer>();
    private bool _isVisible;
    private Dictionary<int, PVSNode> _attachedNodes = new Dictionary<int, PVSNode>();
    
    // UNITY methods
    private void OnEnable() {
        PVSSystem.instance.OnChange.AddListener(InvalidateVisibility);
    }

    private void OnDisable() {
        PVSSystem.instance.OnChange.RemoveListener(InvalidateVisibility);
    }

    private void Start() {
        foreach (var renderer in _renderers) {
            Renderer[] renderers = renderer.GetComponentsInChildren<Renderer>(true);
            foreach (var r in renderers) {
                _rendererBucket.Add(r);
            }
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (!other.CompareTag("PVS Node")) return;

        _attachedNodes[other.GetInstanceID()] = other.GetComponent<PVSNode>();
        InvalidateVisibility();
    }

    private void OnTriggerExit(Collider other) {
        if (!other.CompareTag("PVS node")) return;

        _attachedNodes.Remove(other.GetInstanceID());
        InvalidateVisibility();
    }

    private void Show(bool show) {
        foreach (var r in _rendererBucket) {
            if (r != null)
                r.enabled = show;
        }
        _isVisible = show;
    }

    // PUBLIC methods
    public void InvalidateVisibility() {
        if (!PVSSystem.instance.PvsEnabled || !PVSSystem.instance.CullAgents) {
            Show(true);
            return;
        }

        bool visible = false;
        foreach (var pair in _attachedNodes) {
            if (pair.Value.Visible) {
                visible = true;
                break;
            }
        }
        
        if (visible != _isVisible)
            Show(visible);
    }
}
