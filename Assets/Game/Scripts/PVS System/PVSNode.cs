﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PVSConnectedNode {
    public PVSNode Node;
    public PVSGate Gate;
}

public class PVSNode : MonoBehaviour {
    // Inspector serializable
    public bool EditorVisibility = true;

    [Header("Managed Game Objects")] 
    [SerializeField] private List<GameObject> _nodeObjects = new List<GameObject>();

    [Header("Connections")] 
    [SerializeField] private List<PVSConnectedNode> _connections = new List<PVSConnectedNode>();
    
    [Header("Terminal Nodes")]
    [SerializeField] private List<PVSNode> _terminals = new List<PVSNode>();
    
    // Internals
    private Collider _collider;
    private int _currentFrame;
    private bool _visible = true;
    
    // Render/Light buckets
    private List<Renderer> _rendererBucket = new List<Renderer>();
    private List<Light> _lightBucket = new List<Light>();
    
    // Terminal Node hash sets
    private HashSet<PVSNode> _terminalNodes = new HashSet<PVSNode>();
    private HashSet<PVSNode> _combinedTerminalNodes = new HashSet<PVSNode>();

    public int Id => _collider.GetInstanceID();
    public int CurrentFrame => _currentFrame;
    public bool Visible => _visible;

    // UNITY methods
    private void OnEnable() {
        if (_collider == null)
            _collider = GetComponent<Collider>();
        
        PVSSystem.instance.RegisterNode(this);
    }

    private void OnDisable() {
        PVSSystem.instance.UnregisterNode(this);
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("PVS Viewer"))
            PVSSystem.instance.SetViewNode(this);
    }

    private void Start() {
        foreach (GameObject go in _nodeObjects) {
            Renderer[] renderers = go.GetComponentsInChildren<Renderer>(true);
            Light[] lights = go.GetComponentsInChildren<Light>(true);

            foreach (var renderer in renderers) {
                _rendererBucket.Add(renderer);
            }

            foreach (var light in lights) {
                _lightBucket.Add(light);
            }
        }
        
        // Create terminal node hashset
        foreach (var node in _terminals) {
            _terminalNodes.Add(node);
        }

        RevealAllLights();
        RevealAllRenderers();
    }
    
    // HELPER methods

    private void CombineHashSet(HashSet<PVSNode> terminalNodes) {
        _combinedTerminalNodes.Clear();
        if (terminalNodes != null) {
            foreach (var node in terminalNodes) {
                _combinedTerminalNodes.Add(node);
            }
        }

        foreach (var node in _terminalNodes) {
            _combinedTerminalNodes.Add(node);
        }
    }

    // PUBLIC methods

    public void ShowObjects(bool show) {
        _visible = show;
        if (Application.isPlaying) {
            if (PVSSystem.instance.CullRenderers) {
                foreach (var renderer in _rendererBucket) {
                    if (renderer != null)
                        renderer.enabled = show;
                }
            }

            if (PVSSystem.instance.CullLights) {
                foreach (var light in _lightBucket) {
                    if (light != null)
                        light.enabled = show;
                }
            }
        }
        else {
            foreach (GameObject go in _nodeObjects) {
                Renderer[] renderers = go.GetComponentsInChildren<Renderer>(true);
                Light[] lights = go.GetComponentsInChildren<Light>(true);
                foreach (var renderer in renderers)
                    renderer.enabled = show;
                foreach (var light in lights)
                    light.enabled = show;
            }
        }
    }
    
    // GAME OBJECT MANIPULATION methods
    
    public void RevealAllLights() {
        foreach (var light in _lightBucket)
            light.enabled = true;
    }
    
    public void RevealAllRenderers() {
        foreach (var renderer in _rendererBucket)
            renderer.enabled = true;
    }
    
    // FLOODING
    public void DoFlood(HashSet<PVSNode> terminalNodes = null) {
        _currentFrame = Time.frameCount;
        ShowObjects(true);

        CombineHashSet(terminalNodes);

        foreach (var connect in _connections) {
            if (connect.Node.CurrentFrame != _currentFrame) {
                if (connect.Gate == null || connect.Gate.IsOpen()) {
                    if (!_combinedTerminalNodes.Contains(connect.Node)) {
                        connect.Node.DoFlood(_combinedTerminalNodes);
                    }
                }
            }
        }
    }
}
