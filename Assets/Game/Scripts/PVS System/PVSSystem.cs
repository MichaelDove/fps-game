﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PVSSystem : MonoBehaviour {
    // Singleton
    private static PVSSystem _instance = null;

    public static PVSSystem instance {
        get {
            if (_instance == null)
                _instance = (PVSSystem) FindObjectOfType(typeof(PVSSystem));
            return _instance;
        }
    }

    [SerializeField] private bool _pvsEnabled = true;
    [SerializeField] private bool _cullRenderers = true;
    [SerializeField] private bool _cullLights = true;
    [SerializeField] private bool _cullAgents = true;
    
    // Getters
    public bool CullRenderers {
        get => _cullRenderers;
        set {
            _cullRenderers = value;
            if (!_cullRenderers) 
                RevealAllRenderers();
        }
    }

    public bool CullLights {
        get => _cullLights;
        set {
            _cullLights = value;
            if (!_cullLights)
                RevealAllLights();
        }
    }

    public bool CullAgents {
        get => _cullAgents;
        set => _cullAgents = value;
    }

    public bool PvsEnabled => _pvsEnabled;

    // Events
    public UnityEvent OnChange = new UnityEvent();
    
    // Internals
    private List<PVSNode> _nodes = new List<PVSNode>();
    private Dictionary<int, PVSNode> _nodesByID = new Dictionary<int, PVSNode>();
    private PVSNode _currNode;

    private void OnDisable() {
        OnChange.RemoveAllListeners();
        _instance = null;
    }

    private void RevealAllRenderers() {
        foreach (var node in _nodes) {
            node.RevealAllRenderers();
        }
    }

    private void RevealAllLights() {
        foreach (var node in _nodes)
            node.RevealAllLights();
    }
    
    // PUBLIC methods

    public void EnablePVS(bool enable) {
        _pvsEnabled = enable;
        if (Application.isPlaying) {
            if (!_pvsEnabled) {
                foreach (var node in _nodes) {
                    node.RevealAllLights();
                    node.RevealAllRenderers();
                }
                
                OnChange.Invoke();
            }
            else {
                Refresh();
            }
        }
    }

    public void RegisterNode(PVSNode node) {
        _nodes.Add(node);
        _nodesByID[node.Id] = node;
    }

    public void UnregisterNode(PVSNode node) {
        _nodes.Remove(node);
        _nodesByID.Remove(node.Id);
    }

    public void SetViewNode(PVSNode node) {
        if (!_pvsEnabled) return;
        
        _currNode = node;
        foreach (var n in _nodes) {
            n.ShowObjects(false);
        }
        
        _currNode.DoFlood();
        
        OnChange.Invoke();
    }
    
    public void Refresh() {
        if (_pvsEnabled && _currNode)
            SetViewNode(_currNode);
    }

    public bool IsNodeVisible(Collider col) {
        PVSNode node;
        if (_nodesByID.TryGetValue(col.GetInstanceID(), out node)) {
            return node.Visible;
        }
        
        return false;
    }
}
