﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PVSInteractiveDoor : PVSGate {
    // Internals
    private InteractiveDoor _door;
    
    // UNITY methods
    private void Awake() {
        _door = GetComponent<InteractiveDoor>();
        _door.OnStateChangedEvent.AddListener(OnStateChanged);
    }
    
    // PUBLIC methods
    public void OnStateChanged(bool isOpen) {
        if (PVSSystem.instance)
            PVSSystem.instance.Refresh();
    }

    public override bool IsOpen() {
        return _door.IsOpen || _door.IsAnimating;
    }
}
