﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PVSGate : MonoBehaviour {
    public abstract bool IsOpen();
}
