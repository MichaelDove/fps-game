﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ItemCollectionUI : MonoBehaviour {
    // Inspector assigned
    [Header("UI References")] 
    [SerializeField] private GameObject _uiContainer;
    [SerializeField] private List<Transform> _mounts;
    [SerializeField] private Text _ownerText;
    [SerializeField] private Transform _infoPopup;

    [Header("Colors")] 
    [SerializeField] private Color _mountHoverColor = Color.gray;
    [SerializeField] private Color _mountSelectedColor = Color.red;

    [Header("Shared Variables")] 
    [SerializeField] private SharedFloat _crosshairTransparency;
    
    // Internals
    private CharacterManager _characterManager;
    private ItemCollection _itemCollection;
    private List<InventoryMountInfo> _mountInfos = new List<InventoryMountInfo>();
    private Color _mountColor = Color.black;
    private int _selectedMount = -1;


    private void Awake() {
        Image mountBackground = _mounts[0].GetComponent<Image>();
        _mountColor = mountBackground.color;
    }

    public void Show(ItemCollection collection, CharacterManager characterManager) {
        _characterManager = characterManager;
        _itemCollection = collection;

        _crosshairTransparency.Value = 0f;
        Invalidate();
        
        _characterManager.ExternalUIActive(true);
        
        _uiContainer.SetActive(true);
    }

    public void Hide() {
        _uiContainer.SetActive(false);
        _characterManager.ExternalUIActive(false);
        Destroy(this.gameObject);
    }

    private void Invalidate() {
        // fetch all allocated mounts from collection
        _itemCollection.GetMounts(_mountInfos);
        
        // Reset Mounts
        foreach (var mount in _mounts) {
            var itemImage = mount.Find("Image");
            var itemText = mount.Find("Empty");

            itemImage.gameObject.SetActive(false);
            itemText.gameObject.SetActive(true);
        }
        
        _infoPopup.gameObject.SetActive(false);
        _ownerText.text = _itemCollection.Owner;

        int slotIndex = 0;
        
        // Render Mounts
        foreach (var mi in _mountInfos) {
            if (mi == null) continue;
            
            Sprite sprite = null;
            // cast to the correct type
            if (mi is WeaponMountInfo) {
                if ((mi as WeaponMountInfo).Weapon == null) {
                    slotIndex++;
                    continue;
                }
                
                sprite = (mi as WeaponMountInfo).Weapon.InventoryImage;
            }
            else if (mi is AmmoMountInfo) {
                if ((mi as AmmoMountInfo).Ammo == null) {
                    slotIndex++;
                    continue;
                }
                
                sprite = (mi as AmmoMountInfo).Ammo.InventoryImage;
            }
            else if (mi is BackpackMountInfo) {
                if ((mi as BackpackMountInfo).Item == null) {
                    slotIndex++;
                    continue;
                }
                
                sprite = (mi as BackpackMountInfo).Item.InventoryImage;
            }
            else if (mi is AudioMountInfo) {
                if ((mi as AudioMountInfo).Audio != null) {
                    slotIndex++;
                    continue;
                }
                
                sprite = (mi as AudioMountInfo).Audio.InventoryImage;
            }
            else {
                continue;
            }

            var itemMount = _mounts[slotIndex];

            var image = itemMount.Find("Image").GetComponent<Image>();
            var empty = itemMount.Find("Empty");
            image.sprite = sprite;
            image.gameObject.SetActive(true);
            empty.gameObject.SetActive(false);

            slotIndex++;
        }
    }

    private void DisplayInfo(int mount) {
        if (mount == -1 || mount >= _mountInfos.Count) {
            _infoPopup.gameObject.SetActive(false);
            return;
        }
        

        _infoPopup.gameObject.SetActive(true);
        string itemString = null;
        string infoString = null;

        var mi = _mountInfos[mount];

        if (mi is WeaponMountInfo) {
            var wmi = mi as WeaponMountInfo;
            if (wmi.Weapon == null) {
                _infoPopup.gameObject.SetActive(false);
                return;
            }
            itemString = wmi.Weapon.name;
            if (wmi.Weapon.AttackType == InventoryWeaponAttackType.Ammunition) {
                infoString = "Ammo: " + wmi.InGunAmmo;
            }
            else {
                DisplayInfo(-1);
            }
        }
        else if (mi is BackpackMountInfo) {
            var imi = mi as BackpackMountInfo;
            if (imi.Item == null) {
                _infoPopup.gameObject.SetActive(false);
                return;
            }
            itemString = imi.Item.name;
            infoString = imi.Item.GetStatString(imi.ContextData);
        }
        else if (mi is AmmoMountInfo) {
            var ami = mi as AmmoMountInfo;
            if (ami.Ammo == null) {
                _infoPopup.gameObject.SetActive(false);
                return;
            }
            itemString = ami.Ammo.name;
            infoString = "Ammo: " + ami.AmmoRounds;
        }
        else if (mi is AudioMountInfo) {
            var ami = mi as AudioMountInfo;
            if (ami.Audio == null) {
                _infoPopup.gameObject.SetActive(false);
                return;
            }
            itemString = ami.Audio.name;
            infoString = ami.Audio.GetStatString();
        }

        var infoTextTrans = _infoPopup.Find("Info Text");
        var itemTextTrans = _infoPopup.Find("Item Text");

        var infoText = infoTextTrans.GetComponent<Text>();
        infoText.text = infoString;
        var itemText = itemTextTrans.GetComponent<Text>();
        itemText.text = itemString;
    }

    public void OnEnterMount(Image image) {
        int mount;
        if (!int.TryParse(image.name, out mount)) {
            Debug.LogWarning("OnEnterMount Error: Could not parse image name as ing");
            return;
        }

        if (_selectedMount != mount) {
            image.color = _mountHoverColor;
        } else if (_selectedMount == -1) {
            DisplayInfo(mount);
        }
    }
    
    public void OnExitMount(Image image) {
        int mount;
        if (!int.TryParse(image.name, out mount)) {
            Debug.LogWarning("OnExitMount Error: Could not parse image name as ing");
            return;
        }

        if (_selectedMount != mount) {
            image.color = _mountColor;
        } else if (_selectedMount == -1) {
            DisplayInfo(-1);
        }
    }

    public void OnClickMount(Image image) {
        int mount;
        if (!int.TryParse(image.name, out mount)) {
            Debug.LogWarning("OnClickMount Error: Could not parse image name as ing");
            return;
        }

        if (_selectedMount != -1 && _selectedMount != mount) {
            _mounts[_selectedMount].GetComponent<Image>().color = _mountColor;
        }

        if (_selectedMount != mount) {
            _selectedMount = mount;
            image.color = _mountSelectedColor;
            DisplayInfo(mount);
        }
        else {
            _selectedMount = -1;
            image.color = _mountColor;
            DisplayInfo(-1);
        } 
    }

    public void TakeItem() {
        if (_selectedMount != -1) {
            var mi = _mountInfos[_selectedMount];

            if (_characterManager.Inventory.AddItemFromMountInfo(mi, _characterManager.transform.position)) {
                _itemCollection.ClearMount(mi);
                _mounts[_selectedMount].GetComponent<Image>().color = _mountColor;
                _selectedMount = -1;
                DisplayInfo(-1);
                Invalidate();
            }
        }
    }
}
