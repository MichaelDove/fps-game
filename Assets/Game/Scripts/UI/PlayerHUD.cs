﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum ScreenFadeType {FadeIn, FadeOut}

public class PlayerHUD : MonoBehaviour {
    // Inspector assigned UI References
    [Header("UI Sliders")]
    [SerializeField] private Slider _healthSlider;
    [SerializeField] private Slider _staminaSlider;
    [SerializeField] private Slider _batterySlider;
    [SerializeField] private Slider _nightVisionSlider;
    
    [Header("UI Texts")]
    [SerializeField] private Text _notificationText;
    [SerializeField] private Text _transcriptText;
    [SerializeField] private Text _interactionText;
    [SerializeField] private Text _inWeaponAmmoText;
    [SerializeField] private Text _inAmmoBeltAmmoText;

    [Header("UI Images")]
    [SerializeField] private Image _crosshair;

    [Header("mp4 References")] 
    [SerializeField] private GameObject _mp4Overlay;
    [SerializeField] private Slider _timelineSlider;
    [SerializeField] private Text _person;
    [SerializeField] private Text _subject;
    
    [Header("Shared Variables")] 
    [SerializeField] private SharedFloat _health;
    [SerializeField] private SharedFloat _stamina;
    [SerializeField] private SharedFloat _flashlightBattery;
    [SerializeField] private SharedFloat _nightVisionBattery;
    [SerializeField] private SharedString _interactionString;
    [SerializeField] private SharedString _transcriptString;
    [SerializeField] private SharedTimeStringQueue _notificationQueue;
    
    [Header("Crosshair")]
    [SerializeField] private SharedVector3 _crosshairPosition;
    [SerializeField] private SharedSprite _crosshairSprite;
    [SerializeField] private SharedFloat _crosshairAlpha;

    [Header("Gun Info")] 
    [SerializeField] private SharedInt _inWeaponAmmo;
    [SerializeField] private SharedInt _inAmmoBeltAmmo;

    [Header("Additional")]
    [SerializeField] private Image _screenFade;
    [SerializeField] private Sprite _defaultCrosshair;
    [SerializeField] private float _crosshairAlphaScale = 1.0f;
    [SerializeField] private GameObject _exitWindow;

    // Private variables
    private float _currFadeLevel = 1.0f;
    private IEnumerator _coroutine = null;

    // Start is called before the first frame update
    void Start() {
        Color c = _screenFade.color;
        c.a = _currFadeLevel;
        _screenFade.color = c;
    }

    private void Update() {
        // update the values:
        _healthSlider.value = _health.Value;
        _staminaSlider.value = _stamina.Value;
        _batterySlider.value = _flashlightBattery.Value;
        _nightVisionSlider.value = _nightVisionBattery.Value;
        _interactionText.text = _interactionString.Value;
        _transcriptText.text = _transcriptString.Value;
        _notificationText.text = _notificationQueue.Text;
        _inWeaponAmmoText.text = _inWeaponAmmo.Value.ToString();
        _inAmmoBeltAmmoText.text = _inAmmoBeltAmmo.Value.ToString();

        _crosshair.transform.position = _crosshairPosition.Value;
        _crosshair.sprite = _crosshairSprite.Value == null? _defaultCrosshair : _crosshairSprite.Value;
        _crosshair.color = new Color(_crosshair.color.r, _crosshair.color.g, _crosshair.color.b, _crosshairAlpha.Value * _crosshairAlphaScale);
    }

    public void OnBeginAudio(InventoryItemAudio audioItem) {
        _mp4Overlay.SetActive(true);
        _person.text = audioItem.Person;
        _subject.text = audioItem.Subject;
        _timelineSlider.value = 0;
    }

    public void OnUpdateAudio(float time) {
        _timelineSlider.value = time;
    }
    
    public void OnEndAudio() {
        _mp4Overlay.SetActive(false);   
    }

    public void Exit() {
        _exitWindow.SetActive(true);
    }

    public void CancelExit() {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        _exitWindow.SetActive(false);
    }

    public void MainMenu() {
        ApplicationManager.instance.LoadMainMenu();
    }

    public void Fade(float seconds, ScreenFadeType direction) {
        if (_coroutine != null) StopCoroutine(_coroutine);
        float fadeTarget = 0.0f;

        switch (direction) {
            case ScreenFadeType.FadeIn:
                fadeTarget = 1.0f;
                break;
            case ScreenFadeType.FadeOut:
                fadeTarget = 0.0f;
                break;
        }

        _coroutine = FadeInternal(seconds, fadeTarget);
        StartCoroutine(_coroutine);
    }

    IEnumerator FadeInternal(float time, float target) {
        float timer = 0.0f;
        float srcFade = _currFadeLevel;
        Color color = _screenFade.color;
        // avoid division by 0
        if (time < 0.1f) time = 0.1f;

        while (timer < time) {
            timer += Time.deltaTime;
            _currFadeLevel = Mathf.Lerp(srcFade, target, timer / time);
            color.a = _currFadeLevel;
            _screenFade.color = color;
            yield return null;
        }

        color.a = _currFadeLevel = target;
        _screenFade.color = color;
    }
    
    
}
