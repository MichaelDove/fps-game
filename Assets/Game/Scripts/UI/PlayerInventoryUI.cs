﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum InventoryPanelType {None, Backpack, Ammo, Weapons, Mp3}

[System.Serializable]
public struct InventoryUI_Mp3References {
     public Transform LogEntries;
     public RawImage Mugshot;
     public Text Author;
     public Text Subject;
     public Slider Timeline;
     public GameObject LogEntryPrefab;
}

[System.Serializable]
public struct InventoryUI_StatusReferences {
    public Slider Health;
    public Slider Stamina;
    public Slider Battery;
    public Slider NightVision;
}

[System.Serializable]
public struct InventoryUI_TabGroupItem {
    public Text TabText;
    public GameObject LayoutContainer;
}

[System.Serializable]
public struct InventoryUI_TabGroup {
    public List<InventoryUI_TabGroupItem> Items;
}

[System.Serializable]
public struct InventoryUI_DescriptionLayout {
    public GameObject LayoutContainer;
    public Image Image;
    public Text Title;
    public Text Description;
    public ScrollRect ScrollView;
}

[System.Serializable]
public struct InventoryUI_Button {
    public GameObject GameObject;
    public Text Text;
}

public class PlayerInventoryUI : MonoBehaviour  {
   // Inspector assigned 
   [Header("Inventory")] 
   [SerializeField] private Inventory _inventory;
   
   [Header("Equipment Mount References")] 
   [SerializeField] private List<GameObject> _backpackMounts = new List<GameObject>();
   [SerializeField] private List<GameObject> _weaponMounts = new List<GameObject>();
   [SerializeField] private List<GameObject> _ammoMounts = new List<GameObject>();
   
   [Header("Mp3")]
   [SerializeField] private InventoryUI_Mp3References _mp3References;
   
   [Header("Status Panel")]
   [SerializeField] private InventoryUI_StatusReferences _statusReferences;
   
   [Header("BackPack / Mp3 Tab Group")]
   [SerializeField] private InventoryUI_TabGroup _tabGroup;
   
   [Header("Description Layout")] 
   [SerializeField] private InventoryUI_DescriptionLayout _generalDescription;
   [SerializeField] private InventoryUI_DescriptionLayout _weaponDescription;
   
   [Header("Button References")] 
   [SerializeField] private InventoryUI_Button _button1;
   [SerializeField] private InventoryUI_Button _button2;
   
   [Header("Shared variables")]
   [SerializeField] private SharedFloat _health;
   [SerializeField] private SharedFloat _stamina;
   [SerializeField] private SharedFloat _battery;
   [SerializeField] private SharedFloat _nightVisionBattery;
   
   [Header("Colors")]
   [SerializeField] private Color _tabTextHover = Color.cyan;
   [SerializeField] private Color _tabTextInactive = Color.gray;
   [SerializeField] private Color _backpackMountHover = Color.red;
   [SerializeField] private Color _ammoMountHover = Color.red;
   [SerializeField] private Color _weaponMountHover = Color.red;
   
   // Public properties
   public Inventory Inventory {
       get => _inventory;
       set => _inventory = value;
   }

   // Private members
    // equipment mount references that will be cached
     //backpack
   private List<Image> _backpackMountImages = new List<Image>();
   private List<Text> _backpackMountText = new List<Text>();
     //weapons
    private List<Image> _weaponMountImages = new List<Image>();
    private List<Text> _weaponMountNames = new List<Text>();
    private List<Text> _weaponMountAmmo = new List<Text>();
      //ammo
    private List<Image> _ammoMountImages = new List<Image>();
    private List<Text> _ammoMountEmptyText = new List<Text>();
    private List<Text> _ammoMountAmmo = new List<Text>();
    
     // cache the starting color, before we hover over them or click them
    private Color _backpackMountColor;
    private Color _weaponMountColor;
    private Color _ammoMountColor;
    private Color _tabTextColor;

     // other
    private InventoryPanelType _selectedPanelType = InventoryPanelType.None;
    private int _selectedMount = -1;
    private bool _isInitialized = false;
    private int _activeTab = 0;

    private void OnEnable() {
        Input.ResetInputAxes();
        Time.timeScale = 0f;
        AudioListener.pause = true;
        
        // display the UI in its reset position
        Invalidate();
    }

    private void OnDisable() {
        Time.timeScale = 1f;
        AudioListener.pause = false;
        Input.ResetInputAxes();
    }

    private void Invalidate() {
        // make sure its initialized
        Initialize();
        
        // reset selections
        _selectedPanelType = InventoryPanelType.None;
        _selectedMount = -1;
        
        // deactivate descriptions
        _generalDescription.LayoutContainer.SetActive(false);
        _weaponDescription.LayoutContainer.SetActive(false);
        
        // deactivate buttons
        _button1.GameObject.SetActive(false);
        _button2.GameObject.SetActive(false);
        
        // clear the weapon mounts
        for (int i = 0; i < _weaponMounts.Count; i++) {
            _weaponMountImages[i].sprite = null;
            _weaponMountNames[i].text = "";
            _weaponMounts[i].transform.GetComponent<Image>().fillCenter = false;
            _weaponMounts[i].SetActive(false);
        }
        
        // clear the backpack
        for (int i = 0; i < _backpackMounts.Count; i++) {
            _backpackMountImages[i].gameObject.SetActive(false);
            _backpackMountImages[i].sprite = null;
            // enable 'empty' text
            _backpackMountText[i].gameObject.SetActive(true);
            // destroy any custom sliders:
            var t = _backpackMounts[i].transform.Find("Custom Render Context");
            if (t) {
                t.SetParent(null);
                Destroy(t.gameObject);
            }

            Image img = _backpackMounts[i].GetComponent<Image>();
            img.fillCenter = false;
            img.color = _backpackMountColor;
        }
        
        // configure ammo slots
        for (int i = 0; i < _ammoMounts.Count; i++) {
            _ammoMountImages[i].gameObject.SetActive(false);
            _ammoMountImages[i].sprite = null;

            _ammoMountEmptyText[i].gameObject.SetActive(true);
            _ammoMountAmmo[i].gameObject.SetActive(false);
            
            Image img = _ammoMounts[i].GetComponent<Image>();
            img.fillCenter = false;
            img.color = _ammoMountColor;
        }
        
        // update status panel
        _statusReferences.Health.value = _health.Value;
        _statusReferences.Stamina.value = _stamina.Value;
        _statusReferences.Battery.value = _battery.Value;
        _statusReferences.NightVision.value = _nightVisionBattery.Value;

        UpdateInventory();
    }

    private void UpdateInventory() {
        // paint all the data in the inventory system to the inventory ui
        for (var i = 0; i < _weaponMounts.Count; i++) {
            var weaponInfo = _inventory.GetWeapon(i);
            var weapon = weaponInfo.Weapon;
            
            // we don't have a weapon assigned
            if (weapon == null) continue;

            _weaponMountImages[i].sprite = weapon.InventoryImage;
            _weaponMountNames[i].text = weapon.InventoryName;
            if (weapon.AttackType == InventoryWeaponAttackType.Melee) {
                _weaponMountAmmo[i].text = "\u221E"; // infinity symbol
            }
            else {
                _weaponMountAmmo[i].text = weaponInfo.InGunAmmo + " / " + weapon.AmmoCapacity;
            }
            _weaponMounts[i].SetActive(true);
        }

        for (var i = 0; i < _ammoMounts.Count; i++) {
            var ammoInfo = _inventory.GetAmmo(i);
            var ammo = ammoInfo.Ammo;

            // no ammo in this slot
            if (ammo == null) continue;
            
            _ammoMountImages[i].sprite = ammo.InventoryImage;
            _ammoMountAmmo[i].text = ammoInfo.AmmoRounds.ToString();
            
            _ammoMountImages[i].gameObject.SetActive(true);
            _ammoMountAmmo[i].gameObject.SetActive(true);
            _ammoMountEmptyText[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < _backpackMounts.Count; i++) {
            var backpackInfo = _inventory.GetBackpack(i);
            var backpack = backpackInfo.Item;

            // no item in this slot
            if (backpack == null) continue;

            _backpackMountImages[i].sprite = backpack.InventoryImage;
            _backpackMountImages[i].gameObject.SetActive(true);
            _backpackMountText[i].gameObject.SetActive(false);

            var go = backpack.OnRenderMount(_backpackMounts[i].transform, backpackInfo.ContextData);
            if (go) {
                go.name = "Custom Render Context";
            }
        }
        
        // update recordings
        int audioCount = _inventory.GetAudioRecordingCount();
        int displayCount = _mp3References.LogEntries.childCount;
        // process each recording
        for (int i = 0; i < audioCount; i++) {
            var audioItem = _inventory.GetAudioRecording(i);
            if (i >= displayCount) {
                var go = Instantiate(_mp3References.LogEntryPrefab);
                var entry = go.GetComponent<PlayerInventoryUI_mp3Entry>();
                entry.transform.SetParent(_mp3References.LogEntries, false);
                entry.transform.SetSiblingIndex(i);
                entry.SetData(audioItem, i);
            }
            else {
                var entry = _mp3References.LogEntries.GetChild(i).GetComponent<PlayerInventoryUI_mp3Entry>();
                entry.SetData(audioItem,i);
            }
        } 

        for (int i = audioCount; i < _mp3References.LogEntries.childCount; i++) {
            Destroy(_mp3References.LogEntries.GetChild(i).gameObject);
        }
    }

    private void Initialize() {
        if (_isInitialized) return;
        _isInitialized = true;
        
        // cache the color of the backpack item frame
        Image tmp = _backpackMounts[0].GetComponent<Image>();
        _backpackMountColor = tmp.color;
        // do the same for ammos frame
        tmp = _ammoMounts[0].GetComponent<Image>();
        _ammoMountColor = tmp.color;
        // do the same for weapon frame
        tmp = _weaponMounts[0].GetComponent<Image>();
        _weaponMountColor = tmp.color;
        // cache the color of the tab text
        _tabTextColor = _tabGroup.Items[0].TabText.color;
        
        // cache the references for the image/text components of backpack mounts
        foreach (var m in _backpackMounts) {
            Transform parent = m.transform;
            
            Transform temp = parent.Find("image");
            _backpackMountImages.Add(temp.GetComponent<Image>());
            temp = parent.Find("empty");
            _backpackMountText.Add(temp.GetComponent<Text>());
        }
        
        // cache the references for components of ammo mounts
        foreach (var a in _ammoMounts) {
            Transform parent = a.transform;
            
            Transform temp = parent.Find("image");
            _ammoMountImages.Add(temp.GetComponent<Image>());
            temp = parent.Find("empty");
            _ammoMountEmptyText.Add(temp.GetComponent<Text>());
            temp = parent.Find("ammo");
            _ammoMountAmmo.Add(temp.GetComponent<Text>());
        }
        
        // cache the references for components of weapon mounts
        foreach (var w in _weaponMounts) {
            Transform parent = w.transform;
            
            Transform temp = parent.Find("image");
            _weaponMountImages.Add(temp.GetComponent<Image>());
            temp = parent.Find("ammo");
            _weaponMountAmmo.Add(temp.GetComponent<Text>());
            temp = parent.Find("name");
            _weaponMountNames.Add(temp.GetComponent<Text>());
        }

        SelectTabGroup(0);
    }

    private void DisplayWeaponDescription(InventoryItem item, object contextData = null) {
        if (item == null) return;
        
        _generalDescription.LayoutContainer.SetActive(false);
        _weaponDescription.LayoutContainer.SetActive(true);

        _weaponDescription.Image.sprite = item.InventoryImage;
        _weaponDescription.Title.text = item.InventoryName;
        _weaponDescription.Description.text = item.GetDescription(contextData);

        if (item.InventoryAction != InventoryAction.None) {
            _button1.GameObject.SetActive(true);
            _button1.Text.text = item.InventoryActionText;
        }
        else {
            _button1.GameObject.SetActive(false);
        }
        
        _button2.GameObject.SetActive(true);
        _button2.Text.text = "Drop";

        _weaponDescription.ScrollView.verticalNormalizedPosition = 1f;
    }

    private void DisplayGeneralDescription(InventoryItem item, object contextData = null) {
        if (item == null) return;
        
        _generalDescription.LayoutContainer.SetActive(true);
        _weaponDescription.LayoutContainer.SetActive(false);

        _generalDescription.Image.sprite = item.InventoryImage;
        _generalDescription.Title.text = item.InventoryName;
        _generalDescription.Description.text = item.GetDescription(contextData);
        
        if (item.InventoryAction != InventoryAction.None) {
            _button1.GameObject.SetActive(true);
            _button1.Text.text = item.InventoryActionText;
        }
        else {
            _button1.GameObject.SetActive(false);
        }
        
        _button2.GameObject.SetActive(true);
        _button2.Text.text = "Drop";

        _generalDescription.ScrollView.verticalNormalizedPosition = 1f;
    }

    private void HideDescription() {
        _generalDescription.LayoutContainer.SetActive(false);
        _weaponDescription.LayoutContainer.SetActive(false);
        _button1.GameObject.SetActive(false);
        _button2.GameObject.SetActive(false);
    }
    
    // Public methods
    public void RefreshMp3Entries() {
        for (int i = 0; i < _mp3References.LogEntries.childCount; i++) {
            var entry = _mp3References.LogEntries.GetChild(i).GetComponent<PlayerInventoryUI_mp3Entry>();
            var audioItem = _inventory.GetAudioRecording(i);
            
            entry.SetData(audioItem, i);
        }
    }
    
    public void SelectTabGroup(int panel) {
        _activeTab = panel;

        for(int i = 0; i < _tabGroup.Items.Count; i++) {
            if (i == _activeTab) {
                _tabGroup.Items[i].LayoutContainer.SetActive(true);
                _tabGroup.Items[i].TabText.color = _tabTextColor;
            }
            else {
                _tabGroup.Items[i].LayoutContainer.SetActive(false);
                _tabGroup.Items[i].TabText.color = _tabTextInactive;
            }
        }
    }

    // BACKPACK methods
    public void OnEnterBackpackMount(Image img) {
        int mount;
        int.TryParse(img.name, out mount);
        
        // valid index?
        if (mount >= 0 && mount < _backpackMounts.Count) {
            if (_selectedPanelType != InventoryPanelType.Backpack || _selectedMount != mount)
                img.color = _backpackMountHover;

            if (_selectedPanelType != InventoryPanelType.None) return;

            var itemInfo = Inventory.GetBackpack(mount);
            // update description panel
            DisplayGeneralDescription(itemInfo.Item, itemInfo.ContextData);
        } 
        else {
            Debug.LogError("invalid index of backpack mount");
        }
    }

    public void OnExitBackpackMount(Image img) {
        img.color = _backpackMountColor;
        if (_selectedPanelType != InventoryPanelType.None) return;
        HideDescription();
    }

    public void OnClickBackpackMount(Image img) {
        int mount;
        int.TryParse(img.name, out mount);
        
        // valid index?
        if (mount >= 0 && mount < _backpackMounts.Count) {
            if (mount == _selectedMount && _selectedPanelType == InventoryPanelType.Backpack) {
                // unselect
                Invalidate();
                img.color = _backpackMountHover;
                img.fillCenter = false;
                var itemInfo = Inventory.GetBackpack(mount);
                // update description panel
                DisplayGeneralDescription(itemInfo.Item, itemInfo.ContextData);
            }
            else {
                // select new
                Invalidate();
                _selectedPanelType = InventoryPanelType.Backpack;
                _selectedMount = mount;
                img.color = _backpackMountColor;
                img.fillCenter = true;
                var itemInfo = Inventory.GetBackpack(mount);
                // update description panel
                DisplayGeneralDescription(itemInfo.Item, itemInfo.ContextData);
            }
        } 
        else {
            Debug.LogError("invalid index of backpack mount");
        }
    }
    
    // AMMO methods
    public void OnEnterAmmoMount(Image img) {
        int mount;
        int.TryParse(img.name, out mount);
        
        // valid index?
        if (mount >= 0 && mount < _ammoMounts.Count) {
            if (_selectedPanelType != InventoryPanelType.Ammo || _selectedMount != mount)
                img.color = _ammoMountHover;

            if (_selectedPanelType != InventoryPanelType.None) return;
            
            var itemInfo = Inventory.GetAmmo(mount);
            // update description panel
            DisplayGeneralDescription(itemInfo.Ammo);
        } 
        else {
            Debug.LogError("invalid index of ammo mount");
        }
    }

    public void OnExitAmmoMount(Image img) {
        img.color = _ammoMountColor;
        if (_selectedPanelType != InventoryPanelType.None) return;
        HideDescription();
    }

    public void OnClickAmmoMount(Image img) {
        int mount;
        int.TryParse(img.name, out mount);
        
        // valid index?
        if (mount >= 0 && mount < _ammoMounts.Count) {
            if (mount == _selectedMount && _selectedPanelType == InventoryPanelType.Ammo) {
                // unselect
                Invalidate();
                img.color = _ammoMountHover;
                img.fillCenter = false;
                
                var itemInfo = Inventory.GetAmmo(mount);
                // update description panel
                DisplayGeneralDescription(itemInfo.Ammo);
            }
            else {
                // select new
                Invalidate();
                _selectedPanelType = InventoryPanelType.Ammo;
                _selectedMount = mount;
                img.color = _ammoMountColor;
                img.fillCenter = true;
                
                var itemInfo = Inventory.GetAmmo(mount);
                // update description panel
                DisplayGeneralDescription(itemInfo.Ammo);
            }
        } 
        else {
            Debug.LogError("invalid index of ammo mount");
        }
    }
    
    // WEAPON methods
    public void OnEnterWeaponMount(Image img) {
        int mount;
        int.TryParse(img.name, out mount);
        
        // valid index?
        if (mount >= 0 && mount < _weaponMounts.Count) {
            if (_selectedPanelType != InventoryPanelType.Weapons || _selectedMount != mount)
                img.color = _weaponMountHover;

            if (_selectedPanelType != InventoryPanelType.None) return;
            
            var itemInfo = Inventory.GetWeapon(mount);
            // update description panel
            DisplayWeaponDescription(itemInfo.Weapon);
        } 
        else {
            Debug.LogError("invalid index of weapon mount");
        }
    }

    public void OnExitWeaponMount(Image img) {
        img.color = _weaponMountColor;
        if (_selectedPanelType != InventoryPanelType.None) return;
        HideDescription();
    }

    public void OnClickWeaponMount(Image img) {
        int mount;
        int.TryParse(img.name, out mount);
        
        // valid index?
        if (mount >= 0 && mount < _weaponMounts.Count) {
            if (mount == _selectedMount && _selectedPanelType == InventoryPanelType.Weapons) {
                // unselect
                Invalidate();
                img.color = _weaponMountHover;
                img.fillCenter = false;
                
                var itemInfo = Inventory.GetWeapon(mount);
                // update description panel
                DisplayWeaponDescription(itemInfo.Weapon);
            }
            else {
                // select new
                Invalidate();
                _selectedPanelType = InventoryPanelType.Weapons;
                _selectedMount = mount;
                img.color = _weaponMountColor;
                img.fillCenter = true;
                
                var itemInfo = Inventory.GetWeapon(mount);
                // update description panel
                DisplayWeaponDescription(itemInfo.Weapon);
            }
        } 
        else {
            Debug.LogError("invalid index of weapon mount");
        }
    }
    
    // TAB methods
    public void OnEnterTab(int index) {
        if (index >= 0 && index < _tabGroup.Items.Count) {
            _tabGroup.Items[index].TabText.color = _activeTab == index ? _tabTextColor : _tabTextHover;
        }
    }
    
    public void OnExitTab(int index) { 
        if (index >= 0 && index < _tabGroup.Items.Count) {
            _tabGroup.Items[index].TabText.color = _activeTab == index ? _tabTextColor : _tabTextInactive;
        }
    }

    public void OnClickTab(int index) {
        if (index >= 0 && index < _tabGroup.Items.Count) {
            SelectTabGroup(index);
        }
    }
    
    // BUTTON methods
    public void OnActionButton1() {
        switch (_selectedPanelType) {
            case InventoryPanelType.Backpack: 
                _inventory.UseBackpackItem(_selectedMount);
                break;
            case InventoryPanelType.Weapons:
                _inventory.ReloadWeapon(_selectedMount);
                break;
        }
        
        Invalidate();
    }
    
    public void OnActionButton2() {
        switch (_selectedPanelType) {
            case InventoryPanelType.Backpack: 
                _inventory.DropBackpackItem(_selectedMount);
                break;
            case InventoryPanelType.Weapons:
                _inventory.DropWeaponItem(_selectedMount);
                break;
            case InventoryPanelType.Ammo:
                _inventory.DropAmmoItem(_selectedMount);
                break;
        }
        
        Invalidate();
    }
    
    // EVENT Listeners
    public void OnBeginAudio(InventoryItemAudio audioItem) {
        _mp3References.Author.text = audioItem.Person;
        _mp3References.Subject.text = audioItem.Subject;
        _mp3References.Mugshot.texture = audioItem.Image;
        _mp3References.Mugshot.color = Color.white;
        _mp3References.Timeline.value = 0f;
    }

    public void OnUpdateAudio(float time) {
        _mp3References.Timeline.value = time;
    }

    public void OnEndAudio() {
        _mp3References.Author.text = null;
        _mp3References.Subject.text = null;
        _mp3References.Mugshot.color = Color.black;
        _mp3References.Timeline.value = 0f;
        
        if (gameObject.activeInHierarchy)
            StartCoroutine(LateOnEndAudio());
    }

    private IEnumerator LateOnEndAudio() {
        yield return null;
        RefreshMp3Entries();
    }
}
