﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerInventoryUI_mp3Entry : MonoBehaviour {
    // Inspector assigned
    [SerializeField] private Text _person;
    [SerializeField] private Text _subject;
    [SerializeField] private Color _normalColor = Color.cyan;
    [SerializeField] private Color _hoverColor = Color.yellow;
    [SerializeField] private Color _activeColor = Color.red;
    
    // Internals
    private PlayerInventoryUI _inventoryUI;
    private InventoryItemAudio _inventoryItemAudio;
    private int _index = -1;

    PlayerInventoryUI inventoryUI {
        get {
            if (!_inventoryUI)
                _inventoryUI = GetComponentInParent<PlayerInventoryUI>();
            return _inventoryUI;
        }
    }

    bool IsActive() {
        return inventoryUI.Inventory.GetActiveAudioRecording() == _index;
    }

    public void SetData(InventoryItemAudio itemAudio, int index) {
        _inventoryItemAudio = itemAudio;
        _index = index;
        bool isActive = IsActive();

        // set text
        _person.text = itemAudio.Person;
        _subject.text = itemAudio.Subject;
        // set text color
        _person.color = isActive ? _activeColor : _normalColor;
        _subject.color = isActive ? _activeColor : _normalColor;
    }

    public void OnPointerEnter() {
        if (IsActive()) return;
        _person.color = _hoverColor;
        _subject.color = _hoverColor;
    }

    public void OnPointerExit() {
        if (IsActive()) return;
        _person.color = _normalColor;
        _subject.color = _normalColor;
    }

    public void OnPointerClick() {
        inventoryUI.Inventory.PlayAudioRecording(_index);
        inventoryUI.RefreshMp3Entries();
    }
}
