﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

[System.Serializable]
public class InventoryAudioBeginEvent : UnityEvent<InventoryItemAudio> {}

[System.Serializable]
public class InventoryAudioUpdateEvent : UnityEvent<float> {}

public class InventoryAudioPlayer : MonoBehaviour {
    // singleton
    private static InventoryAudioPlayer _instance = null;
    public static InventoryAudioPlayer instance => _instance;
    
    // Inspector assigned
    [Header("Audio configuration")] 
    [SerializeField] private AudioCollection _stateNotificationSounds;
    
    [Header("Event listeners")]
    public InventoryAudioBeginEvent OnBeginAudio = new InventoryAudioBeginEvent();
    public InventoryAudioUpdateEvent OnUpdateAudio = new InventoryAudioUpdateEvent();
    public UnityEvent OnEndAudio = new UnityEvent();
    
    [Header("Shared variables")] 
    [SerializeField] private SharedVector3 _playerPosition;
    [SerializeField] private SharedTimeStringQueue _notificationQueue;
    [SerializeField] private SharedString _transcriptText;
    
    // Internals
    private AudioSource _audioSource;
    private IEnumerator _coroutine;

    private void Awake() {
        _instance = this;
        _audioSource = GetComponent<AudioSource>();
        _audioSource.ignoreListenerPause = true;
    }
    
    // public methods

    public void PlayAudio(InventoryItemAudio audioItem) {
        if (_coroutine != null) {
            StopCoroutine(_coroutine);
            _coroutine = null; 
        }
        if (_audioSource.isPlaying)
            _audioSource.Stop();

        var collection = audioItem.AudioCollection;
        AudioClip clip = collection[2];
        // configure audio source
        _audioSource.clip = clip;
        _audioSource.volume = collection.Volume;
        _audioSource.spatialBlend = collection.SpatialBlend;
        _audioSource.priority = collection.Priority;
        _audioSource.Play();
        
        // trigger the begin event
        OnBeginAudio.Invoke(audioItem);
        
        // start the update coroutine
        _coroutine = UpdateAudio(audioItem);
        StartCoroutine(_coroutine);
    }

    public void StopAudio() {
        _audioSource.clip = null;
        if (_coroutine != null) {
            StopCoroutine(_coroutine);
            _coroutine = null;
        }

        _transcriptText.Value = null;
        
        // trigger the end event
        OnEndAudio.Invoke();
    }

    public IEnumerator UpdateAudio(InventoryItemAudio audioItem) {
        int previousStateKeyIndex = 0;
        int previousCaptionKeyIndex = 0;

        var stateKeys = audioItem.StateKeys;
        var captionKeys = audioItem.CaptionKeys;

        while (_audioSource.isPlaying) {
            OnUpdateAudio.Invoke(_audioSource.time / _audioSource.clip.length);
            for (int i = previousStateKeyIndex; i < stateKeys.Count; i++) {
                var keyFrame = stateKeys[i];
                if (keyFrame.Time > _audioSource.time) {
                    previousStateKeyIndex = i;
                    break;
                }

                if (ApplicationManager.instance.SetGameState(keyFrame.Key, keyFrame.Value)) {
                    _notificationQueue.Enqueue(keyFrame.UIMessage);
                }
                
                // play sound
                var clip = _stateNotificationSounds.audioClip;
                AudioManager.instance.PlaySound(_stateNotificationSounds, clip, _playerPosition.Value,0.0f,true);
                previousStateKeyIndex++;
            } 
            
            for (int i = previousCaptionKeyIndex; i < captionKeys.Count; i++) {
                var keyFrame = captionKeys[i];
                if (keyFrame.Time > _audioSource.time) {
                    previousCaptionKeyIndex = i;
                    break;
                }

                _transcriptText.Value = keyFrame.Text;
                previousCaptionKeyIndex++;
            }

            yield return null;
        }
        
        StopAudio();
    }
}
