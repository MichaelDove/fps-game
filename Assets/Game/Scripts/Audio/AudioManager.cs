﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class TrackInfo {
    public string Name;
    public AudioMixerGroup Group;
    public IEnumerator TrackFader;
}

public class AudioPoolItem {
    public GameObject GameObject = null;
    public Transform Transform = null;
    public AudioSource AudioSource = null;
    public float Unimportance = float.MaxValue;
    public bool Playing = false;
    public IEnumerator Coroutine = null;
    public ulong ID = 0;
}

public class AudioManager : MonoBehaviour {
    // Inspector assigned 
    [SerializeField] private AudioMixer _mixer;
    [SerializeField] private int _maxSounds = 10;
    
    // Statics
    private static AudioManager _instance = null;
    public static AudioManager instance {
        get {
            if (_instance == null)
                _instance = (AudioManager) FindObjectOfType(typeof(AudioManager));
            return _instance;
        }
    }
    
    // Private variables
    private Dictionary<string, TrackInfo> _tracks = new Dictionary<string, TrackInfo>();
    private List<AudioPoolItem> _pool = new List<AudioPoolItem>();
    private Dictionary<ulong, AudioPoolItem> _activePool = new Dictionary<ulong, AudioPoolItem>();
    private List<LayeredAudioSource> _layeredAudio = new List<LayeredAudioSource>(); 
    private ulong _idGiver = 0;
    private Transform _listenerPos = null;

    private void Awake() { 
        // don't destroy object when changing scenes
        DontDestroyOnLoad(gameObject);
        
        // fetch all the groups in the mixer
        AudioMixerGroup[] groups = _mixer.FindMatchingGroups(string.Empty);
        
        // Create our mixer tracks based on group name (Track -> AudioGroup)
        foreach (var group in groups) {
            TrackInfo trackInfo = new TrackInfo();
            trackInfo.Name = group.name;
            trackInfo.Group = group;
            trackInfo.TrackFader = null;
            _tracks[group.name] = trackInfo;
        }
        
        // Generate pool
        for (int i = 0; i < _maxSounds; ++i) {
            GameObject go = new GameObject("Pool item");
            AudioSource aus = go.AddComponent<AudioSource>();
            go.transform.parent = transform;
            
            // create and configure pool item
            AudioPoolItem pi = new AudioPoolItem();
            pi.GameObject = go;
            pi.AudioSource = aus;
            pi.Transform = go.transform;
            pi.Playing = false;
            go.SetActive(false);
            _pool.Add(pi);
        }
    }

    private void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        _listenerPos = FindObjectOfType<AudioListener>().transform;
    }

    private void Update() {
        // Update any layered audio sources
        foreach (var las in _layeredAudio) {
            if (las != null) las.Update();
        }
    }

    // INTERNAL METHODS ------------------------------------------------------------------------------------------------
    
    /// <summary>
    /// A coroutine used by the SetTrackVolume method.
    /// </summary>
    /// <param name="track"></param>
    /// <param name="volume"></param>
    /// <param name="fadeTime"></param>
    /// <returns></returns>
    protected IEnumerator SetTrackVolumeInternal(string track, float volume, float fadeTime) {
        float startVolume = 0.0f;
        float timer = 0.0f;
        _mixer.GetFloat(track, out startVolume);

        while (timer < fadeTime) {
            timer += Time.unscaledDeltaTime;
            _mixer.SetFloat(track, Mathf.Lerp(startVolume, volume, timer / fadeTime));
            yield return null;
        }

        _mixer.SetFloat(track, volume);
    }

    protected ulong ConfigurePoolObject(int poolIndex, string track, AudioClip clip, Vector3 position, float volume, float spatialBlend, float unimportance, float startTime, bool ignoreListenerPause) {
        // if pool index is out of range abort request
        if (poolIndex < 0 || poolIndex >= _pool.Count) return 0;
        
        // Get the pool item
        AudioPoolItem pi = _pool[poolIndex];
        // Generate a new id
        _idGiver++;
        
        // configure the audio source
        AudioSource so = pi.AudioSource;
        so.clip = clip;
        so.volume = volume;
        so.spatialBlend = spatialBlend;
        so.ignoreListenerPause = ignoreListenerPause;
        // assign to requested audio track
        so.outputAudioMixerGroup = _tracks[track].Group;
        // position source at requested position
        so.transform.position = position;
        
        // enable game object and start playing
        pi.Playing = true;
        pi.Unimportance = unimportance;
        pi.ID = _idGiver;
        pi.GameObject.SetActive(true);
        so.time = Mathf.Min(startTime, so.clip.length);
        so.Play();
        pi.Coroutine = StopSoundDelayed(_idGiver, so.clip.length);
        StartCoroutine(pi.Coroutine);
        
        // add sound to the active pool
        _activePool[pi.ID] = pi;

        return pi.ID;
    }

    protected IEnumerator StopSoundDelayed(ulong id, float duration) {
        yield return new WaitForSeconds(duration);

        AudioPoolItem activeSound;
        if (_activePool.TryGetValue(id, out activeSound)) {
            activeSound.AudioSource.Stop();
            activeSound.AudioSource.clip = null;
            activeSound.GameObject.SetActive(false);
            _activePool.Remove(id);
            
            // make it available again
            activeSound.Playing = false;
        }
    }
    
    // PUBLIC METHODS --------------------------------------------------------------------------------------------------

    public ulong PlaySound(string track, AudioClip clip, Vector3 position, float volume, float spatialBlend, int priority = 128, float startTime = 0f, bool ignoreListenerPause = false) {
        // do nothing if track does not exist, clip is null or volume is zero
        if (!_tracks.ContainsKey(track) || clip == null || volume.Equals(0.0f)) return 0;

        float unimportance = (_listenerPos.position - position).sqrMagnitude / Mathf.Max(1, priority);
        int leastImportantIndex = -1;
        float leastImportanceValue = float.MaxValue;
        
        // find an available audio source to use
        for (int i = 0; i < _pool.Count; i++) {
            AudioPoolItem pi = _pool[i];

            if (!pi.Playing) {
                return ConfigurePoolObject(i, track, clip, position, volume, spatialBlend, unimportance, startTime, ignoreListenerPause);
            }
            if (pi.Unimportance > leastImportanceValue) {
                leastImportanceValue = pi.Unimportance;
                leastImportantIndex = i;
            }
        }
        
        if (leastImportanceValue > unimportance)
            return ConfigurePoolObject(leastImportantIndex, track, clip, position, volume, spatialBlend, unimportance, startTime, ignoreListenerPause);

        return 0;
    }

    public ulong PlaySound(AudioCollection collection, AudioClip clip, Vector3 position, float startTime = 0f, bool ignoreListenerPause = false) {
        return PlaySound(collection.AudioGroup, clip, position, collection.Volume,
            collection.SpatialBlend, collection.Priority, startTime, ignoreListenerPause);
    }

    public void StopSound(ulong id) {
        AudioPoolItem activeSound;
        if (_activePool.TryGetValue(id, out activeSound)) {
            activeSound.AudioSource.Stop();
            activeSound.AudioSource.clip = null;
            activeSound.GameObject.SetActive(false);
            _activePool.Remove(id);
            // make it available again
            activeSound.Playing = false;
        }
    }

    public IEnumerator PlaySound(string track, AudioClip clip, Vector3 position, float volume, float spatialBlend, float delay, int priority = 128) {
        yield return new WaitForSeconds(delay);
        PlaySound(track, clip, position, volume, spatialBlend, priority);
    }
    
    /// <summary>
    /// Sets the volume for a certain track. We can also set the transition(fade) time.
    /// </summary>
    /// <param name="track"></param>
    /// <param name="volume"></param>
    /// <param name="fadeTime"></param>
    public void SetTrackVolume(string track, float volume, float fadeTime) {
        TrackInfo trackInfo;
        if (_tracks.TryGetValue(track, out trackInfo)) {
            // stop any coroutine that might be in the middle of fading
            if (trackInfo.TrackFader != null) StopCoroutine(trackInfo.TrackFader);

            if (fadeTime == 0.0f)
                _mixer.SetFloat(track, volume);
            else {
                trackInfo.TrackFader = SetTrackVolumeInternal(track, volume, fadeTime);
                StartCoroutine(trackInfo.TrackFader);
            }
        }
    }
    
    /// <summary>
    /// Returns the volume of a certain track.
    /// </summary>
    /// <param name="track"></param>
    /// <returns></returns>
    public float GetTrackVolume(string track) {
        TrackInfo trackInfo;
        if (_tracks.TryGetValue(track, out trackInfo)) {
            float volume;
            _mixer.GetFloat(track, out volume);
            return volume;
        }

        return -1;
    }

    /// <summary>
    /// Returns Audio Group from the track name that we send as a parameter.
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public AudioMixerGroup GetAudioGroupFromTrackName(string name) {
        TrackInfo ti;
        if (_tracks.TryGetValue(name, out ti)) {
            return ti.Group;
        }

        return null;
    }

    public ILayeredAudioSource RegisterLayeredAudioSource(AudioSource source, int layers) {
        if (source != null && layers > 0) {
            // first check if the source doesn't already exist
            for (int i = 0; i < _layeredAudio.Count; i++) {
                var item = _layeredAudio[i];
                if (item.AudioSource == source)
                    return item;
            }
            
            // create a new layered audio item and add it to the list
            LayeredAudioSource newLayeredSource = new LayeredAudioSource(source, layers);
            _layeredAudio.Add(newLayeredSource);

            return newLayeredSource;
        }

        return null;
    }

    public void UnregisterLayeredAudioSource(ILayeredAudioSource source) {
        _layeredAudio.Remove((LayeredAudioSource)source);
    }

    public void UnregisterLayeredAudioSource(AudioSource source) {
        foreach (var item in _layeredAudio) {
            if (item.AudioSource == source) {
                _layeredAudio.Remove(item);
                break;
            }
        }
    }
}
