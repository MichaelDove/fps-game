﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreAudioListenerPause : MonoBehaviour {
    [SerializeField] private AudioSource _audioSource;

    private void Start() {
        _audioSource.ignoreListenerPause = true;
    }
}
