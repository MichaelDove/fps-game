﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioOnEnter : StateMachineBehaviour {
    [SerializeField] private AudioCollection _collection;
    [SerializeField] private int _bank;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        AudioManager.instance.PlaySound(_collection.AudioGroup, _collection[_bank], animator.transform.position,
            _collection.Volume, _collection.SpatialBlend, _collection.Priority);
    }
}
