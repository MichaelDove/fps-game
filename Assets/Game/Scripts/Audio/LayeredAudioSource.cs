﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioLayer {
    public AudioClip Clip = null;
    public AudioCollection Collection = null;
    public int Bank = 0;
    public bool Looping = false;
    public float Time = 0f;
    public float Duration = 0f;
    public bool Muted = false;
}

public interface ILayeredAudioSource {
    bool Play(AudioCollection pool, int bank, int layer, bool looping = true);
    void Stop(int layer);
    void Mute(int layer, bool mute);
    void Mute(bool mute);
}

public class LayeredAudioSource : ILayeredAudioSource {
    // Private variables
    private AudioSource _audioSource = null;
    private List<AudioLayer> _audioLayers = new List<AudioLayer>();
    private int _activeLayer = -1;
    
    // Public properties
    public AudioSource AudioSource => _audioSource;
    
    // Constructor
    public LayeredAudioSource(AudioSource source, int layers) {
        if (source != null && layers > 0) {
            _audioSource = source;
            
            // create requested amount of layers
            for (int i = 0; i < layers; i++) {
                // create new layer
                AudioLayer layer = new AudioLayer();
                // add to stack
                _audioLayers.Add(layer);
            }
        }
    }
    
    // starts playing a layer
    public bool Play(AudioCollection collection, int bank, int layer, bool looping = true) {
        if (layer >= _audioLayers.Count) return false;
        
        // fetch the layer we wish to config
        AudioLayer al = _audioLayers[layer];
        // if already doing what we want then just return true
        if (al.Collection == collection && al.Bank == bank && al.Looping == looping) return true;

        al.Collection = collection;
        al.Bank = bank;
        al.Looping = looping;
        al.Time = 0f;
        al.Duration = 0f;
        al.Muted = false;
        al.Clip = null;

        return true;
    }

    // stops a layer from playing
    public void Stop(int layer) {
        if (layer >= _audioLayers.Count) return;

        AudioLayer al = _audioLayers[layer];
        al.Looping = false;
        al.Time = al.Duration;
    }

    // mutes the specified layer
    public void Mute(int layer, bool mute) {
        if (layer >= _audioLayers.Count) return;
        
        AudioLayer al = _audioLayers[layer];
        al.Muted = mute;
    }
    
    // mutes all of the layers
    public void Mute(bool mute) {
        for (int i = 0; i < _audioLayers.Count; i++) {
            Mute(i, mute);
        }
    }

    public void Update() {
        int newActiveLayer = -1;
        bool refreshAudioSource = false;
        
        // Update the stack each frame by iterating through layers (backwards)
        for (int i = _audioLayers.Count - 1; i >= 0; i--) {
            AudioLayer al = _audioLayers[i];
            
            // ignore unassigned layers
            if (al.Collection == null) continue;
            
            // update the internal timer of the layer
            al.Time += Time.deltaTime;
            
            // if it has exceeded its duration, we need to take action
            if (al.Time > al.Duration) {
                if (al.Looping || al.Clip == null) {
                    AudioClip clip = al.Collection[al.Bank];
                    if (clip == al.Clip)
                        al.Time = al.Time % al.Clip.length;
                    else
                        al.Time = 0;

                    al.Duration = clip.length;
                    al.Clip = clip;
                    
                    if (newActiveLayer < i) {
                        newActiveLayer = i;
                        // we need to call the Play method
                        refreshAudioSource = true;
                    }
                } else {
                    al.Clip = null;
                    al.Collection = null;
                    al.Duration = 0f;
                    al.Bank = 0;
                    al.Looping = false;
                    al.Time = 0f;
                }
            } else {
                if (newActiveLayer < i) newActiveLayer = i;
            }
        }
        
        // if the new active layer is different from the previous
        if (newActiveLayer != _activeLayer || refreshAudioSource) {
            // we don't want to play anything
            if (newActiveLayer == -1) {
                _audioSource.Stop();
                _audioSource.clip = null;
            } else {
                // play a new audio source
                AudioLayer al = _audioLayers[newActiveLayer];

                _audioSource.clip = al.Clip;
                _audioSource.volume = al.Muted ? 0f : al.Collection.Volume;
                _audioSource.spatialBlend = al.Collection.SpatialBlend;
                _audioSource.time = al.Time;
                _audioSource.loop = false;
                _audioSource.outputAudioMixerGroup = AudioManager.instance.GetAudioGroupFromTrackName(al.Collection.AudioGroup);
                _audioSource.Play();
            }
        }

        _activeLayer = newActiveLayer;

        if (_activeLayer != -1 && _audioSource) {
            AudioLayer al = _audioLayers[_activeLayer];
            if (al.Muted) _audioSource.volume = 0f;
            else _audioSource.volume = al.Collection.Volume;
        }
    }
}
