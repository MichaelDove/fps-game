﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformShaker : MonoBehaviour {
    // Inspector assigned
    [SerializeField] private SharedVector3 _shakeVector;
    [SerializeField] private float _magnitudeScale = 1f;
    
    // Internals
    private Vector3 _localPosition = Vector3.zero;

    private void Start() {
        _localPosition = transform.localPosition;
    }

    private void Update() {
        transform.localPosition = _localPosition + (_shakeVector.Value * _magnitudeScale);
    }
}
