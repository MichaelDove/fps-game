﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionObjective : MonoBehaviour {
    // Inspector assigned
    [SerializeField] private float _gameEndDelay = 0.5f;

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            StartCoroutine(CompleteLevel(other));
        }
    }

    private IEnumerator CompleteLevel(Collider col) {
        yield return new WaitForSeconds(_gameEndDelay);
        
        // fade out
        PlayerInfo pi = GameSceneManager.instance.GetPlayerInfo(col.GetInstanceID());
        pi.CharacterManager.DoLevelComplete();
    }
}
