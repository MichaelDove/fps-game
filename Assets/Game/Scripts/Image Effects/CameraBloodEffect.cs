﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraBloodEffect : MonoBehaviour {
    // Inspector assigned
    [SerializeField] private Texture2D _bloodTexture;
    [SerializeField] private Texture2D _bloodTextureNormal;
    [SerializeField] private Shader _shader;

    [SerializeField] [Range(0,1)] private float _bloodAmount = 0f;
    [SerializeField] [Range(0,1)] private float _minBloodAmount = 0f;
    [SerializeField] private float _distortion = 1f;
    [SerializeField] private bool _autoFade = true;
    [SerializeField] private float _fadeSpeed = 0.05f;
    
    // Public variables
    public float BloodAmount {
        get => _bloodAmount;
        set => _bloodAmount = value;
    }

    public float MinBloodAmount {
        get => _minBloodAmount;
        set => _minBloodAmount = value;
    }

    public bool AutoFade {
        get => _autoFade;
        set => _autoFade = value;
    }

    public float FadeSpeed {
        get => _fadeSpeed;
        set => _fadeSpeed = value;
    }

    // Private variables
    private Material _material;

    private void Update() {
        if (_autoFade) {
            _bloodAmount -= _fadeSpeed * Time.deltaTime;
            _bloodAmount = Mathf.Max(_bloodAmount, _minBloodAmount);
        }
    }

    private void OnRenderImage(RenderTexture src, RenderTexture dest) {
        if (_material == null) {
            _material = new Material(_shader);
        }
        
        // send data to the shader
        _material.SetTexture("_BloodTex", _bloodTexture);
        _material.SetTexture("_BloodNorm", _bloodTextureNormal);
        _material.SetFloat("_Distortion", _distortion);
        _material.SetFloat("_BloodAmount", _bloodAmount);

        Graphics.Blit(src, dest, _material);
    }
}
