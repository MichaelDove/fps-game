﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class CameraNightVisionEffect : MonoBehaviour {
    // Inspector assigned
    [Header("Battery Power / Charge")] 
    public SharedFloat NightVisionPower;

    [Header("Color & Brightness")] 
    [Range(0f, 6f)] public float Contrast = 3f;
    [Range(0.1f, 1f)] public float Brightenss = 0.1f;
    public Color Hue = new Color();
    public GameObject Light;

    [Header("Scanline Configuration")] 
    public Texture2D ScanLineTextrue;
    [Range(0f, 3f)] public float ScanLineMultiplier = 1f;
    [Range(0f, 2f)] public float ScanLineOpacity = 1f;

    [Header("Noise Configuration")] 
    public Texture2D NoiseTexture;
    [Range(0f, 100f)] public float NoiseSpeed = 100f;
    [Range(0f, 1f)] public float NoiseOpacity = 0.3f;
    [Range(0.1f, 5f)] public float StartupTime = 2f;
    [Range(0f, 1f)] public float MaxQuality = 1f;

    [Header("Overlay Configuration")] 
    public Texture2D OverlayTexture;

    [Header("Lens Effects")] 
    [Range(-1f, 1f)] public float Distortion = 0.2f;

    public Shader _shader;
    
    // Internals
    private Material _material;
    private float _quality = 0f;

    private void OnEnable() {
        if (Light)
            Light.SetActive(true);
        _quality = 0f;
    }

    private void OnDisable() {
        if (Light)
            Light.SetActive(false);
    }

    private void OnRenderImage(RenderTexture src, RenderTexture dest) {
        if (!_material) {
            _material = new Material(_shader);
            _material.hideFlags = HideFlags.HideAndDontSave;
        }

        _quality = Mathf.Lerp(_quality, Mathf.Min(NightVisionPower.Value / 100f * 4f, MaxQuality), Time.deltaTime / StartupTime);
        
        _material.SetFloat("_Contrast", Contrast);
        _material.SetFloat("_Brightness", Brightenss);
        _material.SetFloat("_Distortion", Distortion);
        _material.SetFloat("_ScanlineMultiplier", ScanLineMultiplier);
        _material.SetFloat("_ScanlineOpacity", ScanLineOpacity);
        _material.SetFloat("_NoiseSpeed", NoiseSpeed);
        _material.SetFloat("_NoiseOpacity", NoiseOpacity);
        _material.SetFloat("_Quality", _quality);
        
        _material.SetColor("_Hue", Hue);

        _material.SetTexture("_OverlayTex", OverlayTexture);
        _material.SetTexture("_ScanlineTex", ScanLineTextrue);
        _material.SetTexture("_NoiseTex", NoiseTexture);
        
        Graphics.Blit(src, dest, _material);
    }
}
