﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(Camera))]
public class HighlightPrePassEffect : MonoBehaviour {
    [Header("Shaders")] 
    [SerializeField] private Shader _highlightBlurShader;
    [SerializeField] private Shader _highlightReplacementShader;
    
    // Internals
    private RenderTexture _prePassRT;
    private RenderTexture _blurredRT;
    private Material _blurMat;
    private InteractiveItem _highlightItem;
    private Camera _renderTextureCamera;
    
    // Properties
    public InteractiveItem HighlightItem {
        get => _highlightItem;
        set { 
            _highlightItem = value;
            if (_highlightItem != null) {
                Shader.SetGlobalColor("_GlowColor", _highlightItem.HighlightColor);
            } else {
                Shader.SetGlobalColor("GlowColor", Color.black);  
            }
        } 
    }

    private void OnEnable() {
        _renderTextureCamera = GetComponent<Camera>();
        _renderTextureCamera.SetReplacementShader(_highlightReplacementShader, null);
        CreateRenderTargets();
        
        // create material for blur
        _blurMat = new Material(_highlightBlurShader);
    }

    private void OnDisable() {
        _renderTextureCamera.targetTexture = null;
        // release any previously allocated render textures
        if (_prePassRT)
            _prePassRT.Release();
        if (_blurredRT)
            _blurredRT.Release();
        if (_blurMat)
            Destroy(_blurMat);
    }

    private void CreateRenderTargets() {
        // release any previously allocated render textures
        if (_prePassRT)
            _prePassRT.Release();
        if (_blurredRT)
            _blurredRT.Release();
        
        // create render textures
        _prePassRT = new RenderTexture(Screen.width, Screen.height, 24);
        _blurredRT = new RenderTexture(Screen.width, Screen.height, 0);
        _prePassRT.antiAliasing = QualitySettings.antiAliasing;
        
        // set pre pass texture as target
        _renderTextureCamera.targetTexture = _prePassRT;
        Shader.SetGlobalTexture("_GlowPrePassTex", _prePassRT);
        Shader.SetGlobalTexture("_GlowBlurredTex", _blurredRT);
    }

    private void OnPreCull() {
        if (Screen.width != _prePassRT.width || Screen.height != _prePassRT.height)
            CreateRenderTargets();

        if (_highlightItem) {
            var itemsToHighlight = _highlightItem.HighlightObjects;
            foreach (var info in itemsToHighlight) {
                info.Object.layer = LayerMask.NameToLayer("Highlighted");
            }
        }
    }

    private void OnPostRender() {
        if (_highlightItem) {
            var itemsToHighlight = _highlightItem.HighlightObjects;
            foreach (var info in itemsToHighlight) {
                info.Object.layer = info.Layer;
            }
        }
    }

    private void OnRenderImage(RenderTexture src, RenderTexture dest) {
        Graphics.Blit(src, dest);
        Graphics.SetRenderTarget(_blurredRT);
        GL.Clear(false, true, Color.clear);

        if (_highlightItem != null && _highlightItem.HighlightObjects.Count > 0) {
            Graphics.Blit(src, _blurredRT);
            _blurMat.SetVector("_BlurSize", new Vector2(_blurredRT.texelSize.x * _highlightItem.HighlightBlurSize, _blurredRT.texelSize.y * _highlightItem.HighlightBlurSize));

            for (int i = 0; i < _highlightItem.HighlightBlurIterations; i++) {
                // create temp render texture
                var tempRT = RenderTexture.GetTemporary(_blurredRT.width, _blurredRT.height);
                Graphics.Blit(_blurredRT, tempRT, _blurMat, 0);
                Graphics.Blit(tempRT, _blurredRT, _blurMat, 1);
                RenderTexture.ReleaseTemporary(tempRT);
            }
        }
    }
}
