﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class HighlightCompositeEffect : MonoBehaviour {
    // Inspector assigned
    public Shader HighlightCompositeShader;
    [Range(0, 10)] public float Intensity = 2;
    
    // Internals
    private Material _compositeMat;

    private void OnEnable() {
        _compositeMat = new Material(HighlightCompositeShader);
    }

    private void OnDisable() {
        Destroy(_compositeMat);
    }

    private void OnRenderImage(RenderTexture src, RenderTexture dest) {
        _compositeMat.SetFloat("_Intensity", Intensity);
        Graphics.Blit(src, dest, _compositeMat);
    }
}
