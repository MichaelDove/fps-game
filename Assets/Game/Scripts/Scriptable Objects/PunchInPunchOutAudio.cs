﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PunchInPunchOutAudioInfo {
    public AudioClip Clip;
    public float StartTime;
    public float EndTime;
}

[CreateAssetMenu(fileName = "New Punch-In Punch-Out Audio")]
public class PunchInPunchOutAudio : ScriptableObject {
    // Inspector assigned
    [SerializeField] private List<PunchInPunchOutAudioInfo> dataList = new List<PunchInPunchOutAudioInfo>();
    
    // Internal
    private Dictionary<AudioClip, PunchInPunchOutAudioInfo> dataDictionary = new Dictionary<AudioClip, PunchInPunchOutAudioInfo>();

    private void OnEnable() {
        foreach (var info in dataList) {
            dataDictionary[info.Clip] = info;
        }
    }

    public PunchInPunchOutAudioInfo GetClipInfo(AudioClip clip) {
        if (dataDictionary.ContainsKey(clip)) {
            return dataDictionary[clip];
        }

        return null;
    }
}
