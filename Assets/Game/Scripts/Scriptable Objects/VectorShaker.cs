﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/VectorShaker", fileName="new Vector Shaker")]
public class VectorShaker : ScriptableObject {
    // Inspector assigned
    [SerializeField] private SharedVector3 _shakeVector = null;
    
    // Internals
    private IEnumerator _coroutine;

    public void ShakeVector(float duration, float magnitude, float damping = 1.0f) {
        if (duration < 0.001f || magnitude.Equals(0f)) return;
        
        if (_coroutine != null)
            CoroutineRunner.instance.StopCoroutine(_coroutine);

        _coroutine = Shake(duration, magnitude, damping);
        CoroutineRunner.instance.StartCoroutine(_coroutine);
    }
    
    private IEnumerator Shake(float duration, float magnitude, float damping) {
        float time = 0;

        while (time < duration) {
            // random offsets
            float x = Random.Range(-1.0f, 1.0f) * magnitude;
            float y = Random.Range(-1.0f, 1.0f) * magnitude;
            
            // new vector from offsets
            Vector3 unsmoothedVector = new Vector3(x, y, 0f);
            _shakeVector.Value = Vector3.Lerp(unsmoothedVector, Vector3.zero, (time / duration) * damping);

            time += Time.deltaTime;
            yield return null;
        }
        
        _shakeVector.Value = Vector3.zero;
        _coroutine = null;
    }
}
