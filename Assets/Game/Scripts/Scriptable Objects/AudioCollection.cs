﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ClipBank {
    public List<AudioClip> Clips = new List<AudioClip>();
}

[CreateAssetMenu(fileName = "New Audio Collection")]
public class AudioCollection : ScriptableObject {
    // Inspector assigned
    [SerializeField] private string _audioGroup;
    [SerializeField] [Range(0f, 1f)] private float _volume = 1.0f;
    [SerializeField] [Range(0f, 1f)] private float _spatialBlend = 1.0f;
    [SerializeField] [Range(0, 256)] private int _priority = 128;
    [SerializeField] private List<ClipBank> _audioClipBanks = new List<ClipBank>();
    
    // Getters
    public string AudioGroup => _audioGroup;
    public float Volume => _volume;
    public float SpatialBlend => _spatialBlend;
    public int Priority => _priority;
    public int BankCount => _audioClipBanks.Count;
    
    // returns a random clip from the specified clip bank (AudioCollection temp; temp[0] -> returns random clip from first bank)
    public AudioClip this[int i] {
        get {
            if (_audioClipBanks == null || _audioClipBanks.Count <= i) return null;
            if (_audioClipBanks[i].Clips.Count == 0) return null;

            List<AudioClip> clipList = _audioClipBanks[i].Clips;
            AudioClip clip = clipList[Random.Range(0, clipList.Count)];
            return clip;
        }
    }
    
    // returns a random clip from the first clip bank
    public AudioClip audioClip {
        get {
            if (_audioClipBanks == null || _audioClipBanks.Count == 0) return null;
            if (_audioClipBanks[0].Clips.Count == 0) return null;

            List<AudioClip> clipList = _audioClipBanks[0].Clips;
            AudioClip clip = clipList[Random.Range(0, clipList.Count)];
            return clip;
        }
    }
}
