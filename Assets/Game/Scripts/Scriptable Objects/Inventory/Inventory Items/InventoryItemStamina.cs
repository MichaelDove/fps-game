﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName =  "Scriptable Objects/Inventory System/Items/Stamina", fileName = "New Stamina Inventory Item")]
public class InventoryItemStamina : InventoryItem {
    // Inspector assigned
    [Header("Stamina Properties")] 
    [SerializeField] [Range(0.0f, 100f)] protected float _boostAmount = 0f;

    [Header("Shared variables")] 
    [SerializeField] protected SharedFloat _recipient;
    
    // Public properties
    public float HealingAmount => _boostAmount;

    public override InventoryItem Use(Vector3 position, bool playAudio = true, object contextData = null, Inventory inventory = null) {
        // add health
        _recipient.Value += _boostAmount;

        // call base class for default sound processing
        return base.Use(position, playAudio, inventory);
    }

    public override string GetStatString(object contextData = null) {
        return "Stamina boost amount: " + _boostAmount + "%";
    }
}
