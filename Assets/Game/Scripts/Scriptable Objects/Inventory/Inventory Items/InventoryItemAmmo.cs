﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Inventory System/Items/Ammunition", fileName = "New Inventory Item Ammo")]
public class InventoryItemAmmo : InventoryItem {
    // Inspector assigned
    [Header("Ammo Properties")] 
    [SerializeField] protected int _capacity = 0;
    
    // Getters
    public int Capacity => _capacity;
}
