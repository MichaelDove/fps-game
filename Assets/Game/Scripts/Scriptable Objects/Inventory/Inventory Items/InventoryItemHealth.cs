﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName =  "Scriptable Objects/Inventory System/Items/Health", fileName = "New Health Inventory Item")]
public class InventoryItemHealth : InventoryItem {
    // Inspector assigned
    [Header("Health Properties")] 
    [SerializeField] [Range(0.0f, 100f)] protected float _healingAmount = 0f;

    [Header("Shared variables")] 
    [SerializeField] protected SharedFloat _recipient;
    
    // Public properties
    public float HealingAmount => _healingAmount;

    public override InventoryItem Use(Vector3 position, bool playAudio = true, object contextData = null, Inventory inventory = null) {
        // add health
        _recipient.Value = Mathf.Min(_recipient.Value + _healingAmount, 100f);

        // call base class for default sound processing
        return base.Use(position, playAudio, inventory);
    }
    
    public override string GetStatString(object contextData = null) {
        return "Health boost: " + _healingAmount + "%";
    }
}
