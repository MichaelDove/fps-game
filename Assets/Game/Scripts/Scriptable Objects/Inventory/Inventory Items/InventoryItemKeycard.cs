﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Inventory System/Items/Keycard", fileName = "New Inventory Item Keycard")]
public class InventoryItemKeycard : InventoryItem {
    
    public override CollectableItem Drop(Vector3 position, bool playAudio = true, object contextData = null) {
        var item =  base.Drop(position, playAudio, contextData);

        if (item)
            item.InventoryItem = this;

        return item;
    }
}
