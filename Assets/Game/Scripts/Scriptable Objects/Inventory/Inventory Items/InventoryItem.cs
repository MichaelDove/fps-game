﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// public enums that categorize item behaviours
public enum InventoryItemType {None, Ammunition, Consumable, Knowledge, Recording, Weapon}
public enum InventoryWeaponType {None, SingleHanded, DoubleHanded}
public enum InventoryWeaponAttackType {None, Melee, Ammunition}
public enum InventoryWeaponReloadType {None, Partial, NonePartial}
public enum InventoryAction {None, Consume, Reload}

[CreateAssetMenu(menuName = "Scriptable Objects/Inventory System/Items/Base")]
public class InventoryItem : ScriptableObject {
    // Inspector assigned
    [Header("General Properties")] [SerializeField] protected string _pickupText = "Press 'use' to pick up";
    [SerializeField] protected string _inventoryName;
    [SerializeField] protected Sprite _inventoryImage;
    [SerializeField] protected InventoryAction _inventoryAction = InventoryAction.None;
    [SerializeField] protected string _inventoryActionText;
    [SerializeField] protected CollectableItem _collectableItem;
    [SerializeField] protected InventoryItem _replacementItem;
    [SerializeField] [TextArea(5, 10)] protected string _description;
    [SerializeField] protected InventoryItemType _category = InventoryItemType.None;
    [SerializeField] protected AudioCollection _audio;

    // Getters
    public string InventoryName => _inventoryName;
    public Sprite InventoryImage => _inventoryImage;
    public InventoryAction InventoryAction => _inventoryAction;
    public string PickupText => _pickupText;
    public InventoryItemType Category => _category;
    public string InventoryActionText => string.IsNullOrEmpty(_inventoryActionText) ? _inventoryAction.ToString() : _inventoryActionText;
    public AudioCollection AudioCollection => _audio;

    public virtual void Pickup(Vector3 position, bool playAudio = true, object contextData = null) {
        if (playAudio) {
            AudioClip clip = _audio[0];
            AudioManager.instance.PlaySound(_audio, clip, position);
        }
    }

    public virtual CollectableItem Drop(Vector3 position, bool playAudio = true, object contextData = null) {
        if (playAudio) {
            AudioClip clip = _audio[1];
            AudioManager.instance.PlaySound(_audio, clip, position, 0f, true);
        }

        if (_collectableItem != null) {
            CollectableItem go = Instantiate(_collectableItem);
            go.transform.position = position;
            return go;
        }
        
        return null;
    }

    public virtual InventoryItem Use(Vector3 position, bool playAudio = true, object contextData = null, Inventory inventory = null) {
        if (playAudio) {
            var clip = _audio[2];
            AudioManager.instance.PlaySound(_audio, clip, position, 0f, true);
        }

        return _replacementItem;
    }

    public virtual object GetContextData(CollectableItem battery) {
        return null;
    }

    public virtual Type GetContextDataType() {
        return null;
    }

    public virtual string GetDescription(object contextData = null) {
        return _description;
    }
    
    public virtual GameObject OnRenderMount (Transform parent, object contextData) {
        return null;
    }

    public virtual string GetStatString(object contextData = null) {
        return null;
    }
}
