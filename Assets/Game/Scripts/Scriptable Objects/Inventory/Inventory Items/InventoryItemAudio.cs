﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TimedStateKey {
    public float Time;
    public string Key;
    public string Value;
    public string UIMessage;
}

[System.Serializable]
public class TimedCaptionKey {
    public float Time;
    [TextArea(3, 10)] public string Text = null;
}

[CreateAssetMenu(menuName = "Scriptable Objects/Inventory System/Items/Audio", fileName = "New Inventory Audio Item")]
public class InventoryItemAudio : InventoryItem {
    // Inspector assigned
    [Header("Audio log properties")] 
    [SerializeField] private string _person;
    [SerializeField] private string _subject;
    [SerializeField] private Texture2D _image;
    
    [Header("State change keys")]
    [SerializeField] private List<TimedStateKey> _stateKeys = new List<TimedStateKey>();
    
    [Header("Caption Keys")]
    [SerializeField] private List<TimedCaptionKey> _captionKeys = new List<TimedCaptionKey>();
    
    // Public properties
    public string Person => _person;
    public string Subject => _subject;
    public Texture2D Image => _image;
    public List<TimedStateKey> StateKeys => _stateKeys;
    public List<TimedCaptionKey> CaptionKeys => _captionKeys;

    public override InventoryItem Use(Vector3 position, bool playAudio = true, object contextData = null, Inventory inventory = null) {
        return null;
    }
    
    public override string GetStatString(object contextData = null) {
        return _person + ": \n" + _subject;
    }
}
