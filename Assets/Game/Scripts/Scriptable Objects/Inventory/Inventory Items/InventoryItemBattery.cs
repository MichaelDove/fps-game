﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class BatteryItemInfo {
    public float RemainingCharge = 100f;
}

[CreateAssetMenu(menuName = "Scriptable Objects/Inventory System/Items/Battery", fileName = "New Inventory Item Battery")]
public class InventoryItemBattery : InventoryItem {
    [Header("Shared variables")] 
    [SerializeField] protected SharedFloat _recepient;
    [Header("Custom UI Prefab")]
    [SerializeField] protected Slider _batterySlider;

    public override object GetContextData(CollectableItem battery) {
        return new BatteryItemInfo() {RemainingCharge = (battery as CollectableBattery).Charge};
    }

    public override CollectableItem Drop(Vector3 position, bool playAudio = true, object contextData = null) {
        CollectableBattery objectToDrop = base.Drop(position,playAudio) as CollectableBattery;
        objectToDrop.Charge = (contextData as BatteryItemInfo).RemainingCharge;

        return objectToDrop;
    }

    public override InventoryItem Use(Vector3 position, bool playAudio = true, object contextData = null, Inventory inventory = null) {
        if (_recepient && contextData != null) {
            float v = _recepient.Value;
            _recepient.Value = Mathf.Min((contextData as BatteryItemInfo).RemainingCharge, 100f);
            (contextData as BatteryItemInfo).RemainingCharge = v;
        }
        
        return base.Use(position, playAudio, contextData, inventory);
    }

    public override string GetDescription(object contextData = null) {
        return _description + "\n current charge: " + Mathf.FloorToInt((contextData as BatteryItemInfo).RemainingCharge) + "%";
    }

    public override GameObject OnRenderMount(Transform parent, object contextData) {
        if (_batterySlider) {
            // instantiate the slider
            var slider = Instantiate(_batterySlider);

            var rtSlider = slider.GetComponent<RectTransform>();
            var rtPrefab = _batterySlider.GetComponent<RectTransform>();

            rtSlider.SetParent(parent);
            rtSlider.localPosition = rtPrefab.localPosition;
            rtSlider.localScale = new Vector3(1,1,1);

            var info = contextData as BatteryItemInfo;
            if (info != null) {
                slider.value = info.RemainingCharge;
            }

            return slider.gameObject;
        }
        
        return null;
    }

    public override Type GetContextDataType() {
        return typeof(BatteryItemInfo);
    }
    
    public override string GetStatString(object contextData = null) {
        return "Remaining battery charge: " + Mathf.FloorToInt((contextData as BatteryItemInfo).RemainingCharge) + "%";
    }
}
