﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

// Shake Type
public enum WeaponShakeType {OnFire, OnHit}

[CreateAssetMenu(menuName = "Scriptable Objects/Inventory System/Items/Weapon")]
public class InventoryItemWeapon : InventoryItem {
    // Inspector assigned
    [Header("Weapon properties")] [SerializeField] protected InventoryWeaponType _weaponType = InventoryWeaponType.None;
    [SerializeField] protected InventoryWeaponAttackType _attackType = InventoryWeaponAttackType.None;
    [SerializeField] protected InventoryItemAmmo _ammo;
    [SerializeField] protected InventoryWeaponReloadType _reloadType = InventoryWeaponReloadType.None;
    [SerializeField] protected int _ammoCapacity; //if the gun is partial reload
    [SerializeField] protected float _range = 0f;
    [SerializeField] [Range(0, 20)] protected float _soundRadius = 5f;
    [SerializeField] protected bool _automatic = false;
    [FormerlySerializedAs("_dualMode")] [SerializeField] protected bool _scopeMode = false;
    [SerializeField] protected float _rayRadius = 0f;
    [SerializeField] protected int _headDamage = 100;
    [SerializeField] protected int _bodyDamage = 20;
    [SerializeField] protected AnimationCurve _damageAttenuation = new AnimationCurve();
    [SerializeField] protected float _force = 100f;
    [SerializeField] protected AnimationCurve _forceAttenuation = new AnimationCurve();
    
    [Header("FPS Arms system properties")]
    [SerializeField] protected int _weaponAnim = -1;
    [SerializeField] protected int _attackAnimCount = 1;

    [Header("Camera Settings")] 
    [SerializeField] protected float _scopeModeFOV = 50f;
    [SerializeField] protected Sprite _crosshair;

    [Header("Camera Effects")] 
    [SerializeField] protected WeaponShakeType _shakeType = WeaponShakeType.OnFire;
    [SerializeField] protected float _shakeDuration = 0f;
    [SerializeField] protected float _shakeMagnitude = 0f;
    [SerializeField] protected float _shakeDamping = 1f;
    
    // Public properties
    public InventoryWeaponType Type => _weaponType;
    public InventoryWeaponAttackType AttackType => _attackType;
    public InventoryWeaponReloadType ReloadType => _reloadType;
    public InventoryItemAmmo Ammo => _ammo;
    public float Range => _range;
    public float SoundRadius => _soundRadius;
    public bool Automatic => _automatic;
    public bool ScopeMode => _scopeMode;
    public int HeadDamage => _headDamage;
    public int BodyDamage => _bodyDamage;
    public float RayRadius => _rayRadius;
    public float Force => _force;
    public int WeaponAnim => _weaponAnim;
    public int AttackAnimCount => _attackAnimCount;
    public float ScopeModeFov => _scopeModeFOV;
    public Sprite Crosshair => _crosshair;
    public float ShakeDamping => _shakeDamping;
    public float ShakeDuration => _shakeDuration;
    public float ShakeMagnitude => _shakeMagnitude;
    public WeaponShakeType ShakeType => _shakeType;
    

    public int AmmoCapacity {
        get {
            switch (_reloadType) {
                case InventoryWeaponReloadType.None: return 0;
                case InventoryWeaponReloadType.Partial: return _ammoCapacity;
                case InventoryWeaponReloadType.NonePartial: return _ammo.Capacity;
            }

            return -1;
        }
    }

    public int GetAttenuatedDamage(string bodyPart, float dist) {
        var normalDist = Mathf.Clamp(dist / _range, 0f, 1f);
        if (bodyPart.Equals("Head"))
            return (int) (_damageAttenuation.Evaluate(normalDist) * _headDamage);
        
        return (int) (_damageAttenuation.Evaluate(normalDist) * _bodyDamage);
    }
    
    public int GetAttenuatedForce(float dist) {
        var normalDist = Mathf.Clamp(dist / _range, 0f, 1f);
        
        return (int) (_forceAttenuation.Evaluate(normalDist) * _force);
    }
}
