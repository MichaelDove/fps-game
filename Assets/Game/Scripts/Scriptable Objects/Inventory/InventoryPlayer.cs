﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements;

[CreateAssetMenu(menuName = "Scriptable Objects/Inventory System/Player Inventory", fileName = "New Player Inventory")]
public class InventoryPlayer : Inventory, ISerializationCallbackReceiver {
    // Custom inspector properties
    [HideInInspector] public bool WeaponMountsExpanded = false;
    [HideInInspector] public bool AmmoMountsExpanded = false;
    [HideInInspector] public bool BackpackMountsExpanded = false;
    [HideInInspector] public bool AudioLogsMountsExpanded = false;

    // Inspector assigned
    [Header("Mount Configuration and Starting Items")]
    public List<WeaponMountInfo> _weaponMounts = new List<WeaponMountInfo>();
    public List<AmmoMountInfo> _ammoMounts = new List<AmmoMountInfo>();
    public List<BackpackMountInfo> _backpackMounts = new List<BackpackMountInfo>();

    [Header("Audio recordings")] 
    public List<InventoryItemAudio> _audioRecordings = new List<InventoryItemAudio>();
    
    [Header("Shared variables")]
    [SerializeField] protected SharedTimeStringQueue _notificationQueue;
    [SerializeField] protected SharedVector3 _playerPosition;
    [SerializeField] protected SharedVector3 _playerDirection;

    // Private variables
    protected List<WeaponMountInfo> _weapons = new List<WeaponMountInfo>();
    protected List<AmmoMountInfo> _ammo = new List<AmmoMountInfo>();
    protected List<BackpackMountInfo> _backpack = new List<BackpackMountInfo>();
    protected List<InventoryItemAudio> _recordings = new List<InventoryItemAudio>();
    
    // the index of a recording currently being played
    protected int _activeAudioRecordingIndex = -1;
    
    
    public override WeaponMountInfo GetWeapon(int mountIndex) {
        if (mountIndex < 0 || mountIndex > _weapons.Count) return null;
        return _weapons[mountIndex];
    }

    public override AmmoMountInfo GetAmmo(int mountIndex) {
        if (mountIndex < 0 || mountIndex > _ammo.Count) return null;
        return _ammo[mountIndex];
    }

    public override BackpackMountInfo GetBackpack(int mountIndex) {
        if (mountIndex < 0 || mountIndex > _backpack.Count) return null;
        return _backpack[mountIndex];
    }

    // AUDIO RECORDING methods
    public override InventoryItemAudio GetAudioRecording(int recordingIndex) {
        if (recordingIndex < 0 || recordingIndex >= _recordings.Count) return null;
        return _recordings[recordingIndex];
    }

    public override int GetActiveAudioRecording() {
        return _activeAudioRecordingIndex;
    }

    public override int GetAudioRecordingCount() {
        return _recordings.Count;
    }

    public override bool PlayAudioRecording(int recordingIndex) {
        if (recordingIndex < 0 || recordingIndex >= _recordings.Count) return false;

        var audioPlayer = InventoryAudioPlayer.instance;
        audioPlayer.OnEndAudio.RemoveListener(StopAudioListener);
        audioPlayer.OnEndAudio.AddListener(StopAudioListener);
        
        audioPlayer.PlayAudio(_recordings[recordingIndex]);
        _activeAudioRecordingIndex = recordingIndex;
        
        return true;
    }

    public override void StopAudioRecording() {
        var audioPlayer = InventoryAudioPlayer.instance;
        audioPlayer.StopAudio();
    }
    
    // listener method
    protected void StopAudioListener() {
        var audioPlayer = InventoryAudioPlayer.instance;
        audioPlayer.OnEndAudio.RemoveListener(StopAudioListener);
        _activeAudioRecordingIndex = -1;
    }

    public override void DropAmmoItem(int mountIndex, bool playAudio = true) {
        var itemInfo = _ammo[mountIndex];
        if (itemInfo.Ammo == null) return;

        var pos = _playerPosition.Value;
        pos += _playerDirection.Value;
        CollectableAmmo sceneAmmo = itemInfo.Ammo.Drop(pos, playAudio) as CollectableAmmo;

        // we need to transfer the data to the new object
        if (sceneAmmo)
            sceneAmmo.Ammo = _ammo[mountIndex].AmmoRounds;

        _ammo[mountIndex].Ammo = null;
        _ammo[mountIndex].AmmoRounds = 0;
    }

    public override void DropBackpackItem(int mountIndex, bool playAudio = true) {
        var itemInfo = _backpack[mountIndex];
        if (itemInfo.Item == null) return;

        var pos = _playerPosition.Value;
        pos += _playerDirection.Value;
        itemInfo.Item.Drop(pos, playAudio, itemInfo.ContextData);

        _backpack[mountIndex].Item = null;
        _backpack[mountIndex].ContextData = null;
        _backpack[mountIndex].ContextDataString = null;
    }

    public override void DropWeaponItem(int mountIndex, bool playAudio = true) {
        var itemInfo = _weapons[mountIndex];
        if (itemInfo.Weapon == null) return;

        var pos = _playerPosition.Value;
        pos += _playerDirection.Value;
        var sceneWeapon = itemInfo.Weapon.Drop(pos, playAudio) as CollectableWeapon;

        // we need to transfer the data to the new object
        if (sceneWeapon) {
            sceneWeapon.Ammo = itemInfo.InGunAmmo;
        }

        OnWeaponDrop.Invoke(itemInfo.Weapon);
        _weapons[mountIndex].Weapon = null;
    }

    public override bool UseBackpackItem(int mountIndex, bool playAudio = true) {
        var backpackInfo = _backpack[mountIndex];

        if (backpackInfo.Item == null) return false;
        
        var backpackItem = backpackInfo.Item;
        var replacement = backpackItem.Use(Vector3.zero, playAudio, backpackInfo.ContextData);

        // replace the item with the replacement
        _backpack[mountIndex].Item = replacement;

        if (replacement == null) {
            _backpack[mountIndex].ContextData = null;
        }
        
        return true;
    }

    public override bool ReloadWeapon(int mountIndex, bool playAudio = true) {
        var wmi = _weapons[mountIndex];
        var weapon = wmi.Weapon;
        var ammo = weapon.Ammo;

        if (ammo == null || wmi.InGunAmmo >= weapon.AmmoCapacity) return false;
        
        int ammoMount = -1;
        for (int i = 0; i < _ammo.Count; i++) {
            if (_ammo[i].Ammo == ammo) ammoMount = i;
        }

        if (ammoMount == -1) return false;
        
        int neededAmmo = weapon.AmmoCapacity - wmi.InGunAmmo;
        if (neededAmmo < 1) return false;
        
        if (_ammo[ammoMount].AmmoRounds >= neededAmmo) {
            wmi.InGunAmmo += neededAmmo;
            _ammo[ammoMount].AmmoRounds -= neededAmmo;
        }
        else {
            wmi.InGunAmmo += _ammo[ammoMount].AmmoRounds;
            _ammo[ammoMount].Ammo = null;
        }
        // reload sound
        weapon.Use(Vector3.zero, playAudio);

        if (weapon.ReloadType == InventoryWeaponReloadType.NonePartial) {
            // simulate the clip we remove from the gun
            var pos = _playerPosition.Value;
            pos += _playerDirection.Value;
            var sceneAmmo = ammo.Drop(pos, playAudio) as CollectableAmmo;
            sceneAmmo.Ammo = 0;
        }

        return true;
    }

    public override bool AddItem(CollectableItem collectableItem) {
        var invItem = collectableItem.InventoryItem;

        switch (invItem.Category) {
            case InventoryItemType.Consumable:
                return AddBackpackItem(invItem, collectableItem);
            case InventoryItemType.Weapon:
                return AddWeaponItem(invItem as InventoryItemWeapon, collectableItem as CollectableWeapon);
            case InventoryItemType.Ammunition:
                return AddAmmoItem(invItem as InventoryItemAmmo, collectableItem as CollectableAmmo);
            case InventoryItemType.Recording:
                return AddRecordingItem(invItem as InventoryItemAudio, collectableItem as CollectableAudio);
        }
        
        return false;
    }
    
    // Private helper methods
    private bool AddBackpackItem(InventoryItem inventoryItem, CollectableItem collectableItem) {
        // search for empty backpack mount
        for (int i = 0; i < _backpack.Count; i++) {
            if (_backpack[i].Item == null) {
                // we found an empty mount
                _backpack[i].Item = inventoryItem;
                _backpack[i].ContextData = inventoryItem.GetContextData(collectableItem);
                inventoryItem.Pickup(collectableItem.transform.position, true, _backpack[i].ContextData);
                
                // broadcast that the pickup was successful
                _notificationQueue.Enqueue("Added " + inventoryItem.InventoryName + " to backpack");
                return true;
            }
        }
        
        _notificationQueue.Enqueue("Could not pick up the item, backpack is full!");
        return false;
    }

    private bool AddBackpackItem(BackpackMountInfo bmi, Vector3 position, bool playAudio) {
        // search for empty backpack mount
        for (int i = 0; i < _backpack.Count; i++) {
            if (_backpack[i].Item == null) {
                // we found an empty mount
                _backpack[i].Item = bmi.Item;
                _backpack[i].ContextData = bmi.ContextData;
                bmi.Item.Pickup(position, playAudio, _backpack[i].ContextData);
                
                // broadcast that the pickup was successful
                _notificationQueue.Enqueue("Added " + bmi.Item.InventoryName + " to backpack");
                return true;
            }
        }
        
        _notificationQueue.Enqueue("Could not pick up the item, backpack is full!");
        return false;
    }
    
    private bool AddAmmoItem(InventoryItemAmmo inventoryAmmo, CollectableAmmo collectableAmmo) {
        // search for ammo of the same type
        for (int i = 0; i < _ammo.Count; i++) {
            if (_ammo[i].Ammo == inventoryAmmo) {
                _ammo[i].AmmoRounds += collectableAmmo.Ammo;
                inventoryAmmo.Pickup(collectableAmmo.transform.position);
                
                // broadcast that the pickup was successful
                _notificationQueue.Enqueue("Added " + inventoryAmmo.InventoryName + " to backpack");
                return true;
            }
        }
        
        // if we dont yet have this type of ammo, search for empty mount
        for (int i = 0; i < _ammo.Count; i++) {
            if (_ammo[i].Ammo == null) {
                // we found an empty mount
                _ammo[i].Ammo = inventoryAmmo;
                _ammo[i].AmmoRounds = collectableAmmo.Ammo;
                inventoryAmmo.Pickup(collectableAmmo.transform.position);

                // broadcast that the pickup was successful
                _notificationQueue.Enqueue("Added " + inventoryAmmo.InventoryName + " to backpack");
                return true;
            }
        }
        
        _notificationQueue.Enqueue("Could not pick up the ammo, ammo belt is full!");
        return false;
    }

    private bool AddAmmoItem(AmmoMountInfo ami, Vector3 position, bool playAudio) {
        // search for ammo of the same type
        for (int i = 0; i < _ammo.Count; i++) {
            if (_ammo[i].Ammo == ami.Ammo) {
                _ammo[i].AmmoRounds += ami.AmmoRounds;
                ami.Ammo.Pickup(position, playAudio);
                
                // broadcast that the pickup was successful
                _notificationQueue.Enqueue("Added " + ami.Ammo.InventoryName + " to backpack");
                return true;
            }
        }
        
        // if we dont yet have this type of ammo, search for empty mount
        for (int i = 0; i < _ammo.Count; i++) {
            if (_ammo[i].Ammo == null) {
                // we found an empty mount
                _ammo[i].Ammo = ami.Ammo;
                _ammo[i].AmmoRounds = ami.AmmoRounds;
                ami.Ammo.Pickup(position);

                // broadcast that the pickup was successful
                _notificationQueue.Enqueue("Added " + ami.Ammo.InventoryName + " to backpack");
                return true;
            }
        }
        
        _notificationQueue.Enqueue("Could not pick up the ammo, ammo belt is full!");
        return false;
    }
    
    private bool AddRecordingItem(InventoryItemAudio invAudio, CollectableAudio audio) {
        invAudio.Pickup(audio.transform.position);
        _recordings.Add(invAudio);
        // play audio
        PlayAudioRecording(_recordings.Count - 1);
        
        _notificationQueue.Enqueue("Audio recording added");
        return true;
    }
    
    private bool AddRecordingItem(AudioMountInfo ami, Vector3 position, bool playAudio) {
        ami.Audio.Pickup(position);
        _recordings.Add(ami.Audio);
        // play audio
        PlayAudioRecording(_recordings.Count - 1);
        
        _notificationQueue.Enqueue("Audio recording added");
        return true;
    }

    private bool AddWeaponItem(InventoryItemWeapon inventoryWeapon, CollectableWeapon collectableWeapon) {
        var weaponMountInfo = new WeaponMountInfo();
        weaponMountInfo.Weapon = inventoryWeapon;
        weaponMountInfo.InGunAmmo = collectableWeapon.Ammo;
        
        // invoke the event, so the object in charge of the weapon animation can start the process of switching the weapon
        OnWeaponChange.Invoke(weaponMountInfo);
        ApplicationManager.instance.SetGameState("FirstWeapon", "true");
        
        return true;
    }

    public override void AssignWeapon(int mountIndex, WeaponMountInfo mountInfo) {
        _weapons[mountIndex] = mountInfo;
        _weapons[mountIndex].Weapon.Pickup(_playerPosition.Value);
        
        _notificationQueue.Enqueue("New weapon: " + mountInfo.Weapon.InventoryName);
    }

    public override int GetAvailableAmmo(InventoryItemAmmo ammo, AmmoAmountRequestType requestType = AmmoAmountRequestType.NoWeaponAmmo) {
        int ammoCount = 0;

        if (requestType != AmmoAmountRequestType.WeaponAmmo) {
            foreach (var ammoMount in _ammo) {
                if (ammoMount.Ammo == ammo) {
                    ammoCount += ammoMount.AmmoRounds;
                    break;
                }
            }
        }

        if (requestType != AmmoAmountRequestType.NoWeaponAmmo) {
            foreach (var weaponMount in _weapons) { 
                if (weaponMount.Weapon != null && weaponMount.Weapon.Ammo == ammo)
                    ammoCount += weaponMount.InGunAmmo;
            }
        }

        return ammoCount;
    }

    public override int DecreaseAmmoInWeapon(int mountIndex, int amount = 1) {
        var wmi = _weapons[mountIndex];
        if (wmi.Weapon == null) return 0;

        if (wmi.Weapon.AttackType == InventoryWeaponAttackType.Ammunition) {
            wmi.InGunAmmo = Mathf.Max(wmi.InGunAmmo - amount, 0);
            return wmi.InGunAmmo;
        }

        return 0;
    }

    public override bool IsReloadAvailable(int weaponMountIndex) {
        var wmi = _weapons[weaponMountIndex];
        var weapon = wmi.Weapon;
        if (weapon && weapon.ReloadType != InventoryWeaponReloadType.None && wmi.InGunAmmo < weapon.AmmoCapacity) {
            if (GetAvailableAmmo(weapon.Ammo) > 0)
                return true;
        }

        return false;
    }

    public override InventoryMountInfo Search(InventoryItem item) {
        // search backpack
        foreach (var bm in _backpack.Where(bm => bm.Item == item)) {
            return bm;
        }
        
        // search ammo
        foreach (var am in _ammo.Where(am => am.Ammo == item)) {
            return am;
        }
        
        // search weapons
        foreach (var we in _weapons.Where(we => we.Weapon == item)) {
            return we;
        }

        return null;
    }

    public override int Remove(InventoryItem item) {
        int removeCount = 0;
        // search backpack
        foreach (var bm in _backpack.Where(bm => bm.Item == item)) {
            bm.Item = null;
            removeCount++;
        }
        
        // search ammo
        foreach (var am in _ammo.Where(am => am.Ammo == item)) {
            am.Ammo = null;
            am.AmmoRounds = 0;
            removeCount++;
        }
        
        // search weapons
        foreach (var we in _weapons.Where(we => we.Weapon == item)) {
            we.Weapon = null;
            we.InGunAmmo = 0;
            removeCount++;
        }

        return removeCount;
    }

    public override bool RemoveWeapon(int mountIndex) {
        if (_weapons[mountIndex].Weapon == null) return false;

        _weapons[mountIndex].Weapon = null;
        _weapons[mountIndex].InGunAmmo = 0;
        return true;
    }

    public override bool RemoveAmmo(int mountIndex) {
        if (_ammo[mountIndex].Ammo == null) return false;

        _ammo[mountIndex].Ammo = null;
        _ammo[mountIndex].AmmoRounds = 0;
        return true;
    }

    public override bool RemoveBackpack(int mountIndex) {
        if (_backpack[mountIndex].Item == null) return false;

        _backpack[mountIndex].Item = null;
        return true;
    }
    
    public override bool AddItemFromMountInfo(InventoryMountInfo mi, Vector3 position, bool playAudio = true) {
        if (mi is WeaponMountInfo) {
            var wmi = mi as WeaponMountInfo;
            OnWeaponChange.Invoke(wmi);
        }
        else if (mi is AmmoMountInfo) {
            var ami = mi as AmmoMountInfo;
            if (ami == null || ami.Ammo == null)
                return false;

            return AddAmmoItem(ami, position, playAudio);
        }
        else if (mi is BackpackMountInfo) {
            var bmi = mi as BackpackMountInfo;
            if (bmi == null || bmi.Item == null)
                return false;

            return AddBackpackItem(bmi, position, playAudio);
        }
        else if (mi is AudioMountInfo) {
            var ami = mi as AudioMountInfo;
            if (ami == null || ami.Audio == null)
                return false;

            return AddRecordingItem(ami, position, playAudio);
        }

        return true;
    }

    public override void AddNotification(string notification) {
        _notificationQueue.Enqueue(notification);
    }

    public override List<WeaponMountInfo> GetAllWeapons() {
        return _weapons;
    }

    public override List<AmmoMountInfo> GetAllAmmo() {
        return _ammo;
    }

    public override List<BackpackMountInfo> GetAllBackpack() {
        return _backpack;
    }


    public void OnBeforeSerialize() {}

    public void OnAfterDeserialize() {
        _weapons.Clear();
        _ammo.Clear();
        _backpack.Clear();
        _recordings.Clear();

        // Clones inspector lists into runtime lists
        foreach (var info in _weaponMounts) {
            var clone = new WeaponMountInfo();
            clone.InGunAmmo = info.InGunAmmo;
            clone.Weapon = info.Weapon;
            
            _weapons.Add(clone);
            if (_weapons.Count == 2) break;
        }

        foreach (var info in _ammoMounts) {
            var clone = new AmmoMountInfo();
            clone.Ammo = info.Ammo;
            clone.AmmoRounds = info.AmmoRounds;
            
            _ammo.Add(clone);
        }
        
        foreach (var info in _backpackMounts) {
            var clone = new BackpackMountInfo();
            clone.Item = info.Item;
            if (info.Item && info.Item.GetContextDataType() != null)
                clone.ContextData = JsonUtility.FromJson(info.ContextDataString, info.Item.GetContextDataType());
            
            _backpack.Add(clone);
        }

        foreach (var audio in _recordings) {
            _recordings.Add(audio);
        }

        _activeAudioRecordingIndex = -1;
    }
}
