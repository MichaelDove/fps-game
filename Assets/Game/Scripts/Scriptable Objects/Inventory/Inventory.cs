﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public abstract class InventoryMountInfo{}

[System.Serializable]
public class WeaponMountInfo : InventoryMountInfo {
    public InventoryItemWeapon Weapon;
    [Range(0, 100)] public int InGunAmmo = 0;
}

[System.Serializable]
public class AmmoMountInfo : InventoryMountInfo {
    public InventoryItemAmmo Ammo;
    public int AmmoRounds = 0;
}

[Serializable]
public class AudioMountInfo : InventoryMountInfo {
    public InventoryItemAudio Audio;
}

[System.Serializable]
public class BackpackMountInfo : InventoryMountInfo {
    public InventoryItem Item;

    [SerializeField] protected object _contextData;
    [SerializeField] protected string _contextDataString;

    public object ContextData {
        get => _contextData;
        set => _contextData = value;
    }

    public string ContextDataString {
        get => _contextDataString;
        set => _contextDataString = value;
    }
}

public class InventoryWeaponChangeEvent : UnityEvent<WeaponMountInfo> {}
public class InventoryWeaponDropEvent : UnityEvent<InventoryItemWeapon> {}

public enum AmmoAmountRequestType {AllAmmo, NoWeaponAmmo, WeaponAmmo}

// Base class for implementation of inventories
public abstract class Inventory : ScriptableObject {
    // events
    public InventoryWeaponChangeEvent OnWeaponChange = new InventoryWeaponChangeEvent();
    public InventoryWeaponDropEvent OnWeaponDrop = new InventoryWeaponDropEvent();
    
    // standard API
    public abstract WeaponMountInfo GetWeapon(int mountIndex);
    public abstract AmmoMountInfo GetAmmo(int mountIndex);
    public abstract BackpackMountInfo GetBackpack(int mountIndex);
    
    // Audio properties
    public abstract InventoryItemAudio GetAudioRecording(int recordingIndex);
    public abstract int GetActiveAudioRecording();
    public abstract int GetAudioRecordingCount();
    public abstract bool PlayAudioRecording(int recordingIndex);
    public abstract void StopAudioRecording();

    public abstract void DropAmmoItem(int mountIndex, bool playAudio = true);
    public abstract void DropBackpackItem(int mountIndex, bool playAudio = true);
    public abstract void DropWeaponItem(int mountIndex, bool playAudio = true);

    public abstract bool UseBackpackItem(int mountIndex, bool playAudio = true);
    public abstract bool ReloadWeapon(int mountIndex, bool playAudio = true);
    public abstract bool AddItem(CollectableItem collectableItem);
    public abstract void AssignWeapon(int mountIndex, WeaponMountInfo mountInfo);
    
    // Helper methods
    public abstract int GetAvailableAmmo(InventoryItemAmmo ammo, AmmoAmountRequestType requestType = AmmoAmountRequestType.NoWeaponAmmo);
    public abstract int DecreaseAmmoInWeapon(int mountIndex, int amount = 1);
    public abstract bool IsReloadAvailable(int weaponMountIndex);
    public abstract InventoryMountInfo Search(InventoryItem item);
    public abstract int Remove(InventoryItem item);
    public abstract bool RemoveWeapon(int mountIndex);
    public abstract bool RemoveAmmo(int mountIndex);
    public abstract bool RemoveBackpack(int mountIndex);
    public abstract void AddNotification(string notification);

    public abstract bool AddItemFromMountInfo(InventoryMountInfo mountInfo, Vector3 position, bool playAudio = true);
    
    public abstract List<WeaponMountInfo> GetAllWeapons();
    public abstract List<AmmoMountInfo> GetAllAmmo();
    public abstract List<BackpackMountInfo> GetAllBackpack();
}
