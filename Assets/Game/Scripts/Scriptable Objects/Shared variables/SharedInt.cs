﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Shared Variables/Shared String", fileName = "new Shared String")]
public class SharedString : ScriptableObject, ISerializationCallbackReceiver {
    [SerializeField] private string _value;
    
    private string _runtimeValue;

    public string Value {
        get => _runtimeValue;
        set => _runtimeValue = value;
    }


    public void OnBeforeSerialize() {}

    public void OnAfterDeserialize() {
        _runtimeValue = _value;
    }
}
