﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Shared Variables/Shared Vector3", fileName = "New Shared Vector3")]
public class SharedVector3 : ScriptableObject, ISerializationCallbackReceiver {
    [SerializeField] private Vector3 _value = Vector3.zero;
    private Vector3 _runtimeValue = Vector3.zero;

    public Vector3 Value {
        get => _runtimeValue;
        set => _runtimeValue = value;
    }

    public float x {
        get => _runtimeValue.x;
        set => _runtimeValue.x = value;
    }
    
    public float y {
        get => _runtimeValue.y;
        set => _runtimeValue.y = value;
    }
    
    public float z {
        get => _runtimeValue.z;
        set => _runtimeValue.z = value;
    }

    public void OnBeforeSerialize() {}

    public void OnAfterDeserialize() {
        _runtimeValue = _value;
    }
}
