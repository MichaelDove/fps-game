﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Shared Variables/Shared Sprite", fileName = "New Shared Sprite")]
public class SharedSprite : ScriptableObject, ISerializationCallbackReceiver {
    [SerializeField] private Sprite _value;
    private Sprite _runtimeValue;

    public Sprite Value {
        get => _runtimeValue;
        set => _runtimeValue = value;
    }

    public void OnBeforeSerialize() {}

    public void OnAfterDeserialize() {
        _runtimeValue = _value;
    }
}
