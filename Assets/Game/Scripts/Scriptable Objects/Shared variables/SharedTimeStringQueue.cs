﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Shared Collections/Timed String Queue", fileName = "new Timed String Queue")]
public class SharedTimeStringQueue : ScriptableObject, ISerializationCallbackReceiver {
    [SerializeField] private float _dequeueDelay = 3.5f;

    [Header("Sounds")] 
    [SerializeField] private bool _playSound = true;
    [SerializeField] private AudioCollection _notificationSound;
    
    // private variables
    private Queue<string> _queue = new Queue<string>();
    private float _nextDequeueTime = 0f;
    private IEnumerator _coroutine;
    private bool _paused = false;
    private string _currentText;

    public string Text => _currentText;

    public bool Paused {
        get => _paused;
        set => _paused = value;
    }

    public int Count => _queue.Count;

    public void Enqueue(string message) {
        _queue.Enqueue(message);

        if (_coroutine == null) {
            _coroutine = QueueProcessor();
            CoroutineRunner.instance.StartCoroutine(_coroutine);
        }
    }

    private IEnumerator QueueProcessor() {
        while (true) {
            if (!_paused) {
                // update timer
                _nextDequeueTime -= Time.unscaledDeltaTime;
                
                if (_nextDequeueTime < 0) {
                    if (_queue.Count == 0) break;
                    
                    _currentText = _queue.Dequeue();
                    _nextDequeueTime = _dequeueDelay;
                    
                    // play notification sound
                    var clip = _notificationSound[0];
                    AudioManager.instance.PlaySound(_notificationSound, clip, new Vector3(0, 0, 0));
                }
                
            }

            yield return null;
        }

        _currentText = "";
        _coroutine = null;
    }

    public void OnBeforeSerialize() {}

    public void OnAfterDeserialize() {
        _queue.Clear();
        _currentText = "";
    }
}
