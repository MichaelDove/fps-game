﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Shared Variables/Shared Float", fileName = "new Shared Float")]
public class SharedFloat : ScriptableObject, ISerializationCallbackReceiver {
    [SerializeField] private float _value = 0f;
    
    private float _runtimeValue = 0f;

    public float Value {
        get => _runtimeValue;
        set => _runtimeValue = value;
    }

    public void OnBeforeSerialize() {}

    public void OnAfterDeserialize() {
        _runtimeValue = _value;
    }
}
