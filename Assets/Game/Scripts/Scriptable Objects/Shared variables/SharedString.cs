﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Shared Variables/Shared Int", fileName = "new Shared Int")]
public class SharedInt : ScriptableObject, ISerializationCallbackReceiver {
    [SerializeField] private int _value = 0;
    
    private int _runtimeValue = 0;

    public int Value {
        get => _runtimeValue;
        set => _runtimeValue = value;
    }


    public void OnBeforeSerialize() {}

    public void OnAfterDeserialize() {
        _runtimeValue = _value;
    }
}
