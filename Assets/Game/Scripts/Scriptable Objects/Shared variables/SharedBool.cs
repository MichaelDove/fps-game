﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Objects/Shared Variables/Shared Bool", fileName = "new Shared Bool")]
public class SharedBool : ScriptableObject, ISerializationCallbackReceiver {
    [SerializeField] private bool _value;
    
    private bool _runtimeValue;

    public bool Value {
        get => _runtimeValue;
        set => _runtimeValue = value;
    }


    public void OnBeforeSerialize() {}

    public void OnAfterDeserialize() {
        _runtimeValue = _value;
    }
}
