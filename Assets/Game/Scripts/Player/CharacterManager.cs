﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Cursor = UnityEngine.Cursor;
using Random = UnityEngine.Random;

public enum FlashlightType {Primary, Secondary}

[System.Serializable]
public class Flashlight {
    [SerializeField] private GameObject LightObject;
    [SerializeField] private GameObject LightMesh;

    private Light _light;
    private float _intensity;

    public bool IsActive => LightObject.activeSelf;
    public bool IsSet => LightObject != null || LightMesh != null;

    public void OnStart() {
        if (LightObject) {
            _light = LightObject.GetComponent<Light>();
            _intensity = _light.intensity;
        }
    }

    public void OnUpdate(float currPower) {
        if (!LightObject.activeSelf)
            LightObject.SetActive(true);

        if (currPower > Mathf.Epsilon) {
            float depletionFac = Mathf.Clamp01(currPower * 2f / 100f);
            _light.intensity = _intensity * Mathf.Min(depletionFac, 0.5f) +
                               Mathf.PerlinNoise(Time.time * 4f, 0f) 
                               * _intensity * (1f - depletionFac); 
        }
        else {
            _light.intensity = 0;
            LightObject.SetActive(false);
        }
    }

    public void UpdateData(GameObject lightObject, GameObject lightMesh) {
        LightMesh = lightMesh;
        LightObject = lightObject;
    }

    public void ActivateMesh(bool enable) {
        LightObject.SetActive(enable);
        LightMesh.SetActive(enable);
    }

    public void ActivateLight(bool enable) {
        if (LightObject != null)
            LightObject.SetActive(enable);
    }
}

[System.Serializable]
public class ArmsObject {
    public ScriptableObject Identifier;
    public List<GameObject> SceneObjects = new List<GameObject>();
    public Flashlight Light = null;
    public AnimatorStateCallback Callback;
    public Transform CrosshairPosition;
    public Transform CrosshairPositionScoped;
}

[Serializable]
public class LightRay {
    public GameObject Mesh;
    public GameObject Light;
}

public class CharacterManager : MonoBehaviour {
    // Inspector assigned
    [SerializeField] private PlayerHUD _hud;
    [SerializeField] private CapsuleCollider _meleeTrigger;

    [SerializeField] private AISoundEmitter _soundEmitter;
    [SerializeField] private float _crouchRadius = 2.0f;
    [SerializeField] private float _walkRadius = 4.0f;
    [SerializeField] private float _runRadius = 10.0f;
    [SerializeField] private float _landingRadius = 14.0f;
    
    [Header("Camera Effects")]
    [SerializeField] private Camera _sceneCamera;
    [SerializeField] private CameraBloodEffect _cameraBloodEffect;
    [SerializeField] private CameraNightVisionEffect _nightVisionEffect;
    [SerializeField] private HighlightPrePassEffect _highlightEffect;
    
    [Header("Inventory")] 
    [SerializeField] private GameObject _inventoryUI;
    [SerializeField] private Inventory _inventory;
    [SerializeField] private Flashlight _primaryFlashlight = new Flashlight();
    [SerializeField] private InventoryItemWeapon _fists;

    [Header("Arms system")] 
    [SerializeField] private Animator _armsAnim;
    [SerializeField] private List<ArmsObject> _armsObjects = new List<ArmsObject>();
    [SerializeField] private LayerMask _weaponRayLayerMask;
    [SerializeField] private LayerMask _interactiveRayBlockMask;
    [SerializeField] private LightRay _lightRay;

    [Header("Batter Powered Items")] 
    [SerializeField] private SharedFloat _flashlightCharge;
    [SerializeField] private float _flashlightDropRate = 5f;
    [SerializeField] private SharedFloat _nightVisionCharge;
    [SerializeField] private float _nightVisionDropRate = 8f;

    [Header("Shared Variables")] 
    [SerializeField] private SharedFloat _health;
    [SerializeField] private SharedString _interactionText;
    [SerializeField] private SharedFloat _stamina;
    [SerializeField] private SharedVector3 _crosshairPosition;
    [SerializeField] private SharedSprite _crosshairSprite;
    [SerializeField] private SharedFloat _crosshairAlpha;
    [SerializeField] private VectorShaker _cameraShaker;
    [SerializeField] private SharedInt _inWeaponAmmo;
    [SerializeField] private SharedInt _inAmmoBeltAmmo;
    [SerializeField] private SharedInt _currentWeaponType;
    
    // sound collections
    [Header("Sounds")]
    [SerializeField] private AudioCollection _painSounds;
    [SerializeField] private float _painSoundOffset = 0.35f;
	[SerializeField] private AudioCollection _tauntSounds;
	[SerializeField] private float _tauntRadius = 10.0f;
    private float _nextPainSound = 0f;
	private float _nextTauntSound = 0f;
    
    
    // Private members
    private Collider _collider;
    private FPSController _fpsController;
    private CharacterController _charController;
    private GameSceneManager _gameSceneManager;
    private int _aiBodyPart;
    private int _interactiveMask = 0;
    private bool _externalUIActive;
    private bool _firstLanding = true;
    private bool _exitWindowActive = false;
    
    // arms and weapons
    private Flashlight _secondaryFlashlight;
    private InventoryItemWeapon _currWeapon;
    private InventoryItemWeapon _nextWeapon;
    private WeaponMountInfo _nextWeaponMountInfo;
    private bool _canSwitchWeapons = false;
    private IEnumerator _switchWeaponCoroutine;
    private int _availableAmmo = 0;
    private float _initalFOV = 60f;
    
    // dictionary for fast access at runtime
    private Dictionary<ScriptableObject, ArmsObject> _armsObjectsDict = new Dictionary<ScriptableObject, ArmsObject>();
    
    // weapon memory
    private InventoryItemWeapon _weaponMemory;
    private bool _weaponArmedMemory;

    // animation hashes
    private int _weaponArmedHash = Animator.StringToHash("Weapon Armed");
    private int _weaponAnimHash = Animator.StringToHash("Weapon Anim");
    private int _clearWeaponHash = Animator.StringToHash("Clear Weapon");
    private int _scopeModeHash = Animator.StringToHash("Scope Mode Active");
    private int _dualHandedWeaponHash = Animator.StringToHash("Dual Handed Weapon");
    private int _attackHash = Animator.StringToHash("Attack");
    private int _attackAnimHash = Animator.StringToHash("Attack Anim");
    private int _autoFireHash = Animator.StringToHash("Auto Fire");
    private int _reloadHash = Animator.StringToHash("Reload");
    private int _reloadRepeatHash = Animator.StringToHash("Reload Repeat");
    private int _flashlightHash = Animator.StringToHash("Flashlight");
    private int _switchingWeaponHash = Animator.StringToHash("Switching Weapon");
    private int _canSwitchWeaponHash = Animator.StringToHash("Can Switch Weapon");
    private int _speedHash = Animator.StringToHash("Speed");
    private int _staminaHash = Animator.StringToHash("Stamina");
    private int _speedOverrideHash = Animator.StringToHash("Player Speed Override");
    private int _scopeModeFovHash = Animator.StringToHash("Dual Mode FOV Weight");
    private int _crosshairAlphaHash = Animator.StringToHash("Crosshair Alpha");
    private int _toggleNightVisionHash = Animator.StringToHash("Toggle Night Vision");


    // public properties
    public FPSController FpsController => _fpsController;
    public Inventory Inventory => _inventory;

    public bool NightVisionEnabled =>_nightVisionEffect.enabled;

    private void OnEnable() {
        _inventory.OnWeaponChange.AddListener(OnSwitchWeapon);
        _inventory.OnWeaponDrop.AddListener(OnDropWeapon);

        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    
    private void OnDisable() {
        _inventory.OnWeaponChange.RemoveListener(OnSwitchWeapon);
        _inventory.OnWeaponDrop.RemoveListener(OnDropWeapon);
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        _firstLanding = true;
    }

    public Vector3 PickUpTarget => _lightRay.Mesh.transform.position;

    // TEMPORARY
    private void OnCollisionEnter(Collision other) {
        Debug.Log(other.gameObject.name);
    }

    private void OnCollisionStay(Collision other) {
        Debug.Log(other.gameObject.name);
    }

    // EVENT methods
    
    private void OnSwitchWeapon(WeaponMountInfo wmi) {
        if (_canSwitchWeapons && _switchWeaponCoroutine == null) {
            _switchWeaponCoroutine = SwitchWeaponInternal(wmi.Weapon, wmi);
            StartCoroutine(_switchWeaponCoroutine);
        }
    }
    
    public void SwitchMount(InventoryItemWeapon nextWeapon) {
        if (_canSwitchWeapons && _switchWeaponCoroutine == null) {
            _switchWeaponCoroutine = SwitchWeaponInternal(nextWeapon, null);
            StartCoroutine(_switchWeaponCoroutine);
        }
    }

    private IEnumerator SwitchWeaponInternal(InventoryItemWeapon nextWeapon, WeaponMountInfo wmi) {
        _armsAnim.SetBool(_reloadHash, false);
        _armsAnim.SetBool(_weaponArmedHash, false);
        _nextWeapon = nextWeapon;
        _nextWeaponMountInfo = wmi;

        if (nextWeapon) {
            if(nextWeapon.Type == InventoryWeaponType.DoubleHanded)
                _armsAnim.SetBool(_dualHandedWeaponHash, true);
            else
                _armsAnim.SetBool(_dualHandedWeaponHash, false);
            _armsAnim.SetBool(_switchingWeaponHash, true);

            yield return new WaitForSeconds(0.1f);

            _armsAnim.SetBool(_weaponArmedHash, true);
            _armsAnim.SetInteger(_weaponAnimHash, nextWeapon.WeaponAnim);
        }

        _switchWeaponCoroutine = null;
    }

    private void OnDropWeapon(InventoryItemWeapon weapon) {
        if (!_inventoryUI.activeSelf) return;
        
        if (_currWeapon == weapon && _currWeapon != null) {
            // deactivate the arms objects
            ArmsObject armsObject;
            if (_armsObjectsDict.TryGetValue(_currWeapon, out armsObject)) {
                foreach (var subObject in armsObject.SceneObjects) {
                    subObject.SetActive(false);
                }
            }
            
            Input.ResetInputAxes();

            _armsAnim.SetTrigger(_clearWeaponHash);
            _armsAnim.SetBool(_weaponArmedHash, false);
            _armsAnim.SetInteger(_weaponAnimHash, 0);
            
            _currWeapon = null;
        }
    }

    private void Start() {
        _collider = GetComponent<Collider>();
        _fpsController = GetComponent<FPSController>();
        _charController = GetComponent<CharacterController>();
        _gameSceneManager = GameSceneManager.instance;

        _aiBodyPart = LayerMask.NameToLayer("AI Body Part");
        _interactiveMask = 1 << LayerMask.NameToLayer("Interactive");

        _initalFOV = _sceneCamera.fieldOfView;

        // register the player info:
        PlayerInfo info = new PlayerInfo();
        info.Camera = _sceneCamera;
        info.Collider = _collider;
        info.CharacterManager = this;
        info.MeleeTrigger = _meleeTrigger;
        
        _gameSceneManager.RegisterPlayerInfo(_collider.GetInstanceID(), info);
        
        // get rid of the cursor
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        
        _primaryFlashlight.OnStart();
        _primaryFlashlight.ActivateLight(false);
        _primaryFlashlight.ActivateMesh(false);
        ActivateFlashlight(false);
        
        // arms and weapons system
        // an array of all SMB on the animator
        ArmsBase[] stateMachineBehaviours = _armsAnim.GetBehaviours<ArmsBase>();
        // a temporary dictionary of SMBs sorted by the weapon type
        Dictionary<ScriptableObject, List<ArmsBase>> stateMachineBehavioursByWeapon = new Dictionary<ScriptableObject, List<ArmsBase>>();
        
        foreach (var behaviour in stateMachineBehaviours) {
            behaviour.CharacterManager = this;

            if (behaviour.Identifier) {
                List<ArmsBase> behaviourList = null;
                if (stateMachineBehavioursByWeapon.TryGetValue(behaviour.Identifier, out behaviourList)) {
                    behaviourList.Add(behaviour);
                } else {
                    List<ArmsBase> newBehaviourList = new List<ArmsBase>();
                    newBehaviourList.Add(behaviour);
                    stateMachineBehavioursByWeapon[behaviour.Identifier] = newBehaviourList;
                }
            }
        }

        for (int i = 0; i < _armsObjects.Count; i++) {
            var armsObject = _armsObjects[i];
            if (armsObject.Identifier) {
                armsObject.Light?.OnStart(); // if it is not null

                // store game object in the dictionary by type
                _armsObjectsDict[armsObject.Identifier] = armsObject;
                List<ArmsBase> behaviourList;
                if (stateMachineBehavioursByWeapon.TryGetValue(armsObject.Identifier, out behaviourList)) {
                    foreach (var behaviour in behaviourList) {
                        behaviour.CallbackHandler = armsObject.Callback;
                    }
                }
            }
        }
    }

    public void TakeDamage(float damage, bool doPain) {
        _health.Value = Mathf.Max(_health.Value - damage*Time.deltaTime, 0f);
        _cameraBloodEffect.MinBloodAmount = (1f - _health.Value / 100f) / 2f;
        _cameraBloodEffect.BloodAmount = Mathf.Min(_cameraBloodEffect.MinBloodAmount + 0.3f, 1f);
        
        // if a zombie hits us it will stop us
        _fpsController.DragMultiplier = 0.0f;
        
        // do pain sounds
        if (doPain && _nextPainSound < Time.time) {
            AudioClip clip = _painSounds.audioClip;
            _nextPainSound = Time.time + clip.length;
            StartCoroutine(AudioManager.instance.PlaySound(_painSounds.AudioGroup, clip, transform.position,
                _painSounds.Volume, _painSounds.SpatialBlend, _painSoundOffset, _painSounds.Priority));
        }

        if (_health.Value <= 0.0f) {
            Die();
        }
    }

    public void DoDamage(int hitDirection = 0) {
        if (_currWeapon && _currWeapon.AttackType == InventoryWeaponAttackType.Ammunition &&
            _inventory.GetAvailableAmmo(_currWeapon.Ammo, AmmoAmountRequestType.WeaponAmmo) > 0) {
            
            // decrease ammo
            _inventory.DecreaseAmmoInWeapon(_currWeapon.Type == InventoryWeaponType.SingleHanded ? 0 : 1);
            
            if (_inventory.GetAvailableAmmo(_currWeapon.Ammo, AmmoAmountRequestType.WeaponAmmo) < 1) {
                _armsAnim.SetBool(_autoFireHash, false);
            }

            var armsObject = _armsObjectsDict[_currWeapon];
            var weaponArmsCallback = armsObject.Callback as WeaponAnimatorStateCallback;
            weaponArmsCallback.DoMuzzleFlash();
        }

        var weapon = _currWeapon == null ? _fists : _currWeapon;
        if (weapon) {
            _soundEmitter.SetRadius(weapon.SoundRadius);
            // shake the camera
            if (weapon.ShakeType == WeaponShakeType.OnFire)
                _cameraShaker.ShakeVector(weapon.ShakeDuration, weapon.ShakeMagnitude, weapon.ShakeDamping);
            
            // RAY CASTING AND DAMAGE APPLICATION
            Ray ray;
            RaycastHit hit = new RaycastHit();
            RaycastHit[] hits;
            bool isHit = false;
            
            // create a ray through the crosshair
            ray = _sceneCamera.ScreenPointToRay(_crosshairPosition.Value);
            // blast radius weapon
            if (weapon.RayRadius > 0f) {
                ray.origin = ray.origin - transform.forward * weapon.RayRadius;
                hits = Physics.SphereCastAll(ray, weapon.RayRadius, weapon.Range, _weaponRayLayerMask.value, QueryTriggerInteraction.Ignore);
                foreach (var potentialHit in hits) {
                    // ignore everything that is not ai body part
                    if (potentialHit.transform.gameObject.layer == LayerMask.NameToLayer("AI Body Part")) {
                        Ray sightTestRay = ray;
                        ray.origin += transform.forward * weapon.RayRadius;
                        sightTestRay.direction = potentialHit.point - sightTestRay.origin;

                        if (Physics.Raycast(sightTestRay, out hit, 1000, _weaponRayLayerMask.value, QueryTriggerInteraction.Ignore)) {
                            if (potentialHit.transform == hit.transform) {
                                if (hit.rigidbody) {
                                    var stateMachine = _gameSceneManager.GetAIStateMachine(hit.rigidbody.GetInstanceID());
                                    if (stateMachine) {
                                        float damage = weapon.GetAttenuatedDamage(hit.rigidbody.tag, hit.distance);
                                        // apply the damage
                                        stateMachine.TakeDamage(hit.point, 
                                            ray.direction * weapon.GetAttenuatedForce(hit.distance), 
                                            (int)damage, hit.rigidbody, 
                                            this, 
                                            hitDirection);
                                        isHit = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else {
                if (Physics.Raycast(ray, out hit, weapon.Range, _weaponRayLayerMask.value, QueryTriggerInteraction.Ignore)) {
                    if (hit.rigidbody) {
                        // is it a zombie?
                        var stateMachine = _gameSceneManager.GetAIStateMachine(hit.rigidbody.GetInstanceID());
                        if (stateMachine) {
                            float damage = weapon.GetAttenuatedDamage(hit.rigidbody.tag, hit.distance);
                            // apply the damage
                            stateMachine.TakeDamage(hit.point, 
                                ray.direction * weapon.GetAttenuatedForce(hit.distance), 
                                (int)damage, hit.rigidbody, 
                                this, 
                                hitDirection);
                            isHit = true;
                        }
                    }
                }
            }

            if (isHit && weapon.ShakeType == WeaponShakeType.OnHit) {
                _cameraShaker.ShakeVector(weapon.ShakeDuration, weapon.ShakeMagnitude, weapon.ShakeDamping);
            }
        }
    }

    public void ExternalUIActive(bool active) {
        if (active) {
            _externalUIActive = true;
            _inventoryUI.SetActive(false);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            _fpsController.DisableInput = true;
        }
        else {
            _externalUIActive = false;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.Locked;
            _fpsController.DisableInput = false;
        }
    }
    
    private void Update() {
        if (_externalUIActive) return;

        // Process inventory toggle key
        if (Input.GetButtonDown("Inventory")) {
            if (!_inventoryUI.activeSelf) {
                _inventoryUI.SetActive(true);
                _hud.gameObject.SetActive(false);
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                return;
            }
            else {
                _inventoryUI.SetActive(false);
                _hud.gameObject.SetActive(true); 
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
        else if (Input.GetButtonDown("Exit")) {
            if (!_exitWindowActive) {
                _exitWindowActive = true;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                _hud.Exit();
            }
            else {
                _exitWindowActive = false;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                _hud.CancelExit();
            } 
        }
        
        // update shared variables
        _currentWeaponType.Value = 0;
        if (_currWeapon != null) {
            _inWeaponAmmo.Value = _currWeapon.AttackType == InventoryWeaponAttackType.Ammunition
                ? _inventory.GetAvailableAmmo(_currWeapon.Ammo, AmmoAmountRequestType.WeaponAmmo)
                : 0;
            _inAmmoBeltAmmo.Value = _currWeapon.AttackType == InventoryWeaponAttackType.Ammunition
                ? _inventory.GetAvailableAmmo(_currWeapon.Ammo, AmmoAmountRequestType.NoWeaponAmmo)
                : 0;

            if (_currWeapon.Type == InventoryWeaponType.DoubleHanded)
                _currentWeaponType.Value = 2;
            else
                _currentWeaponType.Value = 1;
        } else {
            _inWeaponAmmo.Value = 0;
            _inAmmoBeltAmmo.Value = 0;
        }
        
        

        // update animator variables
        _canSwitchWeapons = _armsAnim.GetFloat(_canSwitchWeaponHash) > 0.9f;
        _fpsController.SpeedOverride = _armsAnim.GetFloat(_speedOverrideHash);
        _crosshairAlpha.Value = _armsAnim.GetFloat(_crosshairAlphaHash);
        // stamina
        float normalStamina = (_stamina.Value / 100f) * _fpsController.DragMultiplier;
        _armsAnim.SetFloat(_staminaHash, Mathf.Min(normalStamina + 0.1f, 1.0f));
        // fov when scoped:
        float zoomWeight = _armsAnim.GetFloat(_scopeModeFovHash);
        if (!zoomWeight.Equals(0f) && _currWeapon && _currWeapon.ScopeMode) {
            _sceneCamera.fieldOfView = Mathf.Lerp(_initalFOV, _currWeapon.ScopeModeFov, zoomWeight);
        } else {
            _sceneCamera.fieldOfView = _initalFOV;
        }

        // raycast for interactive items
        Ray ray;
        RaycastHit hit;
        RaycastHit[] hits;
        // get a ray from the center of the screen
        ray = _sceneCamera.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        // calculate ray length
        float rayLength = Mathf.Lerp(1.0f, 1.8f, Mathf.Abs(Vector3.Dot(_sceneCamera.transform.forward, Vector3.up)));
        // collect all hits
        hits = Physics.RaycastAll(ray, rayLength, _interactiveMask);
        if (hits.Length > 0) {
            // get the hit with the highest priority
            int highestPriority = int.MinValue;
            InteractiveItem priorityItem = null;

            for (int i = 0; i < hits.Length; i++) {
                // process next hit
                hit = hits[i];
                RaycastHit occluder;

                // we cant pick up an object through a wall
                if (Physics.Raycast(ray, out occluder, rayLength, _interactiveRayBlockMask)) {
                    if (occluder.collider != hit.collider && occluder.distance < hit.distance) {
                        continue;
                    }
                }
                
                // fetch its interactive script from the database
                InteractiveItem ii = _gameSceneManager.GetInteractiveItem(hit.collider.GetInstanceID());
                if (ii != null && ii.Priority > highestPriority) {
                    priorityItem = ii;
                    highestPriority = ii.Priority;
                }
            }
            
            // we found the highest priority object
            if (priorityItem != null) {
                _highlightEffect.HighlightItem = priorityItem;
                _interactionText.Value = priorityItem.GetText(this);
                if (!(priorityItem.GetType() == typeof(CollectableWeapon) && (!_canSwitchWeapons || _armsAnim.GetBool("Switching Weapon")))) {
                    if (Input.GetButtonDown("Use")) {
                        priorityItem.Use(this);
                        _armsAnim.SetTrigger("ForceLowerFists");
                        if (_currWeapon)
                            _availableAmmo = _inventory.GetAvailableAmmo(_currWeapon.Ammo, AmmoAmountRequestType.NoWeaponAmmo);
                    }
                }
            }
        } else {
            _interactionText.Value = "";
            _highlightEffect.HighlightItem = null;
        }

        float newRadius = _crouchRadius;
        switch (_fpsController.CurrentStatus) {
            case PlayerStatus.Landing:
                if (!_firstLanding) {
                    newRadius = Mathf.Max(newRadius, _landingRadius);
                    _armsAnim.SetInteger(_speedHash, 0);
                } else {
                    _firstLanding = false;
                }
                break;
            case PlayerStatus.Running:
                newRadius = Mathf.Max(newRadius, _runRadius);
                _armsAnim.SetInteger(_speedHash, 2);
                break;
            case PlayerStatus.Crouching:
                newRadius = Mathf.Max(newRadius, _crouchRadius);
                _armsAnim.SetInteger(_speedHash, 0);
                break;
            case PlayerStatus.Walking:
                newRadius = Mathf.Max(newRadius, _walkRadius);
                _armsAnim.SetInteger(_speedHash, 1);
                break;
            default:
                _armsAnim.SetInteger(_speedHash, 0);
                break;
        }
        
        _soundEmitter.SetRadius(newRadius);
        _fpsController.DragMultiplierLimit = Mathf.Max((_health.Value + _health.Value/3) / 100f, 0.6f);
        
        // Process crosshair position
        if (!_inventoryUI.activeSelf) {
            if (_nightVisionEffect.enabled) {
                // deplete charge
                _nightVisionCharge.Value = Mathf.Clamp(_nightVisionCharge.Value - (_nightVisionDropRate / 60f * Time.deltaTime), 0, 100);
                if (_nightVisionCharge.Value < Mathf.Epsilon)
                    _nightVisionEffect.enabled = false;
            }

            if (_armsAnim.GetBool(_flashlightHash)) {
                if (_primaryFlashlight.IsActive || (_secondaryFlashlight != null && _secondaryFlashlight.IsActive)) {
                    _flashlightCharge.Value = Mathf.Clamp(_flashlightCharge.Value - (_flashlightDropRate / 60f * Time.deltaTime), 0, 100);
                    
                    if (_flashlightCharge.Value < Mathf.Epsilon)
                        ActivateFlashlight(false);
                    
                    if (_primaryFlashlight.IsActive)
                        _primaryFlashlight.OnUpdate(_flashlightCharge.Value);
                    
                    if (_secondaryFlashlight != null && _secondaryFlashlight.IsActive)
                        _secondaryFlashlight.OnUpdate(_flashlightCharge.Value);
                }
            }
            
            // default crosshair position
            _crosshairPosition.Value = _sceneCamera.ViewportToScreenPoint(new Vector3(0.5f, 0.5f, 0.5f));
            
            if (_currWeapon) {
                var armsObject = _armsObjectsDict[_currWeapon];
                if (_currWeapon.ScopeMode) {
                    _crosshairPosition.Value = Vector3.Lerp(
                        _sceneCamera.WorldToScreenPoint(armsObject.CrosshairPosition.position),
                        _sceneCamera.WorldToScreenPoint(armsObject.CrosshairPositionScoped.position),
                        _armsAnim.GetFloat(_scopeModeFovHash));
                    
                }
                else {
                    if (armsObject.CrosshairPosition != null)
                        _crosshairPosition.Value = _sceneCamera.WorldToScreenPoint(armsObject.CrosshairPosition.position);
                }
            }
        }

        // INPUT 
        if (!_inventoryUI.activeSelf) {
            // Turn on FLASHLIGHT
            if (Input.GetButtonDown("Flashlight") && _flashlightCharge.Value > Mathf.Epsilon) {
                if (!_nightVisionEffect.enabled)
                    ActivateFlashlight(!_armsAnim.GetBool(_flashlightHash));
            }
            
            // Wistle
            if (Input.GetButtonDown("Taunt")) {
                DoTaunt();
            }

            // SWITCH weapons
            int mountIndex = -1;
            if (_canSwitchWeapons) {
                if (Input.GetButtonDown("Night Vision") && _nightVisionCharge.Value > Mathf.Epsilon) ToggleNightVision();
                if (Input.GetButtonDown("Secondary")) mountIndex = 0;
                if (Input.GetButtonDown("Primary")) mountIndex = 1;
            }

            if (mountIndex != -1) {
                WeaponMountInfo wmi = _inventory.GetWeapon(mountIndex);
                if (wmi.Weapon != null) {
                    if (_currWeapon == wmi.Weapon) {
                        bool weaponArmed = _armsAnim.GetBool(_weaponArmedHash);
                        weaponArmed = !weaponArmed;
                        if (weaponArmed)
                            _nextWeapon = wmi.Weapon;
                        else
                            _nextWeapon = null;
                        
                        _armsAnim.SetBool(_weaponArmedHash, weaponArmed);
                    } else {
                        SwitchMount(wmi.Weapon);  
                    }
                }
            }
            
            // process FIRE input
            if (Input.GetButtonDown("Fire1")) {
                int attackAnim = 1;
                bool automatic = false;
                bool canFire = true;
                
                if (_fists)
                    attackAnim = Random.Range(1, _fists.AttackAnimCount + 1);

                if (_currWeapon) {
                    mountIndex = _currWeapon.Type == InventoryWeaponType.SingleHanded ? 0 : 1;
                    var wmi = _inventory.GetWeapon(mountIndex);
                    if ((_currWeapon.AttackType == InventoryWeaponAttackType.Ammunition && wmi.InGunAmmo > 0) ||
                        _currWeapon.AttackType == InventoryWeaponAttackType.Melee) {
                        automatic = _currWeapon.Automatic;
                        attackAnim = Random.Range(1, _currWeapon.AttackAnimCount + 1);
                    }
                    else {
                        canFire = false;
                        if (_currWeapon.AttackType == InventoryWeaponAttackType.Ammunition &&
                            _inventory.GetAvailableAmmo(_currWeapon.Ammo, AmmoAmountRequestType.AllAmmo) > 0) {
                            Reload();
                        }
                    }
                }

                if (canFire) {
                    _armsAnim.SetTrigger(_attackHash);
                    _armsAnim.SetInteger(_attackAnimHash, attackAnim);
                    _armsAnim.SetBool(_autoFireHash, automatic);
                }
            } 
            else if (Input.GetButtonUp("Fire1")) {
                _armsAnim.SetBool(_autoFireHash, false);
            }
                
            
            // process SCOPE input
            if (Input.GetButtonDown("Fire2") && _currWeapon != null && _currWeapon.ScopeMode) {
                _armsAnim.SetBool(_scopeModeHash, !_armsAnim.GetBool(_scopeModeHash));
            }

            // process RELOAD input
            if (Input.GetButtonDown("Reload") && _currWeapon != null && _currWeapon.AttackType == InventoryWeaponAttackType.Ammunition) {
                Reload();
            }
        }
        
    }
    
    // Helper methods
    private void Reload() {
        if (_availableAmmo > 0) {
            // what mount are we reloading
            int mountIndex = _currWeapon.Type == InventoryWeaponType.SingleHanded ? 0 : 1;
            var wmi = _inventory.GetWeapon(mountIndex);
            // check if we have any ammo in the ammo belt and if the weapon is not full
            if (_inventory.IsReloadAvailable(mountIndex)) {
                if (wmi.Weapon.ReloadType == InventoryWeaponReloadType.Partial)
                    _armsAnim.SetInteger(_reloadRepeatHash, Mathf.Min(_availableAmmo,wmi.Weapon.AmmoCapacity - wmi.InGunAmmo));
                else
                    _armsAnim.SetInteger(_reloadRepeatHash, 0);
                        
                _armsAnim.SetBool(_reloadHash, true);
            }
        }
    }
    
    public void ActivateFlashlight(bool activate) {
        _armsAnim.SetBool(_flashlightHash, activate);
        _secondaryFlashlight?.ActivateLight(activate);
    }

    public void ToggleNightVision() {
        _weaponMemory = null;

        if (_currWeapon && _currWeapon.Type == InventoryWeaponType.DoubleHanded) {
            _nextWeapon = null;
            _weaponMemory = _currWeapon;
            _weaponArmedMemory = _armsAnim.GetBool(_weaponArmedHash);
            _armsAnim.SetBool(_weaponArmedHash, false);
            _armsAnim.SetBool(_switchingWeaponHash, _weaponArmedMemory);
        }
        
        _armsAnim.SetBool(_toggleNightVisionHash, true);
    }

    // CALLBACK methods
    
    public void ActivateFlashlightMesh_AnimatorCallback(bool enableMesh, FlashlightType type) {
        if (type == FlashlightType.Primary) {
            _primaryFlashlight.ActivateMesh(enableMesh);
        }   
    }

    public void ToggleNightVision_AnimatorCallback() {
        _armsAnim.SetBool(_flashlightHash, false);
        _nightVisionEffect.enabled = !_nightVisionEffect.enabled;
    }

    public void EndToggleNightVision_AnimatorCallback() {
        _armsAnim.SetBool(_toggleNightVisionHash, false);
        if (_weaponMemory) {
            _armsAnim.SetBool(_weaponArmedHash, _weaponArmedMemory);
        }

        if (_weaponMemory != null) {
            _nextWeapon = _weaponMemory;
        }
    }

    public void ActivateFlashlightLight_AnimatorCallback(bool enableLight, FlashlightType type) {
        if (type == FlashlightType.Primary) {
            _primaryFlashlight.ActivateLight(enableLight);
        }
        else {
            _secondaryFlashlight.ActivateLight(enableLight);
        }
    }

    public void EnableWeapon_AnimatorCallback() {
        if (_nextWeapon != null) {
            if (_nextWeapon != _currWeapon && _nextWeaponMountInfo != null) {
                int mountIndex = _nextWeapon.Type == InventoryWeaponType.DoubleHanded ? 1 : 0;
                _inventory.AssignWeapon(mountIndex, _nextWeaponMountInfo);
            }

            ArmsObject armsObject;
            if (_armsObjectsDict.TryGetValue(_nextWeapon, out armsObject)) {
                foreach (var subObject in armsObject.SceneObjects) {
                    subObject.SetActive(true);
                }

                if (armsObject.Light.IsSet)
                    _secondaryFlashlight = armsObject.Light;
                else
                    _secondaryFlashlight = null;
            }

            _currWeapon = _nextWeapon;
            _availableAmmo = _inventory.GetAvailableAmmo(_currWeapon.Ammo, AmmoAmountRequestType.NoWeaponAmmo);
        }
        
        _armsAnim.SetBool(_switchingWeaponHash, false);
        _nextWeapon = null;
        _nextWeaponMountInfo = null;
        
        if (_currWeapon != null && _crosshairSprite.Value != null)
            _crosshairSprite.Value = _currWeapon.Crosshair;
    }

    public void DisableWeapon_AnimatorCallback() {
        if (_currWeapon != null) {
            ArmsObject armsObject;
            if (_armsObjectsDict.TryGetValue(_currWeapon, out armsObject)) {
                foreach (var subObject in armsObject.SceneObjects) {
                    subObject.SetActive(false);
                }
            }
        }

        if (_nextWeapon != null && _nextWeaponMountInfo != null) {
            _inventory.DropWeaponItem(_nextWeaponMountInfo.Weapon.Type == InventoryWeaponType.DoubleHanded ? 1 : 0);
        }

        _currWeapon = null;
        _secondaryFlashlight = null;
        
        _crosshairSprite.Value = null;
    }

    public void ReloadWeapon_AnimatorCallback(InventoryWeaponType weaponType) {
        if (!_currWeapon || weaponType == InventoryWeaponType.None) return;
        _inventory.ReloadWeapon(weaponType == InventoryWeaponType.SingleHanded ? 0 : 1, false);
        _availableAmmo = _inventory.GetAvailableAmmo(_currWeapon.Ammo, AmmoAmountRequestType.NoWeaponAmmo);
    }
    
	private void DoTaunt() {
        AudioClip clip = _tauntSounds[0];
        AudioManager.instance.PlaySound(_tauntSounds.AudioGroup, clip, transform.position, _tauntSounds.Volume,
            _tauntSounds.SpatialBlend, _tauntSounds.Priority);
        _nextTauntSound = Time.time + clip.length; 
        _soundEmitter.SetRadius(_tauntRadius );
    }
    
    // public methods
    public void DoLevelComplete() {
        // freeze movement
        _fpsController.FreezeMovement = true;
        
        // fade out
        _hud.Fade(4.0f, ScreenFadeType.FadeIn);
        /*_hud.ShowMissionText("You escaped from the zombies!");
        _hud.Invalidate(this);*/
        
        // return to menu
        Invoke(nameof(GameOver), 3.0f);
    }

    public void Die() {
        _fpsController.FreezeMovement = true;
        _hud.Fade(4.0f, ScreenFadeType.FadeIn);
        /*_hud.ShowMissionText("You failed to escape from the zombies!");
        _hud.Invalidate(this);*/
        
        Invoke(nameof(GameOver), 3.0f);
    }

    public void TurnOfRay() {
        _lightRay.Mesh.SetActive(false);
        _lightRay.Light.SetActive(false);
    }

    public void TurnOnRay() {
        _lightRay.Mesh.SetActive(true);
        _lightRay.Light.SetActive(true);
    }

    private void GameOver() {
        // return to menu
        ApplicationManager.instance.LoadMainMenu();
    }
}  
