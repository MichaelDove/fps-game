﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

// Enumerations
public enum PlayerStatus {Standing, Walking, Running, Crouching, NotGrounded, Landing}

// Delegates
public delegate void CurveControlledBobCallback();

[Serializable]
public class CurveControlledBobEvent {
    public float Time = 0f;
    public CurveControlledBobCallback Function = null;
}

[Serializable]
public class CurveControllerBob {
    // Inspector assigned
    [SerializeField] AnimationCurve _curve = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(0.5f, 1f), 
                                                                new Keyframe(1f, 0f), new Keyframe(1.5f, -1f), 
                                                                new Keyframe(2f, 0f));

    [SerializeField] private float _horizontalMultiplier = 0.01f;
    [SerializeField] private float _verticalMultiplier = 0.02f;
    [SerializeField] private float _verticalToHorizontalSpeedRatio = 2f;
    [SerializeField] private float _baseInterval = 1f;

    // private variables
    private float _xPlayHead;
    private float _yPlayHead;
    private float _curveEndTime;

    private float _xPlayHeadPrev;
    private float _yPlayHeadPrev;
    private List<CurveControlledBobEvent> _events = new List<CurveControlledBobEvent>();

    public void Initialize() {
        _curveEndTime = _curve[_curve.length - 1].time;
        _xPlayHead = 0f;
        _yPlayHead = 0f;

        _xPlayHeadPrev = 0f;
        _yPlayHeadPrev = 0f;
    }

    public void RegisterEventCallback(float time, CurveControlledBobCallback function) {
        CurveControlledBobEvent e = new CurveControlledBobEvent();
        e.Time = time;
        e.Function = function;

        _events.Add(e);
        // sort the list according to the 'Time' variable
        _events.Sort(
                delegate(CurveControlledBobEvent t1, CurveControlledBobEvent t2) {
                    return t1.Time.CompareTo(t2.Time);
                }
            );
    }

    public Vector3 GetVectorOffset(float speed) {
        _xPlayHeadPrev = _xPlayHead;
        _yPlayHeadPrev = _yPlayHead;
        
        _xPlayHead += (speed * Time.deltaTime) / _baseInterval;
        _yPlayHead += ((speed * Time.deltaTime) / _baseInterval) * _verticalToHorizontalSpeedRatio;

        if (_xPlayHead > _curveEndTime)
            _xPlayHead -= _curveEndTime;

        if (_yPlayHead > _curveEndTime)
            _yPlayHead -= _curveEndTime;
        
        // process events
        foreach (var e in _events) {
            if ((_yPlayHeadPrev < e.Time && _yPlayHead >= e.Time) ||
                (_yPlayHead < _yPlayHeadPrev && (_yPlayHead >= e.Time || _yPlayHeadPrev < e.Time))) {
                e.Function();
            }
        }

        float xPos = _curve.Evaluate(_xPlayHead) * _horizontalMultiplier;
        float yPos = _curve.Evaluate(_yPlayHead) * _verticalMultiplier;
        
        return new Vector3(xPos, yPos, 0f);
    }
}

[RequireComponent(typeof(CharacterController))]
public class FPSController : MonoBehaviour {
    // Inspector assigned
    [SerializeField] private AudioCollection _footSteps;
    [SerializeField] private float _crouchingVolume = 0.2f;
    
    [SerializeField] private float _walkSpeed = 1f;
    [SerializeField] private float _runSpeed = 4.5f;
    [SerializeField] private float _crouchSpeed = 2f;
    [SerializeField] private float _jumpSpeed = 7.5f;
    [SerializeField] private float _staminaDepletion = 5.0f;
    [SerializeField] private float _staminaRecovery = 10.0f;
    private float _stickToGroundForce = 5f;
    private float _gravityMultiplier = 2.5f;
    // use standard assets mouse look class for mouse input -> Camera look control
    [SerializeField] private UnityStandardAssets.Characters.FirstPerson.MouseLook _mouseLook;
    // head moving curve
    [SerializeField] private CurveControllerBob _headBob = new CurveControllerBob();
    // how much does a zombie slow us down
    [SerializeField] [Range(0f, 1f)] private float _slowDownFactor = 0.5f;
    
    [Header("Shared variables")]
    [SerializeField] private SharedFloat _stamina;
    [SerializeField] private SharedVector3 _cameraShakerOffset;
    [SerializeField] private SharedVector3 _broadcastPosition;
    [SerializeField] private SharedVector3 _broadcastDirection;
    [SerializeField] private SharedInt _currentWeaponType;

    // Private variables
    private Vector2 _input = Vector2.zero;
    private Vector3 _moveDirection = Vector3.zero;
    private bool _previouslyGrounded = false;
    private bool _jumpButton = false;
    private bool _isWalking = true;
    private bool _isJumping = false;
    private bool _isCrouching = false;
    private PlayerStatus _currStatus = PlayerStatus.Standing;

    private float _speedOverride = 0f;
    // references
    private Camera _camera;
    private CharacterController _controller;
    // timers
    private float _fallingTimer = 0f;
    // starting camera position
    private Vector3 _startingCameraPos = Vector3.zero;
    // starting collider height
    private float _startingHeight;
    // collision handling
    private float _dragMultiplier = 1.0f;
    private float _dragMultiplierLimit = 1.0f;
    // if we want to stop moving but still be able to look around
    private bool _freezeMovement = false;
    private bool _disableInput;
    
    // Public properties
    public PlayerStatus CurrentStatus => _currStatus;
    public float WalkSpeed => _walkSpeed;
    public float RunSpeed => _runSpeed;
    public float SpeedOverride {
        get => _speedOverride;
        set => _speedOverride = value;
    }
    public bool IsJumping => _isJumping;
    public float DragMultiplierLimit {
        get => _dragMultiplierLimit;
        set => _dragMultiplierLimit = Mathf.Clamp01(value);
    }
    public float DragMultiplier {
        get => _dragMultiplier;
        set => _dragMultiplier = Mathf.Min(value, _dragMultiplierLimit);
    }

    public bool FreezeMovement {
        get => _freezeMovement;
        set => _freezeMovement = value;
    }

    public bool DisableInput {
        get => _disableInput;
        set => _disableInput = value;
    }

    public CharacterController CharacterController => _controller;

    protected void Start() {
        // get references
        _controller = GetComponent<CharacterController>();
        _camera = Camera.main;
        
        // init the mouse look script
        _mouseLook.Init(transform, _camera.transform);
        
        // get the starting position of the camera
        _startingCameraPos = _camera.transform.localPosition;
        
        // initialize the head bob curve
        _headBob.Initialize();
        _headBob.RegisterEventCallback(1.5f, PlayFootstepSound);
        
        // set the starting height
        _startingHeight = _controller.height;
    }

    protected void Update() {
        // if we are falling, increment the timer
        if (_controller.isGrounded) _fallingTimer = 0f;
        else _fallingTimer += Time.deltaTime;

        if (!_disableInput) {
            // make the mouse look script rotate the camera
            if (Time.timeScale > Mathf.Epsilon)
                _mouseLook.LookRotation(transform, _camera.transform);
            
            // process the jump button
            if (!_jumpButton && !_isCrouching)
                _jumpButton = Input.GetButtonDown("Jump");
            
            // process the crouch button
            if (Input.GetButtonDown("Crouch")) {
                _isCrouching = true;
                _controller.height = _startingHeight / 2;
            }

            if (Input.GetButtonUp("Crouch")) {
                _isCrouching = false;
                _controller.height = _startingHeight;
            }
        }

        // calculate character status
        if (_isCrouching) {
            // crouching conditional is first because we dont want falling sounds when we crouch
            _currStatus = PlayerStatus.Crouching;
        }
        else if (!_previouslyGrounded && _controller.isGrounded) {
            if (_fallingTimer > 0.5f) {
                // TODO: play landing sound
            }

            _moveDirection.y = 0f;
            _isJumping = false;
            _currStatus = PlayerStatus.Landing;
        }
        else if (!_controller.isGrounded) {
            _currStatus = PlayerStatus.NotGrounded;
        }
        else if (_controller.velocity.sqrMagnitude < 0.01f) {
            _currStatus = PlayerStatus.Standing;
        }
        else if (_isWalking) {
            _currStatus = PlayerStatus.Walking;
        }
        else {
            _currStatus = PlayerStatus.Running;
        }

        _previouslyGrounded = _controller.isGrounded;
        
        // calculate stamina
        if (_currStatus == PlayerStatus.Running || _currStatus == PlayerStatus.NotGrounded)
            _stamina.Value = Mathf.Max(0f, _stamina.Value - _staminaDepletion * Time.deltaTime);
        else
            _stamina.Value = Mathf.Min(100f, _stamina.Value + _staminaRecovery * Time.deltaTime);

        _dragMultiplier = Mathf.Min(_dragMultiplier + Time.deltaTime, _dragMultiplierLimit);
    }

    protected void FixedUpdate() {
        // read input from axis
        float horizontal = _disableInput ? 0f : Input.GetAxis("Horizontal");
        float vertical = _disableInput ? 0f : Input.GetAxis("Vertical");
        _input = new Vector2(horizontal, vertical);
        // normalize vector if it exceeds 1 in combined length
        if (_input.sqrMagnitude > 1) _input.Normalize();

        _isWalking = !Input.GetKey(KeyCode.LeftShift);
        
        // Check if we are running or walking and set our speed accordingly
        float speed;
        if (_isCrouching)
            speed = _crouchSpeed;
        else if (_isWalking)
            speed = _walkSpeed;
        else
            speed = Mathf.Lerp(_walkSpeed, _runSpeed, _stamina.Value/100f);

        if (_speedOverride > 0f)
            speed = _speedOverride;
        
        // move along the camera forward as it is the direction that is being aimed at
        Vector3 desiredMove = transform.forward * _input.y + transform.right * _input.x;
        // get a normal for the surface that we are standing on to move along it
        RaycastHit hit;
        if (Physics.SphereCast(transform.position, _controller.radius, Vector3.down, out hit, _controller.height / 2f, 1)) {
            desiredMove = Vector3.ProjectOnPlane(desiredMove, hit.normal).normalized;
        }

        // slow the player down based on the weapon he is holding
        float weaponSlowDown;
        if (_currentWeaponType.Value == 2)
            weaponSlowDown = 0.7f;
        else if (_currentWeaponType.Value == 1)
            weaponSlowDown = 0.9f;
        else
            weaponSlowDown = 1f;
        
        // scale movement by our walking/running speed
        _moveDirection.x = !_freezeMovement ? desiredMove.x * speed * _dragMultiplier * weaponSlowDown : 0.0f;
        _moveDirection.z = !_freezeMovement ? desiredMove.z * speed * _dragMultiplier * weaponSlowDown : 0.0f;

        if (_controller.isGrounded) {
            // apply force to keep sticking to the floor
            _moveDirection.y = -_stickToGroundForce;
            
            // if the jump button was pressed apply speed up
            if (_jumpButton) {
                _moveDirection.y = _jumpSpeed;
                _jumpButton = false;
                _isJumping = true;
                // TODO: play jumping sound
            }
        } else {
            // if we are falling, multiply gravity by our variable
            _moveDirection += Physics.gravity * _gravityMultiplier * Time.fixedDeltaTime;
        }
        
        // move the character
        _controller.Move(_moveDirection * Time.fixedDeltaTime);
    
        
        Vector3 speedXZ = new Vector3(_controller.velocity.x, 0f, _controller.velocity.z);
        if (speedXZ.magnitude > 0.01f) {
            _camera.transform.localPosition = _startingCameraPos + _headBob.GetVectorOffset(speedXZ.magnitude * (_isWalking||_isCrouching ? 0.7f : 0.6f)) + _cameraShakerOffset.Value;
        } else {
            _camera.transform.localPosition = _startingCameraPos + _cameraShakerOffset.Value;
        }
        
        // update broadcasters
        _broadcastDirection.Value = transform.forward;
        _broadcastPosition.Value = transform.position;
    } 

    void PlayFootstepSound() {
        if (_isJumping) return;
        
        AudioClip clip;
        clip = _isCrouching ? _footSteps[1] : _footSteps[0];

        AudioManager.instance.PlaySound("Player",
            clip,
            transform.position,
            _isCrouching ? _footSteps.Volume * _crouchingVolume : _footSteps.Volume,
            _footSteps.SpatialBlend,
            _footSteps.Priority);
    }

    public void SlowDown() {
        _dragMultiplier = 1.0f - _slowDownFactor;
    }
}
