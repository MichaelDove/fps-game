﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorEventHandler : MonoBehaviour {
    private CharacterManager _characterManager;
    
    void Start() {
        _characterManager = GetComponentInParent<CharacterManager>();
    }

    public void FireWeaponEvent(int direction) {
        _characterManager.DoDamage(direction);
    }
}
