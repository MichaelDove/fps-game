﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISoundEmitter : MonoBehaviour {
    // Insperctor assigned
    [SerializeField] private float _decayRate = 1f;
    
    // private variables
    private SphereCollider _collider = null;
    private float _startRadius = 0f;
    private float _endRadius = 0f;
    private float _counter = 0f;
    private float _counterSpeed = 0f;
    
    void Awake() {
        // get collider
        _collider = GetComponent<SphereCollider>();
        // set radius value
        _startRadius = _endRadius = _collider.radius;
        
        // set counter
        if (_decayRate > 0.02f)
            _counterSpeed = 1 / _decayRate;
        else
            _counterSpeed = 0;
    }

    private void FixedUpdate() {
        _counter = Mathf.Clamp01(_counter + Time.fixedDeltaTime * _counterSpeed);
        // lerp radius to the smaller radius
        _collider.radius = Mathf.Lerp(_startRadius, _endRadius, _counter);
    
        // if radius is almost zero, disable it
        if (_collider.radius < Mathf.Epsilon) _collider.enabled = false;
        else _collider.enabled = true;
    }

    public void SetRadius(float newRadius, bool instantResize = false) {
        if (newRadius == _endRadius) return; // return if we are already interpolating to the wanted radius 
        // instantly set radius to max when we land or run
        _startRadius = instantResize || newRadius > _collider.radius ? newRadius:_collider.radius;
        _endRadius = newRadius;
        _counter = 0.0f;
    }
}
