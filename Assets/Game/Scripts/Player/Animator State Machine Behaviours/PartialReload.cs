﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartialReload : ArmsBase {
    // Inspector assigned
    public InventoryWeaponType weaponType = InventoryWeaponType.None;

    // Internals
    protected int _reloadHash = Animator.StringToHash("Reload");
    protected int _reloadRepeatHash = Animator.StringToHash("Reload Repeat");

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        int reloadRepeat = animator.GetInteger(_reloadRepeatHash);
        reloadRepeat = Mathf.Max(reloadRepeat - 1, 0);
        animator.SetInteger(_reloadRepeatHash, reloadRepeat);

        if (reloadRepeat == 0) {
            CharacterManager.ReloadWeapon_AnimatorCallback(weaponType);
            animator.SetBool(_reloadHash, false);
        }
    }
}
