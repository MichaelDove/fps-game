﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateFlashlight : ArmsBase {
    // Inspector assigned
    public bool Activate = true;
    public FlashlightType FlashlightType = FlashlightType.Primary;
    
    // Internals
    private int _commandStreamHash = Animator.StringToHash("Command Stream");
    private int _flashlightHash = Animator.StringToHash("Flashlight");
    private bool _done = false;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        CharacterManager.ActivateFlashlightMesh_AnimatorCallback(true, FlashlightType);
        _done = false;
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (_done) return;
        if (!animator.GetBool(_flashlightHash) && Activate) return;

        float commandValue = animator.GetFloat(_commandStreamHash);
        if (commandValue > 0.9f) {
            CharacterManager.ActivateFlashlightLight_AnimatorCallback(Activate, FlashlightType);
            _done = true;
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (!Activate) {
            CharacterManager.ActivateFlashlightLight_AnimatorCallback(false, FlashlightType);
            CharacterManager.ActivateFlashlightMesh_AnimatorCallback(false, FlashlightType);
        }
    }
}
