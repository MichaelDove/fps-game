﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchWeapon : ArmsBase {
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        CharacterManager.DisableWeapon_AnimatorCallback();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        CharacterManager.EnableWeapon_AnimatorCallback();
    }
}
