﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmsBase : StateMachineBehaviour {
    public ScriptableObject Identifier;
    [HideInInspector] public CharacterManager CharacterManager;
    [HideInInspector] public AnimatorStateCallback CallbackHandler;
}
