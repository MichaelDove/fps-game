﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonPartialReload : ArmsBase {
    // Inspector assigned
    public InventoryWeaponType weaponType = InventoryWeaponType.None;
    
    // Internals
    private int _reloadHash = Animator.StringToHash("Reload");

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        CharacterManager.ReloadWeapon_AnimatorCallback(weaponType);
        animator.SetBool(_reloadHash, false);
    }
}
