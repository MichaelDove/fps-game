﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NightVisionProcessor : ArmsBase {
    // Inspector assigned
    [Range(0f, 1f)] public float NormalizedActivationTime = 0.5f;
    public AudioCollection Collection;
    
    // Hashes
    private int _toggleNightVisionHash = Animator.StringToHash("Toggle Night Vision");
    private bool _hasBeenToggled = false;
    
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        AudioManager.instance.PlaySound(Collection, Collection[CharacterManager.NightVisionEnabled ? 1 : 0], CharacterManager.transform.position);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (_hasBeenToggled) return;

        if (animator.GetCurrentAnimatorStateInfo(layerIndex).normalizedTime >= NormalizedActivationTime) {
            _hasBeenToggled = true;
            CharacterManager.ToggleNightVision_AnimatorCallback();
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        CharacterManager.EndToggleNightVision_AnimatorCallback();
        _hasBeenToggled = false;
    }
}
