﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedStateTransition : StateMachineBehaviour {
    // Inspector assigned
    public float IdleTimeout = 10f;
    public float TransitionTime = 0.2f;
    public string StateName = "";
    
    // Internals
    private float _timer;
    private int _stateHash;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        _timer = 0f;
        _stateHash = Animator.StringToHash(StateName);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        _timer += Time.deltaTime;
        if (_timer > IdleTimeout) {
            _timer = 0;
            animator.CrossFade(_stateHash, TransitionTime);
        }
    }
}
