﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunReload : PartialReload {
    // Internals
    protected int _commandStreamHash = Animator.StringToHash("Command Stream");
    protected float _previousCommand = 0f;

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        float command = animator.GetFloat(_commandStreamHash);

        if (!command.Equals(_previousCommand) && !command.Equals(0f)) {
            _previousCommand = command;
            if (command.Equals(1f)) CallbackHandler.OnAction(0);
            else if (command.Equals(2f)) CallbackHandler.OnAction(1);
        }
    }
}
