﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunAnimatorStateCallback : WeaponAnimatorStateCallback {
    [SerializeField] private GameObject _shotgunSheel;

    public override void OnAction(int context, CharacterManager characterManager = null) {
        switch (context) {
            case 1:
                _shotgunSheel.SetActive(true);
                break;
            case 0:
                _shotgunSheel.SetActive(false);
                break;
        }
    }
}
