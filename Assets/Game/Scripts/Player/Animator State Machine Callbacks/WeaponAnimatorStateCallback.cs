﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class MuzzleFlashDescriptor {
    public GameObject MuzzleFlash;
    public float LightIntensity = 1f;
    public Color LightColor = Color.white;
    public float Range = 10f;
}

public class WeaponAnimatorStateCallback : AnimatorStateCallback {
    // Inspector assigned
    public Light MuzzleFlashLight;
    public List<MuzzleFlashDescriptor> MuzzleFlashFrames = new List<MuzzleFlashDescriptor>();
    public float MuzzleFlashTime = 0.1f;
    public int MuzzleFlasheshPerShot = 1;
    
    // Internals
    protected int _currentMuzzleFlashIndex = 0;
    protected int _lightReferenceCount = 0;

    protected virtual void OnEnable() {
        _lightReferenceCount = 0;
        _currentMuzzleFlashIndex = 0;
    }

    protected void OnDisable() {
        MuzzleFlashLight.gameObject.SetActive(false);
        foreach (var frame in MuzzleFlashFrames) {
            frame.MuzzleFlash.SetActive(false);
        }
    }

    public void DoMuzzleFlash() {
        if (MuzzleFlasheshPerShot < 1) return;

        if (MuzzleFlasheshPerShot > 1)
            StartCoroutine(EnableMuzzleFlashSequence());
        else
            EnableMuzzleFlash();
    }

    protected void EnableMuzzleFlash() {
        var frame = MuzzleFlashFrames[_currentMuzzleFlashIndex];
        if (frame != null) {
            frame.MuzzleFlash.SetActive(true);
            // configure the light
            MuzzleFlashLight.color = frame.LightColor;
            MuzzleFlashLight.intensity = frame.LightIntensity;
            MuzzleFlashLight.range = frame.Range;
            MuzzleFlashLight.gameObject.SetActive(true);

            _lightReferenceCount++;
            StartCoroutine(DisableMuzzleFlash(_currentMuzzleFlashIndex));

            _currentMuzzleFlashIndex = _currentMuzzleFlashIndex >= MuzzleFlashFrames.Count-1 ? 0 : _currentMuzzleFlashIndex + 1;
        }
    }

    protected IEnumerator EnableMuzzleFlashSequence() {
        int counter = 0;
        float timer = float.MaxValue;

        while (counter < MuzzleFlasheshPerShot) {
            timer += Time.deltaTime;
            if (timer > MuzzleFlashTime) {
                EnableMuzzleFlash();
                counter++;
                timer = 0f;
            }

            yield return null;
        }
    }

    protected IEnumerator DisableMuzzleFlash(int index) {
        yield return new WaitForSeconds(MuzzleFlashTime);

        MuzzleFlashFrames[index].MuzzleFlash.SetActive(false);
        _lightReferenceCount--;
        if (_lightReferenceCount <= 0)
            MuzzleFlashLight.gameObject.SetActive(false);
    }
}
