﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorStateCallback : MonoBehaviour {
    public virtual void OnAction(string context, CharacterManager characterManager = null) { }
    public virtual void OnAction(int context, CharacterManager characterManager = null) { }
    public virtual void OnAction(float context, CharacterManager characterManager = null) { }
    public virtual void OnAction(Object context, CharacterManager characterManager = null) { }
}
