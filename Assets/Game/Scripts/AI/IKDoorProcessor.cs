﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.PlayerLoop;
using Random = UnityEngine.Random;

public class IKDoorProcessor : MonoBehaviour {
    [SerializeField] private LayerMask _layerMask = -1;

    [Header("Approach Settings")] 
    [SerializeField] private float _jogSlowDownDistance = 2f;
    [SerializeField] private float _sprintSlowDownDistance = 3.25f;
    [SerializeField] private float _approachSpeed = 1f;
    [SerializeField] private float _activateDistance = 0.4f;

    [Header("Interrupt Settings")] 
    [SerializeField] private string _interruptTrigger = "Hit";
    [SerializeField] private float _blowbackOffset = 0.25f;

    [Header("IK Blending")] 
    [SerializeField] [Range(0f, 1f)] private float _headIKWeight = 1f;
    [SerializeField] private bool _drawDebugLines = false;    
    
    // Animator hashes
    private int _openDoorHash = Animator.StringToHash("OpenDoor");
    private int _comChannelHash = Animator.StringToHash("ComChannelDoor");
    private int _ikTargetWeightHash = Animator.StringToHash("HandIKTargetWeight");
    private int _handMirrorHash = Animator.StringToHash("HandsIKMirrorLeft");
    private int _openDoorMirrorHash = Animator.StringToHash("OpenDoorMirror");
    
    // Internals
    private NavMeshAgent _agent;
    private AIZombieStateMachine _zombieStateMachine;
    private bool _leftSide;

    private Transform _ikTarget;
    private InteractiveDoor _door;

    private bool _processingDoor;
    private bool _activatingDoor;
    private bool _attemptSuccessful;

    private float _autoTimer;
    private float _headLerp;

    private Vector3 _standingPosition;
    private Vector3 _lookDirection;

    private void OnEnable() {
        _zombieStateMachine = GetComponent<AIZombieStateMachine>();
        
        // Add listeners to the events
        _zombieStateMachine.OnDeath.AddListener(OnResetSystem);
        _zombieStateMachine.OnTakenDamage.AddListener(OnResetSystem);
        _zombieStateMachine.OnRagdoll.AddListener(OnResetSystem);
    }

    private void OnDisable() {
        // Remove listeners from the events
        _zombieStateMachine.OnDeath.RemoveListener(OnResetSystem);
        _zombieStateMachine.OnTakenDamage.RemoveListener(OnResetSystem);
        _zombieStateMachine.OnRagdoll.RemoveListener(OnResetSystem);
    }

    // Start is called before the first frame update
    void Start() {
        _agent = _zombieStateMachine.NavAgent;

        IKDoorProcessorReset smb = _zombieStateMachine.Animator.GetBehaviour<IKDoorProcessorReset>();
        smb.Behaviour = this;
    }
    
    // Reset the system
    public void OnResetSystem(AIZombieStateMachine npc) {
        if (_door)
            _door.OnPlayerInteractionEvent.RemoveListener(OnAbortProcessing);
        _zombieStateMachine.AnimatorIKEvent.RemoveListener(UpdateIK);

        _processingDoor = false;
        _activatingDoor = false;
        _zombieStateMachine.Animator.SetBool(_openDoorHash, false);

        if (_zombieStateMachine.Health <= 0)
            enabled = false;    
    }

    public void OnAbortProcessing(CharacterManager charManager, bool success, bool isOpen) {
        _zombieStateMachine.Animator.SetTrigger(_interruptTrigger);
        _zombieStateMachine.transform.position -= _zombieStateMachine.transform.forward * _blowbackOffset;
        OnResetSystem(null);
    }

    private void UpdateIK(AIStateMachine stateMachine, int layer, Animator anim) {
        if (_door.IsAnimating) return;
        
        AvatarIKGoal ikGoal;
        HumanBodyBones ikBone;

        bool mirrorNeeded = anim.GetFloat(_handMirrorHash) > 0.9f;

        if (_leftSide) {
            ikGoal = AvatarIKGoal.LeftHand;
            ikBone = HumanBodyBones.LeftHand;
            
            if (mirrorNeeded)
                anim.SetBool(_openDoorMirrorHash, true);
            else
                anim.SetBool(_openDoorMirrorHash, false);
        }
        else {
            ikGoal = AvatarIKGoal.RightHand;
            ikBone = HumanBodyBones.RightHand;
            
            if (mirrorNeeded)
                anim.SetBool(_openDoorMirrorHash, false);
            else
                anim.SetBool(_openDoorMirrorHash, true);
        }

        float ikWeight = anim.GetFloat(_ikTargetWeightHash);
        
        // set the ik weight/position
        anim.SetIKPosition(ikGoal, _ikTarget.position);
        anim.SetIKPositionWeight(ikGoal, ikWeight);

        float headDoorIKWeight = ikWeight * _headIKWeight;
        _headLerp += Time.deltaTime;
        _zombieStateMachine.CurrentLookAtWeight = Mathf.Lerp(_zombieStateMachine.CurrentLookAtWeight, headDoorIKWeight, Time.deltaTime);

        var newLookAtPos = Vector3.Lerp(_zombieStateMachine.CurrentLookAtPosition, _ikTarget.position, _headLerp);
        anim.SetLookAtPosition(newLookAtPos);
        anim.SetLookAtWeight(headDoorIKWeight);

        if (_activatingDoor || !_processingDoor) return;

        var handOffset = _ikTarget.position - anim.GetBoneTransform(ikBone).position;
        _zombieStateMachine.transform.position = Vector3.Lerp(transform.position, _standingPosition + (handOffset * ikWeight), Time.deltaTime);

        if (anim.GetFloat(_comChannelHash) > 0.9f) {
            if (_door.AIDoorType == AIInteractiveDoorType.AICanOpen && _attemptSuccessful) {
                _door.Use(_zombieStateMachine.transform.position, true);
            }
            else {
                ClearTarget();
            }
            
            // we have now tried to activate the door
            _activatingDoor = true;
        }
    }

    private void ClearTarget() {
        // Clear the target
        _zombieStateMachine.ClearTarget();
        _zombieStateMachine.StopPlayerTracking();
        _zombieStateMachine.SetWaypointTarget(true);
    }

    // Update is called once per frame
    void Update() {
        Vector3[] corners;
        int cornersCount;
        _zombieStateMachine.GetPathCorners(out corners, out cornersCount);
        if (cornersCount == 0)
            return;

        _autoTimer -= Time.deltaTime;

        if (_autoTimer > 0 || _processingDoor)
            return;

        float distanceRemaining = _zombieStateMachine.Speed > 2f ? _sprintSlowDownDistance : _jogSlowDownDistance;
        float distanceTracked = 0;
        InteractiveDoor currDoor = null;

        if (_drawDebugLines) {
            for (int i = 1; i < cornersCount; i++) {
                Debug.DrawLine(corners[i - 1], corners[i], Color.red);
            }
        }

        Vector3 nextStartPoint, nextEndPoint;

        for (int i = 0; i < cornersCount; i++) {
            // we have searched ahead enough
            if (distanceRemaining <= 0) break;
            
            // define end points of next ray to test
            if (i > 0) {
                nextStartPoint = corners[i - 1];
                nextEndPoint = corners[i];
            } else {
                nextStartPoint = transform.position;
                nextEndPoint = corners[i];
            }

            float distance = Vector3.Distance(nextStartPoint, nextEndPoint);
            if (distance >= distanceRemaining) {
                distance = distanceRemaining;
                distanceRemaining = 0;
                nextEndPoint = nextStartPoint + (nextEndPoint - nextStartPoint).normalized*distance;
            }
            else {
                distanceRemaining -= distance;
            }
            
            if (_drawDebugLines) { 
                Debug.DrawLine(nextStartPoint, nextEndPoint);
            }
            
            // perform a raycast
            RaycastHit hit;
            if (Physics.Linecast(nextStartPoint, nextEndPoint, out hit, _layerMask.value, QueryTriggerInteraction.Ignore)) {
                // get the interactive door
                currDoor = hit.collider.GetComponentInParent<InteractiveDoor>();
                if (!currDoor) continue;

                if (_door && _door != currDoor) {
                    _door.AIid = -1;
                    _door = null;
                    _zombieStateMachine.DisabledAvoidance = false;
                }

                if ((currDoor.AIid != -1 && currDoor.AIid != _zombieStateMachine.GetInstanceID()) || currDoor.IsAnimating) {
                    _zombieStateMachine.SpeedOverride = 0;
                    return;
                }

                currDoor.AIid = _zombieStateMachine.GetInstanceID();
                _door = currDoor;

                if (!currDoor.IsOpen) {
                    _zombieStateMachine.DisabledAvoidance = true;
                    
                    if (!_zombieStateMachine.Crawling &&
                        distanceTracked + hit.distance <= _activateDistance &&
                        Vector3.Angle(transform.forward, hit.point - transform.position) < 45f
                        && !currDoor.IsAutoOpen) {
                        // open the door
                        // calculate the ideal standing position/orientation
                        _standingPosition = hit.point - (transform.forward * _activateDistance);
                        _lookDirection = currDoor.GetLookDirection(transform.position);

                        // register a listener for player interaction
                        currDoor.OnPlayerInteractionEvent.AddListener(OnAbortProcessing);

                        // is the ik target on the left or the right
                        _ikTarget = currDoor.GetIKTarget(transform.position, hit.collider.GetInstanceID());
                        _leftSide = _zombieStateMachine.transform.InverseTransformPoint(_ikTarget.position).x < 0;

                        // stop the zombie
                        _zombieStateMachine.SpeedOverride = 0;
                        _headLerp = 0;

                        _zombieStateMachine.AnimatorIKEvent.AddListener(UpdateIK);

                        // play the opening animation
                        _zombieStateMachine.Animator.SetBool(_openDoorHash, true);
                        // we are now processing the door
                        _processingDoor = true;
                        _activatingDoor = false;
                        _attemptSuccessful = Random.value < _zombieStateMachine.Pinpointing;

                        return;
                    }
                    else {
                        if (currDoor.IsAutoOpen) {
                            currDoor.Use(transform.position, false);
                            _processingDoor = false;

                            _autoTimer = 1f;
                        }
                        else if (_zombieStateMachine.IsCrawling) {
                            ClearTarget();
                            _zombieStateMachine.SpeedOverride = 0;
                            _autoTimer = 0;
                            return;
                        }

                        if (_zombieStateMachine.Speed >= _approachSpeed)
                            _zombieStateMachine.SpeedOverride = _approachSpeed;
                        else
                            _zombieStateMachine.SpeedOverride = -1;
                        return;
                    }
                }
                else {
                    break;
                }
            }

            distanceTracked += distance;
        }

        if (!currDoor && _door) {
            _door.AIid = -1;
            _door = null;
            _zombieStateMachine.DisabledAvoidance = false;
        }

        _zombieStateMachine.SpeedOverride = -1;
    }

    private void LateUpdate() {
        if (_door && _processingDoor) {
            _agent.velocity = Vector3.zero;
            transform.rotation = Quaternion.LookRotation(_lookDirection);
        }
    }
}
