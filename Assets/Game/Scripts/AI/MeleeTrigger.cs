﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeTrigger : MonoBehaviour {
    private void OnTriggerEnter(Collider other) {
        AIStateMachine machine = GameSceneManager.instance.GetAIStateMachine(other.GetInstanceID());
        if (machine) {
            machine.InMeleeRange = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        AIStateMachine machine = GameSceneManager.instance.GetAIStateMachine(other.GetInstanceID());
        if (machine) {
            machine.InMeleeRange = false;
        }
    }
}
