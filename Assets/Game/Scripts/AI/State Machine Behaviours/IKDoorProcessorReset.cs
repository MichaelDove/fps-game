﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKDoorProcessorReset : StateMachineBehaviour {
    int _openDoorHash = Animator.StringToHash("OpenDoor");

    private IKDoorProcessor _behaviour;

    public IKDoorProcessor Behaviour {
        set => _behaviour = value;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (_behaviour) {
            _behaviour.OnResetSystem(null);
            animator.SetBool(_openDoorHash, false);
        }
    }
}
