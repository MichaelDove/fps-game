﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioCollectionPlayer : AIStateMachineLink {
    // Inspector assigned
    [SerializeField] private ComChannelName _commandChannel = ComChannelName.ComChannel1;
    [SerializeField] private AudioCollection _collection;
    [SerializeField] private CustomCurve _customCurve;
    [SerializeField] private StringList _layerExclusions;

    // Private
    private int _previousCommand = 0;
    private AudioManager _audioManager;
    private int _commandChannelHash = 0;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        _audioManager = AudioManager.instance;
        _previousCommand = 0;
        
        if (_commandChannelHash == 0)
            _commandChannelHash = Animator.StringToHash(_commandChannel.ToString());
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        // don't make any sounds if our layer weight is zero
        if (layerIndex != 0 && animator.GetLayerWeight(layerIndex).Equals(0f)) return;

        if (_layerExclusions != null) {
            for (int i = 0; i < _layerExclusions.Count; i++) {
                if (_stateMachine.IsLayerActive(_layerExclusions[i]))
                    return;
            }
        }
        
        float customCommand = _customCurve == null ? 0 : _customCurve.Evaluate(stateInfo.normalizedTime - (long)stateInfo.normalizedTime);

        //int command = Mathf.FloorToInt(animator.GetFloat(_commandChannelHash));
        float commandF;
        // use custom curve if necessary 
        if (customCommand != 0) commandF = customCommand;
        else commandF = animator.GetFloat(_commandChannelHash);
        
        int command;
        // check if the command is at the top of the curve (and not on its way down), so we don't get unwanted footsteps in between
        if ((commandF * 100) % 100 == 0)
            command = (int) commandF;
        else
            command = 0;
        
        if (_previousCommand != command && command > 0) {
            int bank = Mathf.Max(0, Mathf.Min(command - 1, _collection.BankCount - 1));
            _audioManager.PlaySound(_collection.AudioGroup, _collection[bank], _stateMachine.transform.position, _collection.Volume, _collection.SpatialBlend, _collection.Priority);
        }
        _previousCommand = command;
    }
}
