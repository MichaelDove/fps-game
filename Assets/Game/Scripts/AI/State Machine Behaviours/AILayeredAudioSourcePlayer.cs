﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AILayeredAudioSourcePlayer : AIStateMachineLink {
    // Inspector assigned
    [SerializeField] private AudioCollection _collection = null;
    [SerializeField] private int _bank = 0;
    [SerializeField] private bool _looping = true;
    [SerializeField] private bool _stopOnExit = false;
    
    // Private
    private float _prevLayerWeight = 0.0f;

    public override void OnStateEnter(Animator anim, AnimatorStateInfo stateInfo, int layerIndex) {
        float layerWeight = anim.GetLayerWeight(layerIndex);
        if (layerIndex == 0 || layerWeight > 0.1f) {
            _stateMachine.PlayAudio(_collection, _bank, layerIndex, _looping);
        } else {
            _stateMachine.StopAudio(layerIndex);  
        }

        _prevLayerWeight = layerWeight;
    }

    public override void OnStateUpdate(Animator anim, AnimatorStateInfo stateInfo, int layerIndex) {
        float layerWeight = anim.GetLayerWeight(layerIndex);
        if (_prevLayerWeight != layerWeight) {
            if (layerWeight > 0.1f) _stateMachine.PlayAudio(_collection, _bank, layerIndex, _looping);
            else                    _stateMachine.StopAudio(layerIndex);
        }

        _prevLayerWeight = layerWeight;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (_stopOnExit)
            _stateMachine.StopAudio(layerIndex);
    }
}
