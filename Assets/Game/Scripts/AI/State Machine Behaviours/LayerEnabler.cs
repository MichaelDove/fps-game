﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerEnabler : AIStateMachineLink {
    public bool OnEnter;
    public bool OnExit;
    
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        _stateMachine.SetLayerActive(animator.GetLayerName(layerIndex), OnEnter);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        _stateMachine.SetLayerActive(animator.GetLayerName(layerIndex), OnExit);
    }
}
