﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootMotionConfigurator : AIStateMachineLink {
    [SerializeField] private bool _rootPosition;
    [SerializeField] private bool _rootRotation;
    [SerializeField] private bool _forceRagdoll;

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        _stateMachine.SetRootMotionRequest(_rootPosition, _rootRotation);
        var _zombieStateMachine = _stateMachine as AIZombieStateMachine;
        _zombieStateMachine.ForceRagdoll = _forceRagdoll;
    }
}