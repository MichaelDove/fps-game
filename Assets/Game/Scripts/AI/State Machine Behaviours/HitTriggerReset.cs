﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitTriggerReset : AIStateMachineLink {
    private int _hitHash = Animator.StringToHash("Hit");

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.ResetTrigger(_hitHash);
    }
}
