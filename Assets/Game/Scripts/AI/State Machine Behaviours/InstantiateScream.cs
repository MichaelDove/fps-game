﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class InstantiateScream : AIStateMachineLink {
    int _screamingHash = Animator.StringToHash("Screaming");
    private bool _isProcessed = false;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller) {
        _isProcessed = false;
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        var _zombieStateMachine = _stateMachine as AIZombieStateMachine;
        if (!_zombieStateMachine) return;

        bool isScreaming = animator.GetFloat(_screamingHash) > 0.9f;

        if (!_isProcessed && isScreaming) {
            _isProcessed = true;
            _zombieStateMachine.Scream_AnimatorCallback();
        }
    }
}
