﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AISensor : MonoBehaviour {
    // Private
    private AIStateMachine _parentStateMachine = null;
    
    // Public
    public AIStateMachine ParentStateMachine {
        set { _parentStateMachine = value; } // setter for the parent state machine
    }
    
    // Process all the  events with the sensor and pass them on to the parent state machine
    private void OnTriggerEnter(Collider other) {
        if (_parentStateMachine != null)
            _parentStateMachine.OnSensorTrigger(AITriggerEvent.Enter, other);
    }

    private void OnTriggerStay(Collider other) {
        if (_parentStateMachine != null)
            _parentStateMachine.OnSensorTrigger(AITriggerEvent.Stay, other);
    }

    private void OnTriggerExit(Collider other) {
        if (_parentStateMachine != null)
            _parentStateMachine.OnSensorTrigger(AITriggerEvent.Exit, other);
    }
}
