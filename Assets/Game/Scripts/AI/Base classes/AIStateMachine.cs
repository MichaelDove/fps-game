﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

// different states that our agents will be in
public enum AIStateType {None, Idle, Alerted, Patrol, Attack, Feeding, Pursuit, Dead}
// different types of targets to walk towards
public enum AITargetType {None = 0, Waypoint = 10, Player = 50, Light = 40, Food = 20, Audio = 30}
// type of trigger events
public enum AITriggerEvent {Enter, Stay, Exit}

public enum AIBoneAlignment {front, back, left, right, up, down}

// ------------------------------ AI TARGET STRUCTURE ------------------------------------------------------------------
// we will set an AITarget so the agent will know where to walk towards
public struct AITarget {
    private AITargetType _type;
    private Collider _collider;
    private Vector3 _position;
    private float _distance;
    private float _time;
    
    public AITargetType type {get { return _type;}}
    public Collider collider {get { return _collider;}}
    public Vector3 position {get { return _position;}}
    public float distance {get { return _distance;} set { _distance = value;}}
    public float time {get { return _time; }}

    /// <summary>
    /// Change the target parameters.
    /// </summary>
    /// <param name="t"></param>
    /// <param name="c"></param>
    /// <param name="p"></param>
    /// <param name="d"></param>
    public void Set(AITargetType t, Collider c, Vector3 p, float d) {
        _type = t;
        _collider = c;
        _distance = d;
        _time = Time.time;

        // calculate the position of the target that is on the nav mesh
        NavMeshHit hit;
        if (NavMesh.SamplePosition(p, out hit, 5f, NavMesh.AllAreas)) {
            _position = hit.position;
        }
        else {
            Clear();
        }
    }

    /// <summary>
    /// Empty the target.
    /// </summary>
    public void Clear() {
        _type = AITargetType.None;
        _collider = null;
        _position = Vector3.zero;
        _distance = Mathf.Infinity;
        _time = 0;
    }
}

// ------------------------------ AI STATE MACHINE CLASS ---------------------------------------------------------------

// ------------------------------ EVENTS -------------------------------------------------------------------------------
[System.Serializable]
public class OnAnimatorIKEvent : UnityEvent<AIStateMachine, int, Animator> {}

[System.Serializable]
public class OnAnimatorMoveEvent : UnityEvent<AIStateMachine, Animator> {}

public abstract class AIStateMachine : MonoBehaviour {
    // Public:
    public AITarget Threat;

    // Set in the editor:
    [SerializeField] protected AIStateType _currentStateType = AIStateType.Idle;
    [SerializeField] protected Transform _rootBone;
    [SerializeField] protected AIBoneAlignment _rootBoneAlignment = AIBoneAlignment.front;
    [SerializeField] protected SphereCollider _targetTrigger = null;
    [SerializeField] protected SphereCollider _sensorTrigger = null;
    [SerializeField] [Range(0, 15)] protected float _stoppingDistance = 1f;
    [Header("Path Caculating")] 
    [SerializeField] protected float _repathDistanceMultiplier = 0.035f;
    [SerializeField] protected float _repathMinDuration = 0.05f;
    [SerializeField] protected float _repathMaxDuration = 5f;
    
    // Layered Audio Control
    protected ILayeredAudioSource _layeredAudioSource = null;
    
    // Nav area management
    protected Vector3[] _pathCorners = new Vector3[1000];
    protected int _pathCornersCount;

    // Protected:
    protected AIState _currentState = null;
    protected Dictionary<AIStateType, AIState> _states = new Dictionary<AIStateType, AIState>();
    protected AITarget _target = new AITarget();
    protected int _rootRotationRefCount = 0;
    protected int _rootPositionRefCount = 0;
    protected bool _destinationReached = false;
    protected List<Rigidbody> _bodyParts = new List<Rigidbody>();
    protected int _aiBodyPartLayer = -1;
    protected bool _disabledAvoidance;
    // Animation layer manager
    protected Dictionary<string, bool> _animLayersActive = new Dictionary<string, bool>();
    // Nav mesh pathing
    protected float _repathTimer;
    // Events
    public OnAnimatorMoveEvent AnimatorMoveEvent = new OnAnimatorMoveEvent();
    public OnAnimatorIKEvent AnimatorIKEvent = new OnAnimatorIKEvent();

    // Component cache
    protected Animator _anim = null;
    protected NavMeshAgent _navAgent = null;
    protected Collider _collider = null;

    
    // Getters:
    public Animator Animator => _anim;
    public NavMeshAgent NavAgent => _navAgent;
    public bool UseRootPosition => _rootPositionRefCount > 0;
    public bool UseRootRotation => _rootRotationRefCount > 0;
    public AITargetType TargetType => _target.type;
    public Vector3 TargetPosition => _target.position;
    public float TargetTime => _target.time;
    public Collider TargetCollider => _target.collider;
    public int TargetColliderID => _target.collider == null ? -1 : _target.collider.GetInstanceID();
    public bool DestinationReached => _destinationReached;
    public bool InMeleeRange { get; set; }
    public bool DisabledAvoidance {
        get => _disabledAvoidance;
        set => _disabledAvoidance = value;
    }
    public Vector3 DesiredLookDirection {
        get {
            Vector3 dir;
            if (_disabledAvoidance) {
                dir = _navAgent.steeringTarget - transform.position;
                dir.y = 0;
            } else {
                dir = _navAgent.desiredVelocity;
            }

            return dir;
        }
    }

    public void SetLayerActive(string layerName, bool active) {
        _animLayersActive[layerName] = active;
        if (!active)
            _layeredAudioSource.Stop(_anim.GetLayerIndex(layerName));
    }

    public bool InvalidPath {
        get {
            // return true if we have no path
            return (_navAgent.isPathStale ||
                    (_navAgent.hasPath && _navAgent.pathPending) ||
                    _navAgent.pathStatus != NavMeshPathStatus.PathComplete);
        }
    }

    public bool IsLayerActive(string layerName) {
        bool result;
        if (_animLayersActive.TryGetValue(layerName, out result))
            return result;
        return false;
    }

    public bool PlayAudio(AudioCollection pool, int bank, int layer, bool looping = true) {
        return _layeredAudioSource.Play(pool, bank, layer, looping);
    }

    public void StopAudio(int layer) {
        _layeredAudioSource.Stop(layer);
    }

    public void MuteAudio(bool mute) {
        _layeredAudioSource.Mute(mute);
    }

    public Vector3 SensorPosition {
        get {
            // because the objects get scaled when in the hierarchy, we need to use the lossy scale 
            if (_sensorTrigger == null) return Vector3.zero;
            Vector3 center = _sensorTrigger.center;
            Vector3 lossyScale = _sensorTrigger.transform.lossyScale;

            // apply all the scales of the parent objects (lossyScale)
            Vector3 point = _sensorTrigger.transform.position;
            point.x += center.x * lossyScale.x;
            point.y += center.y * lossyScale.y;
            point.z += center.z * lossyScale.z;

            return point;
        }
    }

    public float SensorRadius {
        get {
            // because the objects get scaled when in the hierarchy, we need to use the lossy scale 
            if (_sensorTrigger == null) return 0f;
            Vector3 lossyScale = _sensorTrigger.transform.lossyScale;

            // the radius is multiplied with the biggest lossyScale 
            float scale = Mathf.Max(lossyScale.x, lossyScale.y, lossyScale.z);
            return _sensorTrigger.radius * scale;
        }
    }

    // ------------------------------ UNITY METHODS --------------------------------------------------------------------
    // ------------------------------ START and AWAKE
    protected void Awake() {
        // get the references 
        _anim = GetComponent<Animator>();
        _navAgent = GetComponent<NavMeshAgent>();
        _collider = GetComponent<Collider>();
        
        // get audio source
        AudioSource source = GetComponent<AudioSource>();

        // do we have a valid scene manager
        if (GameSceneManager.instance != null) {
            // register scene manager with colliders
            if (_collider) GameSceneManager.instance.RegisterStateMachine(_collider.GetInstanceID(), this);
            if (_sensorTrigger) GameSceneManager.instance.RegisterStateMachine(_sensorTrigger.GetInstanceID(), this);
        }
        
        // get body part layer
        _aiBodyPartLayer = LayerMask.NameToLayer("AI Body Part");
        
        // get the body parts
        Rigidbody[] bodies = _rootBone.GetComponentsInChildren<Rigidbody>();
        
        foreach (var bodyPart in bodies) {
            if (bodyPart != null && bodyPart.gameObject.layer == _aiBodyPartLayer) {
                _bodyParts.Add(bodyPart);
                GameSceneManager.instance.RegisterStateMachine(bodyPart.GetInstanceID(), this);
            }
        }
        
        // register the layered audio source
        _layeredAudioSource = AudioManager.instance.RegisterLayeredAudioSource(source, _anim.layerCount);
    }
    
    protected virtual void Start() {
        if (_sensorTrigger != null) {
            AISensor script = _sensorTrigger.GetComponent<AISensor>();
            if (script != null)
                script.ParentStateMachine = this; // set a reference of the sensor script to this state machine
        }
        
        // add all AIState components on the object in an array of states
        AIState[] states = GetComponents<AIState>();
        foreach (AIState state in states) {
            // we can only have ONE of the each AI State types
            if (state != null && !_states.ContainsKey(state.Type())) {
                // set the parent state machine of the state
                state.SetStateMachine(this);
                _states.Add(state.Type(), state);
            }
        }
    
        // set the initial state type if available
        if (_states.ContainsKey(_currentStateType)) {
            _currentState = _states[_currentStateType];
            _currentState.OnStateEnter();
        } else {
            Debug.LogError("the state set is not in the dictionary");
            _currentState = null;
        }
    
        // set all of the state machine behaviours links the parent state machine
        if (_anim) {
            AIStateMachineLink[] links = _anim.GetBehaviours<AIStateMachineLink>();
            foreach (AIStateMachineLink link in links) {
                link.StateMachine = this;
            }
        }
        
    }

    // ------------------------------ UPDATE and FIXED UPDATE

    // remove the threats and calculate the new distance to the current target
    protected void FixedUpdate() {
        Threat.Clear();

        if (_target.type != AITargetType.None) {
            _target.distance = Vector3.Distance(transform.position, _target.position);
        }

        _destinationReached = false;
    }

    protected virtual void Update() {
        _repathTimer += Time.deltaTime;
        
        if (_currentState == null) return;
        
        GetPathCorners();
        
        // get the new state type if necessary
        AIStateType newStateType = _currentState.OnUpdate();
        
        if (newStateType != _currentStateType) {
            AIState newState = null;

            // if the new state is found in the dictionary use it, if not switch to the 'idle' state
            if (_states.ContainsKey(newStateType)) {
                newState = _states[newStateType];   
            }
            else {
                Debug.Log(newStateType + " is not found, switching to 'Idle' state");
                newState = _states[AIStateType.Idle];
            }
            
            _currentState.OnStateExit();
            newState.OnStateEnter();
            _currentState = newState;
            _currentStateType = newStateType;
        }
    }

    private void GetPathCorners() {
        if (!_navAgent || !_navAgent.hasPath || _navAgent.isPathStale || _navAgent.pathPending || _navAgent.pathStatus != NavMeshPathStatus.PathComplete) {
            _pathCornersCount = 0;
            return;
        }

        _pathCornersCount = _navAgent.path.GetCornersNonAlloc(_pathCorners);
    }

    public void GetPathCorners(out Vector3[] corners, out int cornerCount) {
        cornerCount = _pathCornersCount;
        if (cornerCount == 0)
            corners = null;
        else
            corners = _pathCorners;
    }

    public bool RepairEmptyPath() {
        if (_target.type == AITargetType.None) return false;

        if (_pathCornersCount == 0) {
            Debug.Log("PATH EMPTY ERROR: Recalculating path!");
            _navAgent.ResetPath();
            _navAgent.SetDestination(_target.position);

            _repathTimer = 0f;
            _navAgent.isStopped = false;

            return true;
        }

        return false;
    }
    
    // ------------------------------ COLLISION METHODS ----------------------------------------------------------------
     
    protected virtual void OnTriggerEnter(Collider other) {
        // make sure that we hit the right target trigger (not one from another zombie)
        if (_targetTrigger == null || other != _targetTrigger) return;
        
        // update the destination reached boolean
        _destinationReached = true;
        
        // notify state that the target is reached
        if (_currentState)
            _currentState.OnDestinationReach(true);
    }

    protected void OnTriggerStay(Collider other) {
        // make sure that we hit the right target trigger (not one from another zombie)
        if (_targetTrigger == null || other != _targetTrigger) return;
        
        // update the destination reached boolean
        _destinationReached = true;
    }

    protected void OnTriggerExit(Collider other) {
        // make sure that we hit the right target trigger (not one from another zombie)
        if (_targetTrigger == null || other != _targetTrigger) return;
        
        // update the destination reached boolean
        _destinationReached = false;
        
        // notify state that the target is reached
        if (_currentState)
            _currentState.OnDestinationReach(false);
    }

    /// <summary>
    /// Notifies the current state that an object entered the sensor.
    /// </summary>
    /// <param name="type"></param>
    /// <param name="other"></param>
    public virtual void OnSensorTrigger(AITriggerEvent type, Collider other) {
        if (_currentState != null)
            _currentState.OnTriggerEvent(type, other);
    }

    public void SetStateOverride(AIStateType stateType) {
        if (_currentStateType != stateType && _states.ContainsKey(stateType)) {
            if (_currentState != null)
                _currentState.OnStateExit();

            _currentState = _states[stateType];
            _currentStateType = stateType;
            _currentState.OnStateEnter();
        }
    }
    
    // ------------------------------ ANIMATOR STATE METHODS -----------------------------------------------------------------

    protected void OnAnimatorMove() {
        if (_currentState != null)
            _currentState.OnAnimatorUpdate();
        
        AnimatorMoveEvent.Invoke(this, _anim);
    }

    protected void OnAnimatorIK(int layerIndex) {
        if (_currentState != null)
            _currentState.OnAnimatorIKUpdate();
        
        AnimatorIKEvent.Invoke(this, layerIndex, _anim);
    }

    /// <summary>
    /// Change if the nav agent should update the position/rotation.
    /// </summary>
    /// <param name="updatePosition"></param>
    /// <param name="updateRotation"></param>
    public void NavAgentControl(bool updatePosition, bool updateRotation) {
        _navAgent.updatePosition = updatePosition;
        _navAgent.updateRotation = updateRotation;
    }

    /// <summary>
    /// Increase or decrease the rootPosition / rootRotation reference count.
    /// </summary>
    /// <param name="rootPosition"></param>
    /// <param name="rootRotation"></param>
    public void AddRootMotionRequest(int rootPosition, int rootRotation) {
        _rootPositionRefCount += rootPosition;
        _rootRotationRefCount += rootRotation;
    }

    public void SetRootMotionRequest(bool rootPosition, bool rootRotation) {
        _rootPositionRefCount = rootPosition ? 1 : 0;
        _rootRotationRefCount = rootRotation ? 1 : 0;
    }

    // ------------------------------ TARGET METHODS -------------------------------------------------------------------
    
    /// <summary>
    /// Set a new target without an AITarget structure.
    /// </summary>
    /// <param name="t"></param>
    /// <param name="c"></param>
    /// <param name="p"></param>
    /// <param name="d"></param>
    public void SetTarget(AITargetType t, Collider c, Vector3 p, float d, bool ignoreRepathTimer = false) {
        if (_target.type != t)
            ignoreRepathTimer = true;
        
        _target.Set(t, c, p, d);
        
        SetPathToTarget(_stoppingDistance, ignoreRepathTimer);
    }
    
    /// <summary>
    /// Set a new target without an AITarget structure and override the stopping distance.
    /// </summary>
    /// <param name="t"></param>
    /// <param name="c"></param>
    /// <param name="p"></param>
    /// <param name="d"></param>
    /// <param name="s"></param>
    public void SetTarget(AITargetType t, Collider c, Vector3 p, float d, float s, bool ignoreRepathTimer = false) {
        if (_target.type != t)
            ignoreRepathTimer = true;
        
        _target.Set(t, c, p, d);

        SetPathToTarget(s, ignoreRepathTimer);
    }

    /// <summary>
    /// Set new target with an AITarget structure.
    /// </summary>
    /// <param name="t"></param>
    public void SetTarget(AITarget t, bool ignoreRepathTimer = false) {
        if (_target.type != t.type)
            ignoreRepathTimer = true;
        
        _target = t;

        SetPathToTarget(_stoppingDistance, ignoreRepathTimer);
    }

    protected void SetPathToTarget(float targetRadius, bool ignoreRepathTimer) {
        if (_target.type == AITargetType.None) {
            ClearTarget();
            return;
        }

        if ((_target.position - _navAgent.destination).sqrMagnitude > 0.1f) {
            if (ignoreRepathTimer ||
                Mathf.Clamp(_target.distance * _repathDistanceMultiplier, _repathMinDuration, _repathMaxDuration) < _repathTimer) {
                // recalculate the path
                if (!_navAgent.SetDestination(_target.position)) {
                    ClearTarget();
                    return;
                }

                _repathTimer = 0f;
                _navAgent.isStopped = false;
            }
        }

        if (_targetTrigger != null) {
            _targetTrigger.radius = _target.type == AITargetType.Player ? 0.2f : targetRadius;
            _targetTrigger.transform.position = _target.position;
            _targetTrigger.enabled = true;
        }
    }

    /// <summary>
    /// Clear the current target.
    /// </summary>
    public void ClearTarget() {
        _target.Clear();

        if (_targetTrigger != null) {
            _targetTrigger.enabled = false;
        }

        _navAgent.ResetPath();
    }
    
    // ------------------------------ OTHER ----------------------------------------------------------------------------
    public virtual void TakeDamage(Vector3 position, Vector3 force, int damage, Rigidbody bodyPart, CharacterManager charManager, int hitDirection=0) {}

    protected void OnDestroy() {
        if (AudioManager.instance != null)
            AudioManager.instance.UnregisterLayeredAudioSource(_layeredAudioSource);
    }
}
