﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AIState : MonoBehaviour {
    // Private
    protected AIStateMachine _stateMachine = null;
    
    // Public
    public virtual void SetStateMachine( AIStateMachine stateMachine) { _stateMachine = stateMachine; }
    
    // Default Handlers
    public virtual void OnStateEnter() {}
    public virtual void OnStateExit() {}
    public virtual void OnAnimatorUpdate() {
        // apply the rotation and velocity of the animation (the same as we used the default OnAnimatorUpdate method)
        
        if (_stateMachine.UseRootPosition && Time.deltaTime > 0f) {
            _stateMachine.NavAgent.velocity = _stateMachine.Animator.deltaPosition / Time.deltaTime;
        }

        if (_stateMachine.UseRootRotation) {
            _stateMachine.transform.rotation = _stateMachine.Animator.rootRotation;
        }
    }
    public virtual void OnAnimatorIKUpdate() {} // IK means Inverse Kinematics
    public virtual void OnTriggerEvent(AITriggerEvent eventType, Collider collider) {}
    public virtual void OnDestinationReach(bool isReached) {}
    
    // Public static methods
    /// <summary>
    /// Applies lossy scale to a sphere collider to convert it to world space.
    /// </summary>
    /// <param name="coll"></param>
    /// <param name="pos"></param>
    /// <param name="radius"></param>
    public static void ConvertSphereColliderToWorldSpace(SphereCollider coll, out Vector3 pos, out float radius) {
        // Default valuse
        pos = Vector3.zero;
        radius = 0f;
        
        // check for valid collider
        if (coll == null) return;
        
        // Calculate world position
        Vector3 lossyScale = coll.transform.lossyScale;
        Vector3 center = coll.center;
        
        pos = coll.transform.position;
        pos.x += center.x * lossyScale.x;
        pos.y += center.y * lossyScale.y;
        pos.z += center.z * lossyScale.z;
        
        // Calculate radius
        radius = Mathf.Max(coll.radius * lossyScale.x, coll.radius * lossyScale.y, coll.radius * lossyScale.z);
    }

    /// <summary>
    /// Returns a signed angle between two vectors.
    /// </summary>
    /// <param name="fromVector"></param>
    /// <param name="toVector"></param>
    /// <returns></returns>
    public static float GetSignedAngle(Vector3 fromVector, Vector3 toVector, bool ignoreY = true) {
        if (ignoreY) {
            fromVector.y = 0;
            toVector.y = 0;
        }
        
        float angle = Vector3.Angle(fromVector, toVector);
        Vector3 cross = Vector3.Cross(fromVector, toVector);
        angle *= Mathf.Sign(cross.y);

        return angle;
    }
    
    // Abstract methods
    
    /// <summary>
    /// Return the type of the state.
    /// </summary>
    /// <returns></returns>
    public abstract AIStateType Type();
    public abstract AIStateType OnUpdate();
}