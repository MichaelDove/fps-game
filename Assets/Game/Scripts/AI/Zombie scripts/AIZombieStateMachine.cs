﻿ using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
 using UnityEngine.Events;
 using Random = UnityEngine.Random;

 public enum BoneState {Animated, Ragdoll, RagdollToAnim}
public enum AIScreamPosition {Entity, Player}
 
 [Serializable]
 public class ZombieGenericEvent : UnityEvent<AIZombieStateMachine>{}

// class used for storing information about the bones of a zombie, so we can lerp them to the first frame of the reanimation animation
public class AIZombieStateMachine : AIStateMachine {
    // Inspector assigned
    [Header("General Settings")]
    [SerializeField] [Range(0, 100)] private int _health = 100;
    [SerializeField] [Range(0f, 360f)] private float _fieldOfView = 50.0f;
    [SerializeField] [Range(0f, 1f)] private float _sight = 0.5f;
    [SerializeField] [Range(0f, 1f)] private float _hearing = 1.0f;
    [SerializeField] [Range(0f, 1f)] private float _pinpointing = 0.5f;
    
    [Header("Feeding Settings")]
    [SerializeField] [Range(0f, 1f)] private float _satisfaction = 1f;
    [SerializeField] private float _replenishRate = 0.5f;
    [SerializeField] private float _depletionRate = 0.1f;
    
    [Header("Damage Settings")]
    [SerializeField] [Range(0, 100)] private int _lowerBodyDamage = 0;
    [SerializeField] [Range(0, 100)] private int _upperBodyDamage = 0;
    [SerializeField] [Range(0, 100)] private int _upperBodyTreshold = 40;
    [SerializeField] [Range(0, 100)] private int _limpTreshold = 40;
    [SerializeField] [Range(0, 100)] private int _crawlTreshold = 90;

    [Header("Target Path Prediction")] 
    [SerializeField] [Range(0, 10)] private int _predictionBase = 5;
    [SerializeField] [Range(0, 20)] private int _predictionIntelligenceScale = 5;
    
    [Header("Scream Settings")]
    [SerializeField] [Range(0f, 1f)] private float _screamChance = 0.5f;
    [SerializeField] [Range(0f, 50f)] private float _screamRadius = 10f;
    [SerializeField] private AIScreamPosition _screamPosition = AIScreamPosition.Entity;
    [SerializeField] private AISoundEmitter _screamPrefab = null;
    
    [Header("Reanimation Settings")]
    [SerializeField] private bool _reanimate = true;
    [SerializeField] private float _reanimationWaitTime = 3f;
    [SerializeField] private float _reanimationBlendTime = 0.5f;
    [SerializeField] private float _headFeetHeightError = 0.18f;
    [SerializeField] private LayerMask _geometryLayers = 0;

    [Header("Waypoint Settings")]
    [SerializeField] protected AIWaypointNetwork _waypointNetwork;
    [SerializeField] protected bool _randomPatrol = true;
    [SerializeField] protected int _currentWaypoint = 0;
    
    [Header("Events")]
    public ZombieGenericEvent OnTakenDamage = new ZombieGenericEvent();
    public ZombieGenericEvent OnRagdoll = new ZombieGenericEvent();
    public ZombieGenericEvent OnDeath = new ZombieGenericEvent();

    [Header("Item Collection")] 
    [SerializeField] private ItemCollection _itemCollection;

    [Header("Other")]
    [SerializeField] private AudioCollection _ragdollSounds;
    [SerializeField] private SharedInt _zombieCount;
    
    // Private 
    private int _seeking;
    private int _attackType;
    private bool _feeding;
    private bool _crawling = false;
    private float _speed;
    private float _speedOverride = -1f;
    private bool _isScreaming;
    private float _playerTrackingTimer;
    private int _dontReanimateOverride;
    private bool _forceRagdoll;
    
    // IK
    private Vector3 _currentLookAtPosition;
    private float _currentLookAtWeight;
    
    // Ragdoll:
    private BoneState _boneState = BoneState.Animated;
    private List<BodyPartSnapshot> _bodyPartSnapshots = new List<BodyPartSnapshot>();
    private float _reanimStartTime;
    private Vector3 _ragdollHipPos;
    private Vector3 _ragdollFeetPos;
    private Vector3 _ragdollHeadPos;
    private IEnumerator _reanimationCoroutine = null;
    private float _mecanimTransitionTime = 0.1f;
    private float _nextPainSound = 0.0f;

    // Public getters / setters
    public float FieldOfView => _fieldOfView;
    public float Sight => _sight;
    public float Hearing => _hearing;
    public float Pinpointing => _pinpointing;
    public bool  Crawling => _crawling;
    public float ReplenishRate => _replenishRate;
    public bool IsCrawling => _lowerBodyDamage >= _crawlTreshold;
    public float Satisfaction {
        get => _satisfaction;
        set => _satisfaction = value;
    }
    public int Health {
        get => _health;
        set => _health = value;
    }
    public int AttackType {
        get => _attackType;
        set => _attackType = value;
    }
    public bool Feeding {
        get => _feeding;
        set => _feeding = value;
    }
    public int Seeking {
        get => _seeking;
        set => _seeking = value;
    }
    public float Speed {
        get => _speed;
        set => _speed = value;
    }
    public float SpeedOverride {
        get => _speedOverride;
        set => _speedOverride = value;
    }
    public bool ForceRagdoll {
        get => _forceRagdoll;
        set => _forceRagdoll = value;
    }
    public bool IsScreaming => _isScreaming;
    public float ScreamChance => _screamChance;
    public bool IsTrackingPlayer => _playerTrackingTimer > 0f;
    public float CurrentLookAtWeight {
        get => _currentLookAtWeight;
        set => _currentLookAtWeight = value;
    }
    public Vector3 CurrentLookAtPosition {
        get => _currentLookAtPosition;
        set => _currentLookAtPosition = value;
    }
    public AIWaypointNetwork WaypointNetwork {
        get => _waypointNetwork;
        set => _waypointNetwork = value;
    }

    // hashes (for optimization)
    private int _speedHash = Animator.StringToHash("Speed");
    private int _feedingHash = Animator.StringToHash("Feeding");
    private int _seekingHash = Animator.StringToHash("Seeking");
    private int _attackHash = Animator.StringToHash("Attack");
    private int _crawlHash = Animator.StringToHash("Crawling");
    private int _hitHash = Animator.StringToHash("Hit");
    private int _hitTypeHash = Animator.StringToHash("HitType");
    private int _reanimBackHash = Animator.StringToHash("Reanimate Back");
    private int _reanimFrontHash = Animator.StringToHash("Reanimate Front");
    private int _reanimTypeHash = Animator.StringToHash("Reanimate type");
    private int _stateHash = Animator.StringToHash("State");
    private int _screamHash = Animator.StringToHash("Scream");
    private int _screamingHash = Animator.StringToHash("Screaming");
    private int _screamTypeHash = Animator.StringToHash("ScreamType");

    private int _damagedHash = Animator.StringToHash("Damaged");
    private int _upperBodyBrokenHash = Animator.StringToHash("Upper Body Broken");
    private int _lowerBodyBrokenHash = Animator.StringToHash("Lower Body Broken");

    // -------------------------------- PUBLIC METHODS -----------------------------------------------------------------

    public void StartPlayerTracking() {
        if (_playerTrackingTimer > 0f) return;

        _playerTrackingTimer = _predictionBase + _predictionIntelligenceScale * _pinpointing;
    }

    public void StopPlayerTracking() {
        _playerTrackingTimer = 0f;
    }

    public void DontReanimate(bool increment) {
        _dontReanimateOverride += increment ? 1 : -1;
    }
    
    // -------------------------------- START --------------------------------------------------------------------------

    protected override void Start() {
        base.Start();

        // store snapshots of the bone transforms into a list
        Transform[] transforms = _rootBone.GetComponentsInChildren<Transform>();
        foreach (var t in transforms) {
            BodyPartSnapshot bpss = new BodyPartSnapshot();
            bpss.transform = t;
            _bodyPartSnapshots.Add(bpss);
        }
        
        // add self to zombie count
        _zombieCount.Value++;

        UpdateAnimatorDamage();
    }
    
    // -------------------------------- UPDATE -------------------------------------------------------------------------

    protected override void Update() {
        base.Update();

        _playerTrackingTimer -= Time.deltaTime;

        if (_anim != null) {
            _anim.SetFloat(_speedHash, _speedOverride < 0f ? _speed : _speedOverride);
            _anim.SetBool(_feedingHash, _feeding);
            _anim.SetInteger(_seekingHash, _seeking);
            _anim.SetInteger(_attackHash, _attackType);
            _anim.SetInteger(_stateHash, (int)_currentStateType);
            
            // check if the zombie is screaming
            _isScreaming = _anim.GetFloat(_screamingHash) > 0.1f;
        }

        _satisfaction = Mathf.Max(0, _satisfaction - ((_depletionRate * Time.deltaTime) / 100f) * Mathf.Max(1,Mathf.Pow(_speed, 3f)));
        
        UpdateAnimatorDamage();
    }
    
    // -------------------------------- LATE UPDATE --------------------------------------------------------------------

    protected void LateUpdate() {
        if (_boneState == BoneState.RagdollToAnim) {
            if (Time.time <= _reanimStartTime + _mecanimTransitionTime) {
                Vector3 animatedToRagdoll = _ragdollHipPos - _rootBone.position;
                Vector3 newRootPos = transform.position + animatedToRagdoll;

                RaycastHit[] hits = Physics.RaycastAll(newRootPos + Vector3.up * 3f, Vector3.down, float.MaxValue, _geometryLayers);
                newRootPos.y = float.MinValue;
                foreach (var hit in hits) {
                    if (!hit.transform.IsChildOf(transform)) {
                        newRootPos.y = Mathf.Max(hit.point.y, newRootPos.y);
                    }
                }

                NavMeshHit navMeshHit;
                Vector3 baseOffset = Vector3.zero;
                if (_navAgent) baseOffset.y = _navAgent.baseOffset;
                // if the zombie is off the navigation mesh, snap back on to it
                if (NavMesh.SamplePosition(newRootPos + Vector3.up, out navMeshHit, 3.0f, NavMesh.AllAreas))
                    transform.position = navMeshHit.position + baseOffset;

                Vector3 ragdollDirection = _ragdollHeadPos - _ragdollFeetPos;
                ragdollDirection.y = 0.0f;

                Vector3 midFeetPos = 0.5f * (_anim.GetBoneTransform(HumanBodyBones.LeftFoot).position +
                                             _anim.GetBoneTransform(HumanBodyBones.RightFoot).position);
                Vector3 animatedDirection = _anim.GetBoneTransform(HumanBodyBones.Head).position - midFeetPos;
                animatedDirection.y = 0;
                
                // match the rotations of the ragdoll and the animator. Set y components to zero, because we only want to rotate around Y axis
                transform.rotation *= Quaternion.FromToRotation(animatedDirection.normalized, ragdollDirection.normalized);
            }
            
            // calculate interpolation value
            float blendAmount = Mathf.Clamp01((Time.time - _reanimStartTime - _mecanimTransitionTime) / _reanimationBlendTime);

            // calculate blended bone positions by interpolating ragdoll bone snapshots and animated bone positions
            foreach (var ss in _bodyPartSnapshots) {
                // whe also move the root bone
                if (ss.transform == _rootBone) {
                    ss.transform.position = Vector3.Lerp(ss.startPos, ss.transform.position, blendAmount);
                }
                ss.transform.rotation = Quaternion.Slerp(ss.startRot, ss.transform.rotation, blendAmount);
            }

            // exit reanimation code
            if (blendAmount == 1.0f) {
                _boneState = BoneState.Animated;
                if (_navAgent) _navAgent.isStopped = false;
                if (_collider) _collider.enabled = true;

                AIState newState = null;
                if (_states.TryGetValue(AIStateType.Alerted, out newState)) {
                    newState.OnStateEnter();
                    _currentState = newState;
                    _currentStateType = AIStateType.Alerted;
                }
            }
        }
    }

    // ------------------------------  ANIMATOR METHODS ----------------------------------------------------------------

    protected void UpdateAnimatorDamage() {
        _anim.SetBool(_damagedHash, _upperBodyDamage > _upperBodyTreshold || _lowerBodyDamage > _limpTreshold);
        _anim.SetBool(_crawlHash, IsCrawling);
        _anim.SetBool(_lowerBodyBrokenHash, _lowerBodyDamage >= 70);
        _anim.SetBool(_upperBodyBrokenHash, _upperBodyDamage >= 70);
        
    }
    
    // Screaming
    public bool Scream() {
        if (_isScreaming) return true;
        _anim.SetTrigger(_screamHash);
        // set a random scream animation
        int random = Random.Range(0, 2);
        _anim.SetInteger(_screamTypeHash, random);

        return true;
    }

    public void Scream_AnimatorCallback() {
        Vector3 spawnPos = _screamPosition == AIScreamPosition.Entity ? transform.position : Threat.position;
        AISoundEmitter screamEmitter = Instantiate(_screamPrefab, spawnPos, Quaternion.identity);
        
        if (screamEmitter != null)
            screamEmitter.SetRadius(_screamRadius);
    }

    // ------------------------------  AI WAYPOINT METHODS -------------------------------------------------------------
    
    /// <summary>
    /// Calculates the next waypoint and sets it as a target.
    /// </summary>
    public Vector3 SetWaypointTarget(bool increment, bool ignoreRepathTimer = false) {
        if (increment) {
            if (_randomPatrol) {
                int oldWaypoint = _currentWaypoint;
                // don't set the same waypoint
                while (oldWaypoint == _currentWaypoint) {
                    _currentWaypoint = Random.Range(0, _waypointNetwork.Waypoints.Count);
                }
            }
            else {
                int oldWaypoint = _currentWaypoint;
                _currentWaypoint++;
                if (_currentWaypoint >= _waypointNetwork.Waypoints.Count) {
                    _currentWaypoint = 0;
                    if (oldWaypoint == _currentWaypoint)
                        _currentWaypoint++;
                }
            }   
        }

        // set the waypoint as a target in the state machine
        Transform newWaypoint = _waypointNetwork.Waypoints[_currentWaypoint];
        if (newWaypoint != null) {
            float distance = Vector3.Distance(SensorPosition, newWaypoint.position);
            SetTarget(AITargetType.Waypoint, null, newWaypoint.position, distance, ignoreRepathTimer || increment);
            return newWaypoint.position;
        }

        return Vector3.zero;
    }

    /// <summary>
    /// Returns the position of the current waypoint.
    /// </summary>
    /// <returns></returns>
    public Vector3 GetWaypointPosition() {
        return _waypointNetwork.Waypoints[_currentWaypoint].position;
    }

    // ------------------------------  DAMAGE METHODS ------------------------------------------------------------------
    
    public override void TakeDamage(Vector3 position, Vector3 force, int damage, Rigidbody bodyPart, CharacterManager charManager, int hitDirection = 0) {
        base.TakeDamage(position, force, damage, bodyPart, charManager, hitDirection);
        
        // emit blood particles
        ParticleSystem sys = GameSceneManager.instance.BloodParticles;
        sys.transform.position = position;
        sys.Emit(100);
        
        // store health for further use
        float prevHealth = _health;

        bool reanimate = _dontReanimateOverride > 0 ? false : _reanimate;
        
        float hitStrength = force.magnitude;
        if (_boneState == BoneState.Ragdoll) {
            // play pain sound
            if (Time.time > _nextPainSound && _health > 0) {
                AudioClip clip = _ragdollSounds[0];
                _nextPainSound = Time.time + clip.length;
                AudioManager.instance.PlaySound(_ragdollSounds.AudioGroup, clip, position, _ragdollSounds.Volume, _ragdollSounds.SpatialBlend, _ragdollSounds.Priority);
            }
            
            // apply force to the body
            if (hitStrength > 1f)
                bodyPart.AddForce(force, ForceMode.Impulse);

            // Calculate the new health
            if (bodyPart.CompareTag("Head")) {
                _health = Mathf.Max(_health - damage, 0);
            } else if (bodyPart.CompareTag("Upper Body")) {
                _upperBodyDamage += damage;
                _health = Mathf.Max(_health - damage/2, 0);
            } else if (bodyPart.CompareTag("Lower Body")) {
                _lowerBodyDamage += damage;
                _health = Mathf.Max(_health - damage / 3, 0);
            }
                
            UpdateAnimatorDamage();
            
            OnTakenDamage.Invoke(this);
            if (!_reanimate)
                _health = 0;

            if (_reanimationCoroutine != null)
                StopCoroutine(_reanimationCoroutine);
            
            if (_health > 0) {
                _reanimationCoroutine = Reanimate();
                StartCoroutine(_reanimationCoroutine);
            }
            else {
                if (prevHealth > 0) {
                    OnDeath.Invoke(this);
                    this.enabled = false;
                    OnDeath.Invoke(this);
                    _zombieCount.Value--;
                    if (_itemCollection)
                        _itemCollection.gameObject.SetActive(true);
                }
            }

            return;
        }

        // get local space position of the attacker
        Vector3 attackerLocalPos = transform.InverseTransformPoint(charManager.transform.position);
        // get local space position of the hit
        Vector3 hitLocalPos = transform.InverseTransformPoint(position);

        // cases when we always ragdoll: 
        bool shouldRagdoll = _boneState != BoneState.Animated || _forceRagdoll || IsCrawling || attackerLocalPos.z < 0 || _health <= 0;
        
        // Calculate the new health
        if (bodyPart.CompareTag("Head")) {
            _health = Mathf.Max(_health - damage, 0);
        } else if (bodyPart.CompareTag("Upper Body")) {
            _upperBodyDamage += damage;
            _health = Mathf.Max(_health - damage/2, 0);
            UpdateAnimatorDamage();
        } else if (bodyPart.CompareTag("Lower Body")) {
            _lowerBodyDamage += damage;
            _health = Mathf.Max(_health - damage / 3, 0);
            shouldRagdoll = true;
            UpdateAnimatorDamage();
        }
        
        OnTakenDamage.Invoke(this);

        if (!shouldRagdoll) {
            float angle = 0f;
            if (hitDirection == 0) {
                Vector3 vecToHit = (position - transform.position).normalized;
                angle = AIState.GetSignedAngle(vecToHit, transform.forward);
            }

            int hitType = 0;
            if (bodyPart.CompareTag("Head")) {
                if (angle < -10 || hitDirection == -1)
                    hitType = 1;
                else if (angle > 10 || hitDirection == 1)
                    hitType = 3;
                else
                    hitType = 2;
            }
            else if (bodyPart.CompareTag("Upper Body")) {
                if (angle < -20 || hitDirection == -1)
                    hitType = 4;
                else if (angle > 20 || hitDirection == 1)
                    hitType = 6;
                else
                    hitType = 5;
            }
            _anim.SetTrigger(_hitHash);
            _anim.SetInteger(_hitTypeHash, hitType);
        }
        else {
            if (_currentState) {
                _currentState.OnStateExit();
                _currentState = null;
                _currentStateType = AIStateType.None;
            }

            Vector3 newTargetPos = transform.position + (transform.forward * Mathf.Sign(attackerLocalPos.z) * 3.0f);
            SetTarget(AITargetType.Audio, null, newTargetPos, 3.0f, true);
            
            _navAgent.isStopped = true;
            _anim.enabled = false;
            _collider.enabled = false;
            
            // mute audio when ragdolled
            _layeredAudioSource.Mute(true);
            
            // play pain sound
            if (Time.time > _nextPainSound && prevHealth > 0) {
                AudioClip clip = _ragdollSounds[0];
                _nextPainSound = Time.time + clip.length;
                AudioManager.instance.PlaySound(_ragdollSounds.AudioGroup, clip, position, _ragdollSounds.Volume, _ragdollSounds.SpatialBlend, _ragdollSounds.Priority);
            }

            foreach (var body in _bodyParts) {
                body.isKinematic = false;
            }
            
            if (hitStrength > 1f)
                bodyPart.AddForce(force, ForceMode.Impulse);

            _boneState = BoneState.Ragdoll;
            OnRagdoll.Invoke(this);
            if (!reanimate)
                _health = 0;
        }

        if (_reanimationCoroutine != null)
            StopCoroutine(_reanimationCoroutine);
        
        if (_health > 0) {
            _reanimationCoroutine = Reanimate();
            StartCoroutine(_reanimationCoroutine);
        }
        else {
            if (prevHealth > 0) {
                OnDeath.Invoke(this);
                this.enabled = false;
                StartCoroutine(DestroyZombie());
                _zombieCount.Value--;
                if (_itemCollection != null)
                    _itemCollection.gameObject.SetActive(true);
            }
        }
    }

    protected IEnumerator DestroyZombie() {
        yield return new WaitForSeconds(30);
        Destroy(gameObject);
    }
    
    // start the reanimation procedure
    protected IEnumerator Reanimate() {
        // only reanimate if we are in a ragdoll state
        if (_boneState != BoneState.Ragdoll || _anim == null)
            yield break;

        // first wait for some number of seconds:
        yield return new WaitForSeconds(_reanimationWaitTime);
        
        // record time at the start of the reanimation process
        _reanimStartTime = Time.time;
        // set all body parts to kinematic so the physics system doesn't mess with them
        foreach (var b in _bodyParts) {
            b.isKinematic = true;
        }
        
        // set that state to the reanimation state
        _boneState = BoneState.RagdollToAnim;
        // record the starting positions and rotations of the body parts
        foreach (var bpss in _bodyPartSnapshots) {
            bpss.startPos = bpss.transform.position;
            bpss.startRot = bpss.transform.rotation;
            bpss.startLocRot = bpss.transform.localRotation;
        }

        _ragdollHeadPos = _anim.GetBoneTransform(HumanBodyBones.Head).position;
        // get the position exactly between the two feet
        _ragdollFeetPos = (_anim.GetBoneTransform(HumanBodyBones.LeftFoot).position + _anim.GetBoneTransform(HumanBodyBones.RightFoot).position) * 0.5f;
        _ragdollHipPos = _rootBone.position;

        float error = Mathf.Abs(_ragdollFeetPos.y - _ragdollHeadPos.y);

        if (error < _headFeetHeightError) {
            // enable animator
            _anim.enabled = true;

            float forwardTest;
            switch (_rootBoneAlignment) {
                case AIBoneAlignment.front:
                    forwardTest = _rootBone.forward.y;
                    break;
                case AIBoneAlignment.back:
                    forwardTest = -_rootBone.forward.y;
                    break;
                case AIBoneAlignment.up:
                    forwardTest = _rootBone.up.y;
                    break;
                case AIBoneAlignment.down:
                    forwardTest = -_rootBone.up.y;
                    break;
                case AIBoneAlignment.right:
                    forwardTest = _rootBone.right.y;
                    break;
                case AIBoneAlignment.left:
                    forwardTest = -_rootBone.right.y;
                    break;
                default:
                    forwardTest = _rootBone.forward.y;
                    break;
            }
            
            if (forwardTest >= 0f)
                _anim.SetTrigger(_reanimBackHash);
            else
                _anim.SetTrigger(_reanimFrontHash);
            _anim.SetInteger(_reanimTypeHash, Random.Range(0,2));
        }
        else {
            _health = 0;
            _navAgent.enabled = false;
            this.enabled = false;
            OnDeath.Invoke(this);
            _zombieCount.Value--;
            _itemCollection.gameObject.SetActive(true);
        }
    }
}

public class BodyPartSnapshot {
    public Transform transform;
    public Vector3 startPos;
    public Quaternion startRot;
    public Quaternion startLocRot;
}