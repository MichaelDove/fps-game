﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieState_Feeding1 : AIZombieState {
    // Inspector assigned
    [SerializeField] private float _slerpSpeed = 5f;
    [SerializeField] private Transform _particleMount;
    
    // Private variables
    private int _feedingStateHash = Animator.StringToHash("feeding");
    private int _crawlingFeedingStateHash = Animator.StringToHash("crawl_Feeding");
    private float _timer;
    
    // return the type of the state
    public override AIStateType Type() {
        return AIStateType.Feeding;
    }

    public override void OnStateEnter() {
        base.OnStateEnter();

        // configure state machine's animator
        _zombieStateMachine.Feeding = true;
        _zombieStateMachine.Seeking = 0;
        _zombieStateMachine.Speed = 0;
        _zombieStateMachine.AttackType = 0;
        
        // agent updates position but not rotation
        _zombieStateMachine.NavAgentControl(true, false);

        _timer = 0f;
    }

    public override void OnStateExit() {
        base.OnStateExit();
        
        // set feeding boolean to false
        _zombieStateMachine.Feeding = false;
    }

    public override AIStateType OnUpdate() {
        _timer += Time.deltaTime;
        
        // carry on patrolling if he is not hungry anymore 
        if (_zombieStateMachine.Satisfaction >= 0.9f) {
            _zombieStateMachine.SetWaypointTarget(false);
            return AIStateType.Alerted;
        }

        if (!_zombieStateMachine.DestinationReached) {
            _zombieStateMachine.ClearTarget();
            return AIStateType.Alerted;
        }

        if (_zombieStateMachine.Threat.type > AITargetType.Food) {
            _zombieStateMachine.SetTarget(_zombieStateMachine.Threat);
            return AIStateType.Alerted;
        }

        if (_zombieStateMachine.RepairEmptyPath()) {
            _zombieStateMachine.Speed = 0;
            return AIStateType.Feeding;
        }

        var distanceToTarget = Vector3.Distance(_zombieStateMachine.SensorPosition, _zombieStateMachine.TargetCollider.transform.position);
        _zombieStateMachine.SetTarget(_zombieStateMachine.TargetType, 
                                    _zombieStateMachine.TargetCollider, 
                                    _zombieStateMachine.TargetCollider.transform.position, 
                                    distanceToTarget);

        // if the zombie is feeding, increase it's satisfaction level
        int currentAnim = _zombieStateMachine.Animator.GetCurrentAnimatorStateInfo(0).shortNameHash;
        if (currentAnim == _feedingStateHash || currentAnim == _crawlingFeedingStateHash) {
            float newSatis = _zombieStateMachine.Satisfaction + (Time.deltaTime * _zombieStateMachine.ReplenishRate)/100f;
            _zombieStateMachine.Satisfaction = Mathf.Min(newSatis, 1f);

            if (_timer >= 0.5f) {
                ParticleSystem particles = GameSceneManager.instance.BloodParticles;
                particles.transform.position = _particleMount.position;
                particles.transform.rotation = _particleMount.rotation;
                particles.Emit(20);

                _timer = 0f;
            }
        }

        if (!_zombieStateMachine.UseRootRotation) {
            // keep the zombie facing the feeding object
            Vector3 targetPos = _zombieStateMachine.TargetPosition;
            targetPos.y = _zombieStateMachine.transform.position.y;
            Quaternion newRot = Quaternion.LookRotation(targetPos - _zombieStateMachine.transform.position);
            _zombieStateMachine.transform.rotation = Quaternion.Slerp(_zombieStateMachine.transform.rotation, newRot, _slerpSpeed * Time.deltaTime);   
        }

        Vector3 headToTarget = _zombieStateMachine.TargetPosition - _zombieStateMachine.Animator.GetBoneTransform(HumanBodyBones.Head).position;
        headToTarget.y = 0f;
        if (Vector3.SqrMagnitude(headToTarget) > 0.01f)
            _zombieStateMachine.transform.position = Vector3.Lerp(_zombieStateMachine.transform.position, _zombieStateMachine.transform.position + headToTarget, Time.deltaTime);

        // stay in the feeding state
        return AIStateType.Feeding;
    }
}
