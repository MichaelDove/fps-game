﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class ZombieState_Attack1 : AIZombieState {
    // Inspector assigned
    [SerializeField] [Range(0, 10)] private float _speed = 0f;
    [SerializeField] private float _stoppingDistance = 1f;
    [SerializeField] private float _slerpSpeed = 5f;
    [SerializeField] private float _slideSpeed = 1f;
    
    public override AIStateType Type() {
        return AIStateType.Attack;
    }

    public override void OnStateEnter() {
        base.OnStateEnter();
        
        // Configure state machine
        _zombieStateMachine.NavAgentControl(true, false);
        _zombieStateMachine.Seeking = 0;
        _zombieStateMachine.Feeding = false;
        _zombieStateMachine.AttackType = Random.Range(1, 100);
        _zombieStateMachine.Speed = _speed;
    }

    public override void OnStateExit() {
        base.OnStateExit();
        _zombieStateMachine.AttackType = 0;
    }

    public override AIStateType OnUpdate() {
        Vector3 targetPos;
        Quaternion newRot;

        if (Vector3.Distance(_zombieStateMachine.transform.position, _zombieStateMachine.TargetPosition) < _stoppingDistance)
            _zombieStateMachine.Speed = 0;
        else
            _zombieStateMachine.Speed = _speed;
        

        // do we see the player
        if (_zombieStateMachine.Threat.type == AITargetType.Player) {
            // set new target
            _zombieStateMachine.SetTarget(_zombieStateMachine.Threat);

            if (!_slideSpeed.Equals(0f)) {
                _zombieStateMachine.NavAgent.nextPosition = Vector3.Lerp(transform.position, _zombieStateMachine.Threat.position, _slideSpeed * Time.deltaTime);
            }
            
            // if the player is not close enough, go chase him
            if (!_zombieStateMachine.InMeleeRange) 
                return AIStateType.Pursuit;

            //if (!_zombieStateMachine.UseRootRotation) {
                // keep the zombie looking at the player
                targetPos = _zombieStateMachine.TargetPosition;
                targetPos.y = _zombieStateMachine.transform.position.y; // we are doing a 2d rotation
                newRot = Quaternion.LookRotation(targetPos - _zombieStateMachine.transform.position);
                _zombieStateMachine.transform.rotation = Quaternion.Slerp(_zombieStateMachine.transform.rotation, newRot, Time.deltaTime * _slerpSpeed);
            //}
            
            _zombieStateMachine.AttackType = Random.Range(1, 100);

            return AIStateType.Attack;
        }
        
        // we have lost sight of the player, so just face in the direction we last saw him
        targetPos = _zombieStateMachine.TargetPosition;
        targetPos.y = _zombieStateMachine.transform.position.y;
        newRot = Quaternion.LookRotation(targetPos - _zombieStateMachine.transform.position);
        _zombieStateMachine.transform.rotation = Quaternion.Slerp(_zombieStateMachine.transform.rotation, newRot, Time.deltaTime * _slerpSpeed);

        // Look around for the player
        return AIStateType.Alerted;
    }
}
