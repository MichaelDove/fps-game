﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieState_Pursuit1 : AIZombieState {
    // Inspector assigned
    [SerializeField] [Range(0f,3f)] private float _speed = 3f;
    [SerializeField] private float _slerpSpeed = 5f;
    [SerializeField] private float _maxStateDuration = 40f;
    [SerializeField] private float _pathPendindStopTime = 0.5f;
    [SerializeField] [Range(0, 360)] private float _turnOnSpotTreshold = 90f;
    
    // Private variables
    private float _timer;
    private float _pathPendingTimer;
    
    public override AIStateType Type() {
        return AIStateType.Pursuit;
    }
    
    // Default handlers
    public override void OnStateEnter() {
        base.OnStateEnter();
        
        // configure state machine
        _zombieStateMachine.NavAgentControl(true, false);
        _zombieStateMachine.Seeking = 0;
        _zombieStateMachine.Feeding = false;
        _zombieStateMachine.AttackType = 0;
        
        // zombies will only pursue for so long:
        _timer = 0f;
        _pathPendingTimer = 0f;

        // Set path
        if (_zombieStateMachine.NavAgent.enabled) {
            _zombieStateMachine.NavAgent.SetDestination(_zombieStateMachine.TargetPosition);
            _zombieStateMachine.NavAgent.isStopped = false;   
        }
    }

    public override AIStateType OnUpdate() {
        _timer += Time.deltaTime;

        // give up the pursuit if it is taking too long
        if (_timer > _maxStateDuration) {
            _zombieStateMachine.SetWaypointTarget(false);
            return AIStateType.Patrol;
        }
        
        // if the threat is the highest priority
        if (_zombieStateMachine.Threat.type >= _zombieStateMachine.TargetType 
            || (_zombieStateMachine.Threat.time > _zombieStateMachine.TargetTime && _zombieStateMachine.Threat.type != AITargetType.Food)) {
            _zombieStateMachine.SetTarget(_zombieStateMachine.Threat);
        }

        // if we are chasing the player, and we are close enough, then attack
        if (_stateMachine.TargetType == AITargetType.Player && _zombieStateMachine.InMeleeRange)
            return AIStateType.Attack;
    
        // if we have reached the source:
        if (_zombieStateMachine.DestinationReached) {
            switch (_zombieStateMachine.TargetType) {
                case AITargetType.Audio:
                case AITargetType.Light:
                    _zombieStateMachine.SetWaypointTarget(false);
                    return AIStateType.Alerted;
                case AITargetType.Food:
                    return AIStateType.Feeding;
            }
        }

        if (_zombieStateMachine.NavAgent.pathPending) {
            _pathPendingTimer += Time.deltaTime;
            _zombieStateMachine.Speed = _pathPendingTimer >= _pathPendindStopTime ? 0f : _speed;
            return AIStateType.Pursuit;
        }

        if (_zombieStateMachine.InvalidPath) {
            _zombieStateMachine.SetWaypointTarget(true);
            return AIStateType.Alerted;
        }

        if (_zombieStateMachine.RepairEmptyPath()) {
            _zombieStateMachine.Speed = 0;
            return AIStateType.Pursuit;
        }
        
        _zombieStateMachine.Speed = _speed;
        _pathPendingTimer = 0f;
        
        // if we have player in sight and if he is close enough to be in the trigger, then keep facing towards him
        if (_zombieStateMachine.TargetType == AITargetType.Player && _zombieStateMachine.Threat.type == AITargetType.Player && _zombieStateMachine.DestinationReached) {
            Vector3 targetPos = _zombieStateMachine.TargetPosition;
            targetPos.y = _zombieStateMachine.transform.position.y;
            Quaternion newRot = Quaternion.LookRotation(targetPos - _zombieStateMachine.transform.position);
            _zombieStateMachine.transform.rotation = newRot;
        }
        // slowly rotate the rotation to match the nav agents desired rotation
        else if (!_zombieStateMachine.DestinationReached) {
            if (_zombieStateMachine.TargetType != AITargetType.Player || _zombieStateMachine.TargetTime + 0.25f < Time.time) {
                float angle = GetSignedAngle(_zombieStateMachine.transform.forward,
                    _zombieStateMachine.NavAgent.steeringTarget - _zombieStateMachine.transform.position);
                if (Mathf.Abs(angle) > _turnOnSpotTreshold)
                    return AIStateType.Alerted;
            }
        
            // generate a new quaternion
            Quaternion newRot = Quaternion.LookRotation(_zombieStateMachine.DesiredLookDirection);
            // rotate to the new rotation over time
            _zombieStateMachine.transform.rotation = Quaternion.Slerp(_zombieStateMachine.transform.rotation, newRot, Time.deltaTime * _slerpSpeed);
        }
        else if (_zombieStateMachine.DestinationReached) {
            _zombieStateMachine.ClearTarget();
            return AIStateType.Alerted;
        }
        
        return AIStateType.Pursuit;
    }
}
