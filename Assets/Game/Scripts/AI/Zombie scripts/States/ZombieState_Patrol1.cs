﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class ZombieState_Patrol1 : AIZombieState {
    // Inspector assigned
    [SerializeField] [Range(0,360)] private float _turnOnSpotTreshold = 80f;
    [SerializeField] private float _slerpSpeed = 5f;
    [SerializeField] [Range(0f, 3f)] private float _speed = 1;
    [SerializeField] private float _pathPendingStopTime = 0.5f;

    [Header("Head IK Settings")] 
    [SerializeField] [Range(0f, 1f)] private float _randomHeadLookChance = 0.75f;
    [SerializeField] [Range(0f, 10f)] private float _headDirectionChangeTime = 3f;
    [SerializeField] private Transform _randomLookTargeter;

    // Internals
    private float _pathPendingTimer;
    private float _nextIKTimer = 0f;
    private bool _randomLookDirectionInUse = false;
    
    public override AIStateType Type() {
        return AIStateType.Patrol;
    }
    
    // called when we first get into the patrol state
    public override void OnStateEnter() {
        base.OnStateEnter();

        _zombieStateMachine.NavAgentControl(true, false);
        _zombieStateMachine.Seeking = 0;
        _zombieStateMachine.Feeding = false;
        _zombieStateMachine.AttackType = 0;

        _pathPendingTimer = 0f;
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.magenta;
        if (_zombieStateMachine != null)
            Gizmos.DrawSphere(_zombieStateMachine.NavAgent.destination, 0.5f);
    }

    public override AIStateType OnUpdate() {
        // if the threat is the highest priority
        if (_zombieStateMachine.Threat.type >= _zombieStateMachine.TargetType 
            || (_zombieStateMachine.Threat.time > _zombieStateMachine.TargetTime && _zombieStateMachine.Threat.type != AITargetType.Food)) {
            _zombieStateMachine.SetTarget(_zombieStateMachine.Threat, true);
            return AIStateType.Alerted;
        }

        _zombieStateMachine.SetWaypointTarget(false);
        // if path is being computed, wait
        if (_zombieStateMachine.NavAgent.pathPending) {
            _pathPendingTimer += Time.deltaTime;
            _zombieStateMachine.Speed = _pathPendingTimer >= _pathPendingStopTime ? 0f : _speed;
            return AIStateType.Patrol;
        }

        if (_zombieStateMachine.InvalidPath) {
            _zombieStateMachine.SetWaypointTarget(true);
            return AIStateType.Patrol;
        }

        if (_zombieStateMachine.RepairEmptyPath()) {
            _zombieStateMachine.Speed = 0;
            return AIStateType.Patrol;
        }

        _zombieStateMachine.Speed = _speed;
        _pathPendingTimer = 0f;
        
        // Process the patrol state: 
        float angle = GetSignedAngle(_zombieStateMachine.transform.forward,
            _zombieStateMachine.NavAgent.steeringTarget - _zombieStateMachine.transform.position);
        
        // if the angle is too large, we need to do a turn on the spot animation
        if (Mathf.Abs(angle) > _turnOnSpotTreshold && !_zombieStateMachine.UseRootRotation) {
            return AIStateType.Alerted;
        }
        
        // if we aren't using root rotation, we need to apply rotation ourselves
        if (!_zombieStateMachine.UseRootRotation) {
            if (_zombieStateMachine.NavAgent.desiredVelocity != Vector3.zero) {
                // create new quaternion
                Quaternion newRot = Quaternion.LookRotation(_zombieStateMachine.DesiredLookDirection);
                // gradually rotate
                _zombieStateMachine.transform.rotation = Quaternion.Slerp(_zombieStateMachine.transform.rotation, newRot, Time.deltaTime * _slerpSpeed);   
            }
        }

        return AIStateType.Patrol;
    }

    /// <summary>
    /// Method is called when the zombie enters the target trigger.
    /// </summary>
    /// <param name="isReached"></param>
    public override void OnDestinationReach(bool isReached) {
        if (!isReached) return;

        // if the target is still a waypoint, set a new waypoint
        if (_zombieStateMachine.TargetType == AITargetType.Waypoint) 
            _zombieStateMachine.SetWaypointTarget(true);
    }

    public override void OnAnimatorIKUpdate() {
        _nextIKTimer += Time.deltaTime;

        if (_nextIKTimer > _headDirectionChangeTime) {
            _nextIKTimer = 0f;
            _randomLookDirectionInUse = false;

            if (Random.value < _randomHeadLookChance) {
                _randomLookDirectionInUse = true;
                var randomPos = _zombieStateMachine.transform.position + Random.onUnitSphere + _zombieStateMachine.transform.forward;
                randomPos.y = _zombieStateMachine.Animator.GetBoneTransform(HumanBodyBones.Head).transform.position.y;

                _randomLookTargeter.position = randomPos;
            }
        }

        if (!_randomLookTargeter) {
            _zombieStateMachine.CurrentLookAtWeight = Mathf.Lerp(_zombieStateMachine.CurrentLookAtWeight, 0, Time.deltaTime);
            _zombieStateMachine.Animator.SetLookAtPosition(_zombieStateMachine.CurrentLookAtPosition);
            _zombieStateMachine.Animator.SetLookAtWeight(_zombieStateMachine.CurrentLookAtWeight);
        }
        else {
            var newLookAtTarget = Vector3.Lerp(_zombieStateMachine.CurrentLookAtPosition, _randomLookTargeter.position, Time.deltaTime);
            var angle = Vector3.Angle(_zombieStateMachine.transform.forward, newLookAtTarget - _zombieStateMachine.transform.position);
            var weight = Mathf.Clamp01(Mathf.InverseLerp(0f, 120f, angle));
            
            _zombieStateMachine.CurrentLookAtWeight = Mathf.Lerp(_zombieStateMachine.CurrentLookAtWeight, 1 - weight, Time.deltaTime);
            _zombieStateMachine.CurrentLookAtPosition = newLookAtTarget;
            
            _zombieStateMachine.Animator.SetLookAtPosition(_zombieStateMachine.CurrentLookAtPosition);
            _zombieStateMachine.Animator.SetLookAtWeight(_zombieStateMachine.CurrentLookAtWeight);
        }
    }
}
