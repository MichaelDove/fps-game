﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieState_Alerted1 : AIZombieState {
    // Assigned by the inspector
    [SerializeField] [Range(1, 60)] private float _maxDuration = 10f;
    [SerializeField] [Range(0, 360)] private float _angleTreshold = 35f;
    [SerializeField] [Range(0, 10)] private float _directionChangeTime = 1.5f;
    [SerializeField] private float _slerpSpeed = 1.0f;
    
    // Private variables
    private float _timer;
    private float _screamChance;
    private float _directionChangeTimer;
    private float _nextScream = 0f;
    private float _screamFrequency = 100f;
    
    // called when we first get into the alerted state
    public override void OnStateEnter() {
        base.OnStateEnter();

        _zombieStateMachine.NavAgentControl(true, false);
        _zombieStateMachine.Speed = 0f;
        _zombieStateMachine.Seeking = 0;
        _zombieStateMachine.Feeding = false;
        _zombieStateMachine.AttackType = 0;
        
        _timer = _maxDuration;
        _directionChangeTimer = 0f;
        // scream chance will be positive if we want to scream or negative if we don't want to scream
        _screamChance = _zombieStateMachine.ScreamChance - Random.value;

        if (_zombieStateMachine.TargetType == AITargetType.None || _zombieStateMachine.InvalidPath)
            _zombieStateMachine.SetWaypointTarget(true);
    }
    
    public override AIStateType Type() {
        return AIStateType.Alerted;
    }

    public override AIStateType OnUpdate() {
        // reduce timer
        _timer -= Time.deltaTime;
        _directionChangeTimer += Time.deltaTime;
        // transition back to the patrol state
        if (_timer <= 0) {
            _zombieStateMachine.SetWaypointTarget(false);
            _timer = _maxDuration;
        }

        if (_zombieStateMachine.Threat.type == AITargetType.Player) {
            if (_screamChance > 0f && Time.time >= _nextScream) {
                if (_zombieStateMachine.Scream()) {
                    _screamChance = float.MinValue;
                    _nextScream = Time.time + _screamFrequency;
                    return AIStateType.Alerted;
                }
            }
        }
        
        // if the threat is the highest priority
        if (_zombieStateMachine.Threat.type >= _zombieStateMachine.TargetType 
            || (_zombieStateMachine.Threat.time > _zombieStateMachine.TargetTime && _zombieStateMachine.Threat.type != AITargetType.Food)) {
            _zombieStateMachine.SetTarget(_zombieStateMachine.Threat);
            _timer = _maxDuration;
        }

        if (_zombieStateMachine.TargetType == AITargetType.Waypoint)
            _zombieStateMachine.SetWaypointTarget(false);

        if (_zombieStateMachine.NavAgent.pathPending)
            return AIStateType.Alerted;

        if (_zombieStateMachine.InvalidPath) {
            _zombieStateMachine.SetWaypointTarget(true);
            return AIStateType.Alerted;
        }
        
        if (_zombieStateMachine.RepairEmptyPath())
            return AIStateType.Alerted;

        float angle;
        if (!_zombieStateMachine.DestinationReached) {
            angle = GetSignedAngle(_zombieStateMachine.transform.forward,
                                    _zombieStateMachine.NavAgent.steeringTarget - _zombieStateMachine.transform.position);

            if (Mathf.Abs(angle) <= _angleTreshold) {
                if (_zombieStateMachine.TargetType == AITargetType.Waypoint)
                    return AIStateType.Patrol;
                if (_zombieStateMachine.TargetType != AITargetType.None)
                    return AIStateType.Pursuit;
            }

            if (_directionChangeTimer > _directionChangeTime) {
                if (Random.value < _zombieStateMachine.Pinpointing || _zombieStateMachine.TargetType == AITargetType.Waypoint) 
                    _zombieStateMachine.Seeking = (int)Mathf.Sign(angle);
                else 
                    _zombieStateMachine.Seeking = (int) Mathf.Sign(Random.Range(-1f, 1f));
                

                _directionChangeTimer = 0f;
            }
        }
        else {
            if (_directionChangeTimer > _directionChangeTime) {
                _zombieStateMachine.Seeking = (int) Mathf.Sign(Random.Range(-1f, 1f));
                _directionChangeTimer = 0f;
            }
        }

        if (!_zombieStateMachine.UseRootRotation) {
            _zombieStateMachine.transform.Rotate(new Vector3(0.0f, _slerpSpeed * Time.deltaTime * _zombieStateMachine.Seeking, 0.0f));
            return AIStateType.Patrol;
        }

        return AIStateType.Alerted;
    }
}
