﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieState_Idle1 : AIZombieState {
    // Inspector assigned
    [SerializeField] Vector2 _idleTimeRange = new Vector2(10f, 60f);
    
    // Private
    private float _idleTime = 0f;
    private float _timer = 0f;
    
    public override AIStateType Type() {
        return AIStateType.Idle;
    }
    
    // called when we first get into the idle state
    public override void OnStateEnter() {
        base.OnStateEnter();

        _idleTime = Random.Range(_idleTimeRange.x, _idleTimeRange.y);
        _timer = 0f;

        _zombieStateMachine.NavAgentControl(true, false);
        _zombieStateMachine.Speed = 0f;
        _zombieStateMachine.Seeking = 0;
        _zombieStateMachine.Feeding = false;
        _zombieStateMachine.AttackType = 0;
        
        _zombieStateMachine.ClearTarget();
    }

    // called every frame
    public override AIStateType OnUpdate() {
        // if the threat is the highest priority
        if (_zombieStateMachine.Threat.type >= _zombieStateMachine.TargetType 
            || (_zombieStateMachine.Threat.time > _zombieStateMachine.TargetTime 
                && _zombieStateMachine.Threat.type != AITargetType.Food)) {
            _zombieStateMachine.SetTarget(_zombieStateMachine.Threat);
            return AIStateType.Alerted;
        }

        _zombieStateMachine.SetWaypointTarget(false);

        if (_zombieStateMachine.RepairEmptyPath())
            return AIStateType.Idle;
        
        // if not stay in idle for _idleTime and then go to patrol state
        _timer += Time.deltaTime;
        if (_timer > _idleTime) {
            _zombieStateMachine.SetWaypointTarget(false,true);
            return AIStateType.Alerted;
        }
        
        return AIStateType.Idle;
    }
}
