﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.PlayerLoop;
using UnityEngine.Tilemaps;

public abstract class AIZombieState : AIState {
    // Private
    protected int _playerLayerMask = -1;
    protected int _visualLayerMask = -1;
    protected int _bodyPartLayer = -1;
    protected AIZombieStateMachine _zombieStateMachine = null;
    
    private void Awake() {
        _playerLayerMask = LayerMask.GetMask("Player", "AI Body Part", "Default");
        _visualLayerMask = LayerMask.GetMask("Player", "AI Body Part", "Visual Threat", "Default");
        _bodyPartLayer = LayerMask.NameToLayer("AI Body Part");
    }
    
    // Check if the given state machine is a zombie state machine and assign it
    public override void SetStateMachine(AIStateMachine stateMachine) {
        if (stateMachine.GetType() == typeof(AIZombieStateMachine)) {
            base.SetStateMachine(stateMachine);
            _zombieStateMachine = (AIZombieStateMachine) stateMachine;
        }
    }
    
    /// <summary>
    /// Deals with the object that just entered the sensor.
    /// </summary>
    /// <param name="eventType"></param>
    /// <param name="collider"></param>
    public override void OnTriggerEvent(AITriggerEvent eventType, Collider collider) {
        if (_stateMachine == null)
            return;
        
        // we are only interested in the objects that are still in the sensor radius
        if (eventType != AITriggerEvent.Exit) {
            AITargetType currType = _stateMachine.Threat.type;
            
            // check if the player entered the sensor:
            if (collider.CompareTag("Player")) {
                float distance = Vector3.Distance(_stateMachine.SensorPosition, collider.transform.position);
                
                // change the target to the player if he is visible or closer than previously registered
                if (currType != AITargetType.Player || (currType == AITargetType.Player && distance < _stateMachine.Threat.distance)) {
                    RaycastHit hitInfo;
                    
                    if (_zombieStateMachine.IsTrackingPlayer || ColliderIsVisible(collider, out hitInfo, _playerLayerMask)) {
                        _zombieStateMachine.StartPlayerTracking();
                        _stateMachine.Threat.Set(AITargetType.Player, collider, collider.transform.position, distance);
                    }
                }
            }
            else if (collider.CompareTag("Flashlight") && currType != AITargetType.Player) { // The player has the highest priority
                BoxCollider flashlightTrigger = (BoxCollider) collider;
                if (flashlightTrigger == null) return;
                
                float distance = Vector3.Distance(_zombieStateMachine.SensorPosition, collider.transform.position);
                float zSize = flashlightTrigger.size.z * flashlightTrigger.transform.lossyScale.z;
                float aggrFactor = distance / zSize;
                
                if (aggrFactor < _zombieStateMachine.Sight && aggrFactor < _zombieStateMachine.Pinpointing) {
                    _zombieStateMachine.Threat.Set(AITargetType.Light, collider, collider.transform.position, distance);
                }
            }
            else if (collider.CompareTag("AI Sound Emitter") && currType != AITargetType.Player && currType != AITargetType.Player) {
                SphereCollider soundTrigger = (SphereCollider) collider;
                if (soundTrigger == null) return;

                Vector3 soundPos;
                float soundRadius;
                AIState.ConvertSphereColliderToWorldSpace(soundTrigger, out soundPos, out soundRadius);

                // how far in the sounds radius are we
                float distance = Vector3.Distance(soundPos, _zombieStateMachine.SensorPosition);
                // calculate the distance factor that is 1 when the sound radius is touching the center of zombie
                float distanceFactor = distance / soundRadius;
                // bias the factor based on the hearing ability of the zombie
                distanceFactor += distanceFactor * (1f - _zombieStateMachine.Hearing);

                // too far away
                if (distanceFactor > 1f) return;
                
                // if we hear it and it is closer than any other audio threat
                if (distance < _zombieStateMachine.Threat.distance) {
                    // set it as the new audio threat
                    _zombieStateMachine.Threat.Set(AITargetType.Audio, collider, soundPos, distance);
                }
            }
            else if (collider.CompareTag("AI Food") && currType != AITargetType.Player && currType != AITargetType.Light && currType != AITargetType.Audio) { // food has the lowest priority
                float distance = Vector3.Distance(_zombieStateMachine.SensorPosition, collider.transform.position);

                bool hungryEnough = 1f - _zombieStateMachine.Satisfaction > distance / _zombieStateMachine.SensorRadius;

                // if this is the closest food
                if (hungryEnough && distance < _zombieStateMachine.Threat.distance) {
                    RaycastHit hit;
                    // if we see the food
                    if (ColliderIsVisible(collider, out hit, _visualLayerMask)) {
                        // go towards the food
                        _zombieStateMachine.Threat.Set(AITargetType.Food, collider, collider.transform.position, distance);
                    }
                }
            }
        }
    }
    
    /// <summary>
    /// Returns true if the zombie has the collider in the line of sight.
    /// </summary>
    /// <param name="collider"></param>
    /// <param name="hitInfo"></param>
    /// <param name="layerMask"></param>
    /// <returns></returns>
    protected virtual bool ColliderIsVisible(Collider collider, out RaycastHit hitInfo, int layerMask) {
        hitInfo = new RaycastHit();

        // check if the state machine is a zombie state machine
        if (_zombieStateMachine == null) return false;

        Vector3 head = _zombieStateMachine.SensorPosition;
        Vector3 direction = collider.transform.position - head; // get the direction of the threat
        float angle = Vector3.Angle(direction, transform.forward); // calculate the angle from the forward vector


        if (angle > _zombieStateMachine.FieldOfView * 0.5f) {
            return false;
        }
        
        RaycastHit[] hits = Physics.RaycastAll(head, direction.normalized, _zombieStateMachine.SensorRadius * _zombieStateMachine.Sight, layerMask);

        float minDistance = float.MaxValue;
        Collider closest = null;
        
        // find the closest hit from the array of hits
        for (int i = 0; i < hits.Length; i++) {
            RaycastHit hit = hits[i];
            
            // check if it is the closest so far
            if (hit.distance < minDistance) {
                // check if it is a zombie part
                if (hit.transform.gameObject.layer == _bodyPartLayer) {
                    // check if the hit is not a part of this zombie
                    if (_stateMachine != GameSceneManager.instance.GetAIStateMachine(hit.rigidbody.GetInstanceID())) {
                        minDistance = hit.distance;
                        closest = hit.collider;
                        hitInfo = hit;
                    }
                }
                else {
                    // we don't need to check anything if the hit is not a body part
                    minDistance = hit.distance;
                    closest = hit.collider;
                    hitInfo = hit;
                }
            }
        }
        
        // if the closest game object we hit was the collider then we have a line of sight
        if (closest != null && closest.gameObject == collider.gameObject) {
            return true;
        }

        return false;
    }

    public override void OnAnimatorIKUpdate() {
        if (_zombieStateMachine.TargetType != AITargetType.None) {
            var noYLook = new Vector3(_zombieStateMachine.TargetPosition.x,
                _zombieStateMachine.Animator.GetBoneTransform(HumanBodyBones.Head).position.y,
                _zombieStateMachine.TargetPosition.z);

            var newLookAtTarget = Vector3.Lerp(_zombieStateMachine.CurrentLookAtPosition, noYLook, Time.deltaTime);
            var angle = Vector3.Angle(_zombieStateMachine.transform.forward, newLookAtTarget - _zombieStateMachine.transform.position);
            var weight = Mathf.Clamp01(Mathf.InverseLerp(0f, 120f, angle));
           
            _zombieStateMachine.CurrentLookAtWeight = Mathf.Lerp(_zombieStateMachine.CurrentLookAtWeight, 1 - weight, Time.deltaTime);
            _zombieStateMachine.CurrentLookAtPosition = newLookAtTarget;
        }
        else {
            _zombieStateMachine.CurrentLookAtWeight = Mathf.Lerp(_zombieStateMachine.CurrentLookAtWeight, 0, Time.deltaTime);
        }
        
        _zombieStateMachine.Animator.SetLookAtPosition(_zombieStateMachine.CurrentLookAtPosition); 
        _zombieStateMachine.Animator.SetLookAtWeight(_zombieStateMachine.CurrentLookAtWeight);
    }
}
