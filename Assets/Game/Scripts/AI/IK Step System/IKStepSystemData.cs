﻿using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum IKStepSystemType {Simple, Animated, AnimatedArcs}
public enum IKStepSystemDirection {Up,Down}

[CreateAssetMenu(menuName = "Scriptable Objects/AI/IKStepSystemData", fileName = "New IK Step System Data" )]
public class IKStepSystemData : ScriptableObject {
    [Header("General Settings")] 
    [SerializeField] private float _footBoneFloorOffset = 0.175f;
    [SerializeField] private float _raycastOriginHeight = 0.75f;
    [SerializeField] private float _raycastLength = 2f;
    [SerializeField] private float _IKCooldownSpeed = 0.2f;

    [Header("Down Animation Settings")] 
    [SerializeField] private IKStepSystemType _stepDownType = IKStepSystemType.Simple;
    [SerializeField] private float _stepDownPelvisHeight = 1f;
    [SerializeField] private float _stepDownPelvisAdjustmentSpeed = 3f;
    [SerializeField] private float _stepDownIKWeight = 1f;
    [SerializeField] private float _stepDownStrideLength = 0.18f;
    [SerializeField] private float _stepDownStrideWidth = 0.1f;
    [SerializeField] private float _stepDownPelvisOffset = 0f;
    [SerializeField] private float _stepDownRayOffset = 0f;
    [SerializeField] private float _stepDownSpeedScale = 0.7f;
    [SerializeField] private float _stepDownFeetSmoothing = 5f;
    [SerializeField] private float _stepDownArcScale = 0.15f;

    [Header("Up Animation Settings")] 
    [SerializeField] private IKStepSystemType _stepUpType = IKStepSystemType.Simple;
    [SerializeField] private float _stepUpPelvisHeight = 1f;
    [SerializeField] private float _stepUpPelvisAdjustmentSpeed = 3f;
    [SerializeField] private float _stepUpIKWeight = 1f;
    [SerializeField] private float _stepUpStrideLength = 0.18f;
    [SerializeField] private float _stepUpStrideWidth = 0.1f;
    [SerializeField] private float _stepUpPelvisOffset = 0f;
    [SerializeField] private float _stepUpRayOffset = 0f;
    [SerializeField] private float _stepUpSpeedScale = 0.7f;
    [SerializeField] private float _stepUpFeetSmoothing = 5f;
    [SerializeField] private float _stepUpArcScale = 0.15f;
    
    // General Settings Getters
    public float FootBoneFloorOffset => _footBoneFloorOffset;
    public float RaycastOriginHeight => _raycastOriginHeight;
    public float RaycastLength => _raycastLength;
    public float IkCooldownSpeed => _IKCooldownSpeed;
    
    // Up / Down Animation Settings Getters
    public IKStepSystemType GetIKType(IKStepSystemDirection dir) {
        if (dir == IKStepSystemDirection.Down)
            return _stepDownType;
        return _stepUpType;
    }

    public void SetIKType(IKStepSystemDirection dir, IKStepSystemType type) {
        if (dir == IKStepSystemDirection.Down)
            _stepDownType = type;
        else
            _stepUpType = type;
    }
    
    public float GetPelvisHeight (IKStepSystemDirection dir) {
        if (dir == IKStepSystemDirection.Down)
            return _stepDownPelvisHeight;
        return _stepUpPelvisHeight;
    }
    
    public float GetPelvisAdjustmentSpeed (IKStepSystemDirection dir) {
        if (dir == IKStepSystemDirection.Down)
            return _stepDownPelvisAdjustmentSpeed;
        return _stepUpPelvisAdjustmentSpeed;
    }
    
    public float GetIKweight (IKStepSystemDirection dir) {
        if (dir == IKStepSystemDirection.Down)
            return _stepDownIKWeight;
        return _stepUpIKWeight;
    }
    
    public float GetStrideLength (IKStepSystemDirection dir) {
        if (dir == IKStepSystemDirection.Down)
            return _stepDownStrideLength;
        return _stepUpStrideLength;
    }
    
    public float GetStrideWidth (IKStepSystemDirection dir) {
        if (dir == IKStepSystemDirection.Down)
            return _stepDownStrideWidth;
        return _stepUpStrideWidth;
    }
}
