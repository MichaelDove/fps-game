﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIDamage : MonoBehaviour {
    // Inspector assigned
    [SerializeField] private string _parameter;
    [SerializeField] private float _damageAmount = 0.1f;
    [SerializeField] private bool _doPainSound = true;

    // Private variables
    private AIStateMachine _machine;
    private Animator _anim;
    private int _parameterHash;

    private void Start() {
        _machine = transform.root.GetComponentInChildren<AIStateMachine>();
        _anim = gameObject.GetComponentInParent<Animator>();
        // turn string to hash for more efficiency
        _parameterHash = Animator.StringToHash(_parameter);
    }

    private void OnTriggerStay(Collider other) {
        if (other.gameObject.CompareTag("Player") && _anim.GetFloat(_parameterHash) > 0.9f) {
            ParticleSystem system = GameSceneManager.instance.BloodParticles;
            // temporary:
            system.transform.position = transform.position;
            system.transform.rotation = Camera.main.transform.rotation;
            system.Emit(10);

            PlayerInfo info = GameSceneManager.instance.GetPlayerInfo(other.GetInstanceID());
            info.CharacterManager.TakeDamage(_damageAmount, _doPainSound);
        }
    }
}
