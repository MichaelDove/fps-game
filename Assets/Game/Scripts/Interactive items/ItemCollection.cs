﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemCollectionMask {
    Weapons = 1,
    Ammo = 2,
    Backpack = 4,
    Recordings = 8,
    All = 15
}

public class ItemCollection : InteractiveItem, ISerializationCallbackReceiver {
    // Inspector assigned
    public List<WeaponMountInfo> Weapons = new List<WeaponMountInfo>();
    public List<AmmoMountInfo> Ammo = new List<AmmoMountInfo>();
    public List<BackpackMountInfo> Backpack = new List<BackpackMountInfo>();
    public List<InventoryItemAudio> Recordings = new List<InventoryItemAudio>();

    public string Owner = "";

    public string ExploreText = null;

    public bool WeaponMountsExpanded = false;
    public bool AmmoMountsExpanded = false;
    public bool BackpackMountsExpanded = false;
    public bool AudioLogsMountsExpanded = false;

    // item collection prefab
    public ItemCollectionUI UIPrefab;
    
    public override string GetText(CharacterManager characterManager = null) {
        return ExploreText;
    }

    public override void Use(CharacterManager characterManager) {
        ItemCollectionUI ui = Instantiate(UIPrefab);
        
        ui.Show(this, characterManager);
    }

    public void OnBeforeSerialize() {}

    public void OnAfterDeserialize() {
        foreach (var mount in Backpack) {
            if (mount.Item && mount.Item.GetContextDataType() != null) {
                mount.ContextData = JsonUtility.FromJson(mount.ContextDataString, mount.Item.GetContextDataType());
            }
        }
    }

    public void GetMounts(List<InventoryMountInfo> result, ItemCollectionMask mask = ItemCollectionMask.All) {
        if (result == null) return;
        
        result.Clear();
        if ((mask & ItemCollectionMask.Weapons) != 0) {
            foreach (var m in Weapons)
                result.Add(m);
        }
        
        if ((mask & ItemCollectionMask.Ammo) != 0) {
            foreach (var a in Ammo)
                result.Add(a);
        }
        
        if ((mask & ItemCollectionMask.Backpack) != 0) {
            foreach (var b in Backpack)
                result.Add(b);
        }
        
        if ((mask & ItemCollectionMask.Weapons) != 0) {
            foreach (var r in Recordings) {
                AudioMountInfo info = new AudioMountInfo();
                info.Audio = r;
                result.Add(info);
            }
        }
    }

    public void ClearMount(InventoryMountInfo info) {
        for (int i = 0; i < Weapons.Count; i++)
            if (Weapons[i] == info) {
                Weapons[i] = new WeaponMountInfo();
                return;
            }
        
        for (int i = 0; i < Ammo.Count; i++)
            if (Ammo[i] == info) {
                Ammo[i] = new AmmoMountInfo();
                return;
            }
        
        for (int i = 0; i < Backpack.Count; i++)
            if (Backpack[i] == info) {
                Backpack[i] = new BackpackMountInfo();
                return;
            }
        
        for (int i = 0; i < Recordings.Count; i++)
            if ((info as AudioMountInfo).Audio == Recordings[i]) {
                Recordings[i] = null;
                return;
            }
    }
}
