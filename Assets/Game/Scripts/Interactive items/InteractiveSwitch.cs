﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

public enum AnimatorParameterType {Trigger, Bool, Int, Float, String}

[System.Serializable]
public class AnimatorParameter {
    public AnimatorParameterType Type = AnimatorParameterType.Bool;
    public string Name;
    public string Value;
}

[System.Serializable]
public class AnimatorConfigurator {
    public Animator Animator;
    public List<AnimatorParameter> AnimatorParameters = new List<AnimatorParameter>();
}

public class InteractiveSwitch : InteractiveItem {
    [Header("Game state management")] 
    [SerializeField] protected List<GameState> _requiredStates = new List<GameState>();
    [SerializeField] protected List<GameState> _activateStates = new List<GameState>();
    [SerializeField] protected List<GameState> _deactivateStates = new List<GameState>();
    
    [Header("Inventory Dependancies")]
    [SerializeField] protected List<InventoryItem> _requiredItems = new List<InventoryItem>();

    [Header("Message")] 
    [TextArea(3, 10)] 
    [SerializeField] protected string _notReadyText;
    [TextArea(3, 10)] 
    [SerializeField] protected string _readyText;
    [TextArea(3, 10)] 
    [SerializeField] protected string _activeText;
    [TextArea(3, 10)] 
    [SerializeField] protected string _requiredItemsText;

    [Header("Activation Parameters")] 
    [SerializeField] protected float _activationDelay = 1.0f;
    [SerializeField] protected float _deactivationDelay = 1.0f;
    [SerializeField] protected AudioCollection _activationSounds;
    [SerializeField] protected AudioSource _audioSource;

    [Header("Startup")]
    [SerializeField] protected bool _startActivated = false;
    [SerializeField] protected bool _canToggle = false;
    
    [Header("Configurable entities")]
    // animators that need to be affected by the switch 
    [SerializeField] protected List<AnimatorConfigurator> _animations = new List<AnimatorConfigurator>();
    // materials that have their emissive properties affected
    [SerializeField] protected List<MaterialController> _materialControllers = new List<MaterialController>();
    // game objects to activate/deactivate
    [SerializeField] protected List<GameObject> _activatingObjects = new List<GameObject>();
    [SerializeField] protected List<GameObject> _deactivatingObjects = new List<GameObject>();
    
    // Private variables
    protected IEnumerator _coroutine = null;
    protected bool _activated = false;
    protected bool _used = false;

    protected override void Start() {
        base.Start();
        
        // activate material controller
        foreach(var mat in _materialControllers)
            mat.OnStart();
        
        // turn on/off all objects that we will deactivate/activate
        foreach(var go in _activatingObjects)
            go.SetActive(false);

        foreach (var go in _deactivatingObjects)
            go.SetActive(true);
        
        if (_startActivated)
            Use(null);
    }
    
    public override string GetText(CharacterManager characterManager = null) {
        if (_activated) {
            return _activeText;
        } 
        
        if (!ApplicationManager.instance.AreStatesSet(_requiredStates)) {
            return _notReadyText;
        }
        
        if (!CheckRequiredItems(characterManager.Inventory)) {
            return _requiredItemsText;
        }

        return _readyText;
    }

    public override void Use(CharacterManager characterManager) {
        ApplicationManager am = ApplicationManager.instance;

        if (_used && !_canToggle) return;

        if (!_activated) {
            bool requiredStates = ApplicationManager.instance.AreStatesSet(_requiredStates);
            bool requiredItems = characterManager == null || CheckRequiredItems(characterManager.Inventory);

            if (!requiredItems || !requiredStates) return;
        }

        // switch the object state
        _activated = !_activated;
        _used = true;

        if (_coroutine != null) StopCoroutine(_coroutine);
        _coroutine = DelayedActivation(characterManager);
        StartCoroutine(_coroutine);
    }
    
    protected virtual IEnumerator DelayedActivation(CharacterManager characterManager) {
        foreach (var conf in _animations) {
            foreach (var param in conf.AnimatorParameters) {
                // TODO: support other types
                switch (param.Type) {
                    case AnimatorParameterType.Bool:
                        bool boolean = bool.Parse(param.Value);
                        conf.Animator.SetBool(param.Name, _activated ? boolean : !boolean);
                        break;
                    case AnimatorParameterType.Trigger:
                        conf.Animator.SetTrigger(param.Name);
                        break;
                }
            }
        }

        // play the sound effect
        AudioClip clip = _activationSounds[_activated ? 0 : 1];
        _audioSource.clip = clip;
        _audioSource.volume = _activationSounds.Volume;
        _audioSource.spatialBlend = _activationSounds.SpatialBlend;
        _audioSource.priority = _activationSounds.Priority;
        _audioSource.outputAudioMixerGroup = AudioManager.instance.GetAudioGroupFromTrackName(_activationSounds.AudioGroup);
        _audioSource.Play();
        
        yield return new WaitForSeconds(_activated ? _activationDelay : _deactivationDelay);
        
        SetActivationStates();

        foreach (var go in _activatingObjects) {
            go.SetActive(_activated);
        }

        foreach (var go in _deactivatingObjects) {
            go.SetActive(!_activated);
        }

        foreach (var mc in _materialControllers) {
            mc.Activate(_activated);
        }

        if (_activated)
            ProcessInventoryAfterActivation(characterManager);
        else
            ProcessInventoryAfterDeactivation(characterManager);
    }

    protected void SetActivationStates() {
        ApplicationManager appMan = ApplicationManager.instance;

        if (_activated) {
            foreach (GameState gs in _activateStates) {
                appMan.SetGameState(gs.Key, gs.Value);
            }
        }
        else {
            foreach (GameState gs in _deactivateStates) {
                appMan.SetGameState(gs.Key, gs.Value);
            }
        }
    }

    protected bool CheckRequiredItems(Inventory inventory) {

        foreach (var item in _requiredItems) {
            if (inventory.Search(item) == null)
                return false;
        }

        return true;
    }
    
    protected virtual void ProcessInventoryAfterActivation(CharacterManager characterManager = null) {}
    protected virtual void ProcessInventoryAfterDeactivation(CharacterManager characterManager = null) {}
}
