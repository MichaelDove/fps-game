﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractiveSound : InteractiveItem {
    // Inspector assigned
    [TextArea(3, 10)] [SerializeField] private string _infoText;
    [TextArea(3, 10)] [SerializeField] private string _usedText;
    [SerializeField] private float _usedTextDuration = 3.0f;

    [SerializeField] private AudioCollection _audioCollection;
    [SerializeField] private int _bank = 0;
    
    // private variables
    private IEnumerator _coroutine;
    private float _hideUsedText = 0.0f;

    public override string GetText(CharacterManager characterManager = null) {
        if (_coroutine != null || Time.time < _hideUsedText)
            return _usedText;
        return _infoText;
    }

    public override void Use(CharacterManager characterManager) {
        if (_coroutine == null) {
            _hideUsedText = Time.time + _usedTextDuration;
            _coroutine = DoActivation();
            StartCoroutine(_coroutine);
        }
    }

    private IEnumerator DoActivation() {
        // fetch the clip
        AudioClip clip = _audioCollection[_bank];
        
        // play one shot sound
        AudioManager.instance.PlaySound(_audioCollection.AudioGroup, clip, transform.position, _audioCollection.Volume,
            _audioCollection.SpatialBlend, _audioCollection.Priority);
        
        // Run while clip is playing
        yield return new WaitForSeconds(clip.length);
        
        // unset the coroutine
        _coroutine = null;
    }
}
