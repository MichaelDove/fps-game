﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class HighlightedObjectInfo {
    public GameObject Object;
    public int Layer = 0;
}

public class InteractiveItem : MonoBehaviour {
    // Inspector assigned
    [SerializeField] protected int _priority = 0;

    [Header("Highlight settings")]
    [SerializeField] protected List<GameObject> _highlightObjects = new List<GameObject>();
    [SerializeField] protected Color _highlightColor = Color.yellow;
    [SerializeField] [Range(0.1f, 15f)] protected float _highlightBlurSize = 1.5f;
    [SerializeField] [Range(1, 12)] protected int _highlightBlurIterations = 2;

    [HideInInspector] [SerializeField] List<HighlightedObjectInfo> _highlightedObjectInfos = new List<HighlightedObjectInfo>();
    
    // public members
    public int Priority => _priority;

    // Private/Protected variables
    protected GameSceneManager _gameSceneManager;
    protected Collider _collider;
    
    // Properties
    public List<HighlightedObjectInfo> HighlightObjects => _highlightedObjectInfos;
    public Color HighlightColor {
        get => _highlightColor;
        set => _highlightColor = value;
    }
    public float HighlightBlurSize {
        get => _highlightBlurSize;
        set => _highlightBlurSize = value;
    }
    public int HighlightBlurIterations {
        get => _highlightBlurIterations;
        set => _highlightBlurIterations = value;
    }

    // Methods
    public virtual string GetText(CharacterManager characterManager = null) {return null;}
    public virtual void Use(CharacterManager characterManager) {}

    protected virtual void Start() {
        _gameSceneManager = GameSceneManager.instance;
        _collider = GetComponent<Collider>();
        _gameSceneManager.RegisterInteractiveItem(_collider.GetInstanceID(), this);
    }

    protected void OnValidate() {
        _highlightedObjectInfos.Clear();
        foreach (var go in _highlightObjects) {
            if (go == null) continue;
            var info = new HighlightedObjectInfo();
            info.Object = go;
            info.Layer = go.layer;
            _highlightedObjectInfos.Add(info);
        }
    }
}
