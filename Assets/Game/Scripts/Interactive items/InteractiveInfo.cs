﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveInfo : InteractiveItem {
    [TextArea(3,10)]
    [SerializeField] private string _infoText;

    public override string GetText(CharacterManager characterManager = null) {
        return _infoText;
    }
}
