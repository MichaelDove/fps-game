﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableAmmo : CollectableItem {
    // Inspector assigned
    [Header("Ammo preferences")] 
    [SerializeField] [Range(0, 100)] protected int _capacity;
    [SerializeField] [Range(0, 100)] protected int _ammo;
    
    // Public properties
    public int Capacity => _capacity;
    public int Ammo {
        get => _ammo;
        set => _ammo = value;
    }

    public override string GetText(CharacterManager characterManager = null) {
        if (_interactiveText == null)
            _interactiveText = _inventoryItem.InventoryName + " " + _ammo + "/" + _capacity + "\n" + _inventoryItem.PickupText; 
            
        return _interactiveText;
    }
}
