﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableAudio : CollectableItem {
    // Inspector assigned
    [SerializeField] private Renderer _screenRenderer;
    [SerializeField] private Color _emissiveColor = Color.white;
    
    // Internal 
    private InventoryItemAudio _audioItem;

    protected override void Start() {
        base.Start();
        
        _audioItem = _inventoryItem as InventoryItemAudio;
        if (_screenRenderer) {
            _screenRenderer.material.SetTexture("_EmissionMap", _audioItem.Image);
            _screenRenderer.material.SetColor("_EmissionColor", _emissiveColor);
        }
    }

    public override string GetText(CharacterManager characterManager = null) {
        if (_interactiveText == null) {
            if (!_audioItem)
                _interactiveText = "Empty audio log";
            else
                _interactiveText = "Audio log by " + _audioItem.Person + "\n" + _audioItem.PickupText;
        }

        return _interactiveText;
    }

    public override void Use(CharacterManager characterManager) {
        if (!_audioItem) return;
        
        if (_inventory.AddItem(this)) {
            _interactiveText = null;
            _inventoryItem = null;
            _screenRenderer.material.SetTexture("_EmissionMap", null);
            _screenRenderer.material.SetColor("_EmissionColor", Color.black);
            _audioItem = null;
        }
    }
}
