﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableWeapon : CollectableItem {
    // Inspector assigned
    [Header("Weapon preferences")] 
    [SerializeField] [Range(0,100)] protected int _ammo;
    
    // Public properties
    public int Ammo {
        get => _ammo;
        set => _ammo = value;
    }
    
    public override string GetText(CharacterManager characterManager = null) {
        var weapon = _inventoryItem as InventoryItemWeapon;

        if (_interactiveText == null) {
            if (weapon.AttackType == InventoryWeaponAttackType.Ammunition)
                _interactiveText = _inventoryItem.InventoryName + " - ammo: " + _ammo + "\n" + _inventoryItem.PickupText; 
            else
                _interactiveText = _inventoryItem.InventoryName + "\n" + _inventoryItem.PickupText; 
        }
            
        return _interactiveText;
    }
}
