﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableBattery : CollectableItem {
    // Inspector assigned
    [SerializeField] protected float _charge = 100f;

    public float Charge {
        get => _charge;
        set => _charge = value;
    }

    public override string GetText(CharacterManager characterManager = null) {
        if (_interactiveText == null) {
            _interactiveText = _inventoryItem.name + "\n Charge: " + Mathf.FloorToInt(_charge) + "% \n" + _inventoryItem.PickupText;
        }

        return _interactiveText;
    }
}
