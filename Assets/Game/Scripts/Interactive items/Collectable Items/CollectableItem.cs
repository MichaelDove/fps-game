﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableItem : InteractiveItem {
    // Inspector assigned
    [SerializeField] protected Inventory _inventory;
    
    [Header("Collectable Item properties")]
    [SerializeField] protected InventoryItem _inventoryItem;

    // Public properties
    public Inventory Inventory {
        get => _inventory;
        set => _inventory = value;
    }

    public InventoryItem InventoryItem{
        get => _inventoryItem;
        set => _inventoryItem = value;
    }

    // Internals
    protected string _interactiveText = null;
    
    // Public methods
    public override string GetText(CharacterManager characterManager = null) {
        if (_interactiveText == null)
            _interactiveText = _inventoryItem.InventoryName + "\n" + _inventoryItem.PickupText;
        return _interactiveText;
    }

    public override void Use(CharacterManager characterManager) {
        if (_inventory.AddItem(this)) {
            Destroy(gameObject);
            // drag object into the 'hands' of the player
            // StartCoroutine(PickupObject(characterManager));
        }
    }

    private IEnumerator PickupObject(CharacterManager player) {
        player.TurnOnRay();
        // disable gravity and collision
        var rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
        var col = GetComponent<BoxCollider>();
        col.isTrigger = true;
        
        float dist = Vector3.Distance(transform.position, player.PickUpTarget);

        // turn on the light
        while (dist > 0.1f) {
            var towards = Vector3.MoveTowards(transform.position, player.PickUpTarget, Time.deltaTime * 2);
            transform.position = towards;
            dist = Vector3.Distance(transform.position, player.PickUpTarget);
            
            yield return null;
        }
        
        player.TurnOfRay();
        Destroy(gameObject);
    }
}
