﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableVaccine : CollectableItem {
   public override void Use(CharacterManager characterManager) {
      base.Use(characterManager);
      ApplicationManager.instance.SetGameState("Vaccine", "true");
   }
}