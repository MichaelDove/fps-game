﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComputerTrigger : MonoBehaviour {
    // Inspector assigned
    [SerializeField] private float _downloadTime = 10.0f;
    [SerializeField] private Slider _downloadSlider;
    [SerializeField] private Text _hintText;
    [SerializeField] private MaterialController _materialController;
    [SerializeField] private GameObject _lockedLight;
    [SerializeField] private GameObject _unlockedLight;
    
    // Private variables
    private ApplicationManager _appManager;
    private GameSceneManager _gsManager;
    private bool _inTrigger = false;
    private float _downloadProgress = 0.0f;
    private AudioSource _audio;
    private bool _downloadComplete = false;
    private string _startingText;

    // called automatically when the object is enabled
    private void OnEnable() {
        _appManager = ApplicationManager.instance;
        _audio = GetComponent<AudioSource>();

        _downloadProgress = 0.0f;
        _materialController.OnStart();
        _startingText = _hintText.text;

         string lockdown = _appManager.GetGameState("Lockdown");
        if (lockdown.Equals("true") || String.IsNullOrEmpty(lockdown)) {
            _materialController.Activate(false);
            _unlockedLight.SetActive(false);
            _lockedLight.SetActive(true);
            _downloadComplete = false;
        } else if (lockdown.Equals("false")) {
            _materialController.Activate(true);
            _unlockedLight.SetActive(true);
            _lockedLight.SetActive(false);
            _downloadComplete = true;
        }

        ResetSoundAndUI();
        _hintText.text = _startingText; 
    }

    private void Update() {
        if (_downloadComplete) return;

        if (_inTrigger) {
            if (Input.GetButton("Use")) {
                if (_audio && !_audio.isPlaying)
                    _audio.Play();

                _downloadProgress = Mathf.Clamp(_downloadProgress + Time.deltaTime, 0f, _downloadTime);

                if (_downloadProgress < _downloadTime) {
                    _downloadSlider.gameObject.SetActive(true);
                    _downloadSlider.value = _downloadProgress / _downloadTime;
                    return;
                }
                else {
                    _downloadComplete = true;
                    _hintText.text = "Deactivation successful";
                    _appManager.SetGameState("Lockdown", "false");
                    
                    ResetSoundAndUI();
                    _materialController.Activate(true);
                    _unlockedLight.SetActive(true);
                    _lockedLight.SetActive(false);
                    return;
                }
            }
        }

        _downloadProgress = 0f;
        ResetSoundAndUI();
    }

    private void ResetSoundAndUI() {
        _audio.Stop();
        
        _downloadSlider.gameObject.SetActive(false);
        _downloadSlider.value = 0.0f;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            _inTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Player")) {
            _inTrigger = false;
        }
    }
}
