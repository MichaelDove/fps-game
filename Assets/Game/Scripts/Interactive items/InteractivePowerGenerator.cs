﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractivePowerGenerator : InteractiveSwitch {
    // Inspector assigned
    [Header("Generator settings")] [SerializeField] protected float _flickerDuration = 4f;
    
    // Internals
    private IEnumerator _coroutine = null;
    private Light[] _lights;
    private float[] _lightIntensities;

    protected override void Start() {
        base.Start();

        if (_activatingObjects.Count > 0) {
            _lights = _activatingObjects[0].GetComponentsInChildren<Light>();

            if (_lights.Length > 0) {
                _lightIntensities = new float[_lights.Length];
                for (int i = 0; i < _lights.Length; i++) {
                    _lightIntensities[i] = _lights[i].intensity;
                }
            }
        }
    }

    protected override void ProcessInventoryAfterActivation(CharacterManager characterManager = null) {
        if (_coroutine != null)
            StopCoroutine(_coroutine);

        characterManager.Inventory.Remove(_requiredItems[0]);
        
        _coroutine = FlickerLights();
        StartCoroutine(_coroutine);
    }

    protected override void ProcessInventoryAfterDeactivation(CharacterManager characterManager = null) {
        if (_coroutine != null)
            StopCoroutine(_coroutine);
        
        _coroutine = FlickerLights();
        StartCoroutine(_coroutine);
    }
    
    protected IEnumerator FlickerLights() {
        if (_flickerDuration <= 0) {
            _coroutine = null;
            yield break;
        }

        float timer = 0;

        MaterialController matController = null;
        if (_materialControllers.Count > 0)
            matController = _materialControllers[0];

        Material mat = null;
        Color matColor = Color.black;
        float matScale = 0f;

        mat = matController.Material;
        matColor = matController.EmissiveColor;
        matScale = matController.EmissionScale;

        if (!_activated) {
            _activatingObjects[0].SetActive(true);
        }

        while (timer < _flickerDuration) {
            timer += Time.deltaTime;
            float fac = _activated ? timer / _flickerDuration : 1 - timer/_flickerDuration;
            float randomIntensity = Mathf.PerlinNoise(Time.time * 5f,0);

            float scale = Mathf.Clamp(randomIntensity * matScale, matScale * fac, matScale * fac * 1.5f);
            mat.SetColor("_EmissionColor", matColor * scale);

            for (var i = 0; i < _lights.Length; i++) {
                float lightScale = _lightIntensities[i];
                float newIntensity = Mathf.Clamp(randomIntensity * lightScale, lightScale * fac, lightScale * fac * 1.5f);
                _lights[i].intensity = newIntensity;
            }

            yield return null;
        }

        mat.SetColor("_EmissionColor", _activated ? matColor * matScale : Color.black);
        for (var i = 0; i < _lights.Length; i++) {
            _lights[i].intensity = _activated ? _lightIntensities[i] : 0f;
        }
        
        if (!_activated) {
            _activatingObjects[0].SetActive(false);
        }

        _coroutine = null;
    }
}
