﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;

[System.Serializable]
public class DoorInfo {
    // Transform to animate
    public Transform Transform;
    // Local rotation 
    public Vector3 Rotation = Vector3.zero;
    // Local movement
    public Vector3 Movement = Vector3.zero;
    // Collider
    public Collider Collider;
    // IK Targets
    public Transform FrontSideHandle;
    public Transform BackSideHandle;
    
    // variables used to cache values
    [HideInInspector] public Quaternion ClosedRotation = Quaternion.identity;
    [HideInInspector] public Quaternion OpenRotation = Quaternion.identity;
    [HideInInspector] public Vector3 ClosedPosition = Vector3.zero;
    [HideInInspector] public Vector3 OpenPosition = Vector3.zero;
    [HideInInspector] public NavMeshObstacle NavObstacle;
}

public enum AxisAlignment {xaxis, yaxis, zaxis}
public enum AIInteractiveDoorType {AICanOpen, AICantOpen, CarvedWhenClosed}

[Serializable]
public class InteractiveDoorStateChanged : UnityEvent<bool>{}
[Serializable]
public class InteractiveDoorPlayerInteraction : UnityEvent<CharacterManager, bool, bool>{}

[RequireComponent(typeof(BoxCollider))]
public class InteractiveDoor : InteractiveItem {
    // Inspector assigned
    [Header("Activation Properties")]
    [Tooltip("Does the door open in both directions")] 
    [SerializeField] private bool _isTwoWay = true;

    [Tooltip("Does the door squeak or do we only hear the opening and closing")]
    [SerializeField] private bool _isSqueaky;

    [Tooltip("Does the door open automatically when the player walks into its trigger")] 
    [SerializeField] private bool _autoOpen = false;
    
    [Tooltip("Does the door close automatically after a random amount of time")] 
    [SerializeField] private bool _autoClose = false;
    
    [Tooltip("The random time range to close the door")] 
    [SerializeField] private Vector2 _autoCloseDelay = new Vector2(2f, 7f);

    [Tooltip("Disable Manual Activation")] 
    [SerializeField] private bool _disableActivation = false;
    
    [Tooltip("How should the size of the box collider grow when the door is open")] 
    [SerializeField] private float _colliderLengthScale = 3.0f;
    
    [Tooltip("Should we offset the collider when the door are open")] 
    [SerializeField] private bool _offsetCollider = false;
    // axis setup
    [SerializeField] private AxisAlignment _localForwardAxis = AxisAlignment.zaxis;

    [Header("Game state management")] 
    [SerializeField] private List<GameState> _requiredStates = new List<GameState>();
    [SerializeField] private List<InventoryItem> _requiredItems = new List<InventoryItem>();

    [Header("Messages")] 
    [TextArea(3, 10)] [SerializeField] private string _lockedHintText = "Door: Locked";
    [TextArea(3, 10)] [SerializeField] private string _closedHintText = "Door: Press 'use' to open";
    [TextArea(3, 10)] [SerializeField] private string _openedHintText = "Door: Press 'use' to close";
    
    [Header("Door Transforms")]
    [Tooltip("A list of child transforms to animate")]
    [SerializeField] private List<DoorInfo> _doors = new List<DoorInfo>();

    [Header("Sounds")] 
    [SerializeField] private AudioCollection _doorSounds;
    [SerializeField] private PunchInPunchOutAudio _punchInPunchOutAudio;
    
    [Header("AI and Navigation")] 
    [SerializeField] private AIInteractiveDoorType _aiDoorType = AIInteractiveDoorType.AICanOpen;
    public InteractiveDoorStateChanged OnStateChangedEvent = new InteractiveDoorStateChanged();
    public InteractiveDoorPlayerInteraction OnPlayerInteractionEvent = new InteractiveDoorPlayerInteraction();
    
    // Private variables
    private bool _isClosed = true;
    private IEnumerator _coroutine = null;
    private Vector3 _closedColliderSize = Vector3.zero;
    private Vector3 _closedColliderCenter = Vector3.zero;
    private Vector3 _openColliderSize = Vector3.zero;
    private Vector3 _openColliderCenter = Vector3.zero;
    private BoxCollider _boxCollider;
    private Plane _plane;
    private bool _openedFrontSide;
    // door progress
    private float _normalizedTime = 0f;
    private ulong _oneShotSoundID = 0;
    private float _startAnimTime = 0f;
    private bool _isAnimating;
    
    // Properties
    public bool IsOpen => !_isClosed;
    public bool IsAutoOpen => _autoOpen;
    public AIInteractiveDoorType AIDoorType => _aiDoorType;
    public int AIid = -1;
    public bool IsAnimating => _isAnimating;

    public override string GetText(CharacterManager characterManager = null) {
        if (_disableActivation) return null;
        
        bool requiredStates = true;

        if (_requiredStates.Count > 0)
            requiredStates = ApplicationManager.instance.AreStatesSet(_requiredStates);

        if (_isClosed) {
            if (!HaveInventoryItems(characterManager.Inventory) || !requiredStates) 
                return _lockedHintText;

            return _closedHintText;
        }

        return _openedHintText;
    }

    protected override void Start() {
        base.Start();
        // cache components
        _boxCollider = _collider as BoxCollider;
        
        // calculate the open and closed collider size
        _closedColliderSize = _openColliderSize = _boxCollider.size;
        _closedColliderCenter = _openColliderCenter = _boxCollider.center;
        float offset = 0f;

        switch (_localForwardAxis) {
            case AxisAlignment.xaxis:
                _plane = new Plane(transform.right, transform.position);
                _openColliderSize.x *= _colliderLengthScale;
                offset = _closedColliderCenter.x - (_openColliderCenter.x / 2);
                _openColliderCenter = new Vector3(offset, _closedColliderCenter.y, _closedColliderCenter.z);
                break;
            case AxisAlignment.yaxis:
                _plane = new Plane(transform.up, transform.position);
                _openColliderSize.y *= _colliderLengthScale;
                offset = _closedColliderCenter.y - (_openColliderCenter.y / 2);
                _openColliderCenter = new Vector3(_closedColliderCenter.x, offset, _closedColliderCenter.z);
                break;
            case AxisAlignment.zaxis:
                _plane = new Plane(transform.forward, transform.position);
                _openColliderSize.z *= _colliderLengthScale;
                offset = _closedColliderCenter.z - (_openColliderCenter.z / 2);
                _openColliderCenter = new Vector3(_closedColliderCenter.x, _closedColliderCenter.y, offset);
                break;
        }
        
        // cache the door positions and rotations
        foreach (var door in _doors) {
            door.ClosedPosition = door.Transform.position;
            door.ClosedRotation = door.Transform.localRotation;
            door.OpenPosition = door.Transform.position - door.Transform.TransformDirection(door.Movement);
            door.OpenRotation = Quaternion.Euler(door.Rotation);
            
            door.NavObstacle = door.Transform.GetComponent<NavMeshObstacle>();
            door.NavObstacle.enabled = true;
            
            if (_aiDoorType == AIInteractiveDoorType.CarvedWhenClosed) {
                door.NavObstacle.enabled = true;
                door.NavObstacle.carving = true;
            }
        }

        OnStateChangedEvent.Invoke(false);
    }

    // Helper methods
    private void CarveDoors(bool carve) {
        foreach (var door in _doors) {
            door.NavObstacle.enabled = true;
            door.NavObstacle.carving = carve;
        }
    }

    public bool IsOnFrontSide(Vector3 pos) {
        return _plane.GetSide(pos);
    }

    public Vector3 GetLookDirection(Vector3 pos) {
        return IsOnFrontSide(pos) ? _plane.flipped.normal : _plane.normal;
    }

    public Transform GetIKTarget(Vector3 pos, int doorInstanceID) {
        foreach (var door in _doors) {
            if (door.Collider.GetInstanceID().Equals(doorInstanceID)) {
                return IsOnFrontSide(pos) ? door.FrontSideHandle : door.BackSideHandle;
            }
        }

        return null;
    }

    private IEnumerator DisableRepulseAIWhenClosed(float time) {
        yield return new WaitForSeconds(time);
        
        foreach (var door in _doors) {
            door.NavObstacle.enabled = false;
        }
    }

    private bool HaveInventoryItems(Inventory inventory) {
        foreach (var item in _requiredItems) {
            if (inventory.Search(item) == null) {
                return false;
            }
        }
        
        return true;
    }

    public override void Use(CharacterManager characterManager) {
        if (_disableActivation) return;
        
        bool requiredStates = true;
        if (_requiredStates.Count > 0)
            requiredStates = ApplicationManager.instance.AreStatesSet(_requiredStates);

        if (HaveInventoryItems(characterManager.Inventory) && requiredStates) {
            if (_coroutine != null) StopCoroutine(_coroutine);
            
            OnPlayerInteractionEvent.Invoke(characterManager, true, !_isClosed);
            
            _coroutine = Use(_plane.GetSide(characterManager.transform.position));
            StartCoroutine(_coroutine);
        }
        else {
            OnPlayerInteractionEvent.Invoke(characterManager, false, !_isClosed);
            // find a sound to play
            var clip = _doorSounds[_isSqueaky ? 2 : 1];
            AudioManager.instance.PlaySound(_doorSounds, clip, transform.position);
        }
    }

    public void Use(Vector3 position, bool skipStartTime) {
        if (_coroutine != null) 
            StopCoroutine(_coroutine);

        _coroutine = Use(IsOnFrontSide(position), false, 0f, skipStartTime);
        StartCoroutine(_coroutine);
    }

    private IEnumerator Use(bool frontSide, bool autoClosing = false, float delay = 0f, bool skipStartTime = false) {
        yield return new WaitForSeconds(delay);

        _isAnimating = true;
        AudioClip clip = null;
        
        // used to sync animation with sound
        float duration = 1f;
        float time = 0f;
        
        if (_normalizedTime > 0f)
            _normalizedTime = 1 - _normalizedTime;

        if (!_isTwoWay) frontSide = true;

        if (_isClosed) {
            
            if (_normalizedTime > 0)
                frontSide = _openedFrontSide;
            
            _openedFrontSide = frontSide;
            
            // if the door is squeaky
            if (_isSqueaky) {
                // find a sound to play
                // stop any previous sounds that might be playing
                AudioManager.instance.StopSound(_oneShotSoundID);
                clip = _doorSounds[0];
                duration = clip.length;
                var info = _punchInPunchOutAudio.GetClipInfo(clip);
                // adjust the animation duration based on the clip
                if (info != null) {
                    _startAnimTime = Mathf.Min(info.StartTime, clip.length);
                    if (info.EndTime == 0f) {
                        duration = clip.length - info.StartTime;
                    } else {
                        duration = info.EndTime - info.StartTime;
                    }
                }
            } else {
              // randomly choose opening/closing duration
              duration = Random.Range(0.5f, 1.0f);
            }
            
            // if the animation is already going, start the clip from somewhere in between
            float playbackOffset = 0f;
            if (_normalizedTime > 0f || skipStartTime) {
                playbackOffset = _startAnimTime + (duration * _normalizedTime);
                _startAnimTime = 0;
            }
            
            if (_isSqueaky)
                _oneShotSoundID = AudioManager.instance.PlaySound(_doorSounds, clip, transform.position, playbackOffset);
            
            float offset = 0f;
            switch (_localForwardAxis) {
                case AxisAlignment.xaxis:
                    offset = _openColliderSize.x / 2f;
                    if (!frontSide) offset = -offset;
                    _openColliderCenter = new Vector3(_closedColliderCenter.x - offset, _closedColliderCenter.y, _closedColliderCenter.z);
                    break;
                case AxisAlignment.yaxis:
                    offset = _openColliderSize.y / 2f;
                    if (!frontSide) offset = -offset;
                    _openColliderCenter = new Vector3(_closedColliderCenter.x , _closedColliderCenter.y - offset, _closedColliderCenter.z);
                    break;
                case AxisAlignment.zaxis:
                    offset = _openColliderSize.z / 2f;
                    if (!frontSide) offset = -offset;
                    _openColliderCenter = new Vector3(_closedColliderCenter.x , _closedColliderCenter.y, _closedColliderCenter.z - offset);
                    break;
            }

            if (_offsetCollider) _boxCollider.center = _openColliderCenter;
            _boxCollider.size = _openColliderSize;
            
            if (_startAnimTime > 0f)
                yield return new WaitForSeconds(_startAnimTime);
            
            // set the starting time of the animation
            time = duration * _normalizedTime;

            _isClosed = false;
            OnStateChangedEvent.Invoke(!_isClosed);
            CarveDoors(false);
            
            // animate each door
            while (time <= duration) {
                _normalizedTime = time / duration;
                foreach (var door in _doors) {
                    // calculate new position
                    door.Transform.position = Vector3.Lerp(door.ClosedPosition, door.OpenPosition, _normalizedTime);
                    // calculate new rotation
                    door.Transform.localRotation = door.ClosedRotation * Quaternion.Euler(frontSide ? door.Rotation * _normalizedTime : -door.Rotation * _normalizedTime);
                    if (!door.NavObstacle.enabled)
                        door.NavObstacle.enabled = true;
                }

                yield return null;
                time += Time.deltaTime;
            }
            
            CarveDoors(true);
            
            if (!_isSqueaky) {
                clip = _doorSounds[0];
                AudioManager.instance.PlaySound(_doorSounds, clip, transform.position);
            }

            _normalizedTime = 0f;
            if (_autoClose) {
                _coroutine = Use(frontSide, true, Random.Range(_autoCloseDelay.x, _autoCloseDelay.y));
                StartCoroutine(_coroutine);
            }

            _isAnimating = false;
            yield break;
        }
        else {
            // close the door
            _isClosed = true;

            foreach (var door in _doors) {
                Quaternion rotationToOpen = Quaternion.Euler(_openedFrontSide ? door.Rotation : -door.Rotation);
                door.OpenRotation = door.ClosedRotation * rotationToOpen;
            }

            time = duration * _normalizedTime;

            if (_isSqueaky) {
                // find a sound to play
                // stop any previous sounds that might be playing
                AudioManager.instance.StopSound(_oneShotSoundID);
                clip = _doorSounds[autoClosing ? 3 : 1];
                duration = clip.length;
                PunchInPunchOutAudioInfo info = _punchInPunchOutAudio.GetClipInfo(clip);
                // adjust the animation duration based on the clip
                if (info != null) {
                    _startAnimTime = Mathf.Min(info.StartTime, clip.length);
                    if (info.EndTime == 0f) {
                        duration = clip.length - info.StartTime;
                    }
                    else {
                        duration = info.EndTime - info.StartTime;
                    }
                }
            } else {
                // randomly choose opening/closing duration
                duration = Random.Range(0.5f, 1.0f);
            }
            
            // if the animation is already going, start the clip from somewhere in between
            float playbackOffset = 0f;
            if (_normalizedTime > 0f || skipStartTime) {
                playbackOffset = _startAnimTime + (duration * _normalizedTime);
                _startAnimTime = 0;
            }
            
            if (_isSqueaky) 
                _oneShotSoundID = AudioManager.instance.PlaySound(_doorSounds, clip, transform.position, playbackOffset);
            
            if (_startAnimTime > 0f)
                yield return new WaitForSeconds(_startAnimTime);
            
            CarveDoors(false);
            
            // close over time
            while (time < duration) {
                _normalizedTime = time / duration;
                foreach (var door in _doors) {
                    door.Transform.position = Vector3.Lerp(door.OpenPosition, door.ClosedPosition, _normalizedTime);
                    door.Transform.localRotation = Quaternion.Lerp(door.OpenRotation, door.ClosedRotation, _normalizedTime);


                    if (!door.NavObstacle.enabled)
                        door.NavObstacle.enabled = true;
                }

                yield return null;
                time += Time.deltaTime;
            }
            
            if (!_isSqueaky) {
                clip = _doorSounds[0];
                AudioManager.instance.PlaySound(_doorSounds, clip, transform.position);
            } 

            foreach (var door in _doors) {
                door.Transform.position = door.ClosedPosition;
                door.Transform.localRotation = door.ClosedRotation;
                
                if (_aiDoorType == AIInteractiveDoorType.CarvedWhenClosed)
                    CarveDoors(true);
                else
                    CarveDoors(false);
            }

            _boxCollider.size = _closedColliderSize;
            _boxCollider.center = _closedColliderCenter;
            
            OnStateChangedEvent.Invoke(!_isClosed);
        }

        _normalizedTime = 0f;
        _coroutine = null;
        _isAnimating = false;
    }

    // used only when the doors are configured to open automatically
    private void OnTriggerEnter(Collider other) {
        if (!_autoOpen || !_isClosed) return;
        
        bool requiredStates = true;
        bool requiredItems = false;
        if (_requiredStates.Count > 0)
            requiredStates = ApplicationManager.instance.AreStatesSet(_requiredStates);

        var playerInfo = _gameSceneManager.GetPlayerInfo(other.GetInstanceID());
        if (playerInfo != null) {
            requiredItems = HaveInventoryItems(playerInfo.CharacterManager.Inventory);
        }

        if (requiredItems && requiredStates) {
            if (_coroutine != null) StopCoroutine(_coroutine);
            _coroutine = Use(_plane.GetSide(other.transform.position));
            StartCoroutine(_coroutine);
        }
    }
}
