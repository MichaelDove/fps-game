﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveKeyboard : InteractiveItem {
    [SerializeField] private Transform _doors;
    [SerializeField] private AudioCollection _keyboardAudio;
    [SerializeField] private AudioCollection _doorAudio;
    [SerializeField] private int _bank;
    [SerializeField] private float _activationDelay = 1.0f;

    private bool _isActivated;
    private bool _typed;

    public override string GetText(CharacterManager characterManager = null) {
        ApplicationManager appMan = ApplicationManager.instance;
        string powerState = appMan.GetGameState("Power");
        string lockdownState = appMan.GetGameState("Lockdown");
        string passwordState = appMan.GetGameState("Password");
        string vaccineState = appMan.GetGameState("Vaccine");

        
        if (!powerState.Equals("true")) {
            return "Keyboard: there is no electricity";
        } 
        if (!lockdownState.Equals("false")) {
            return "Keyboard: lock down mode is enabled, you can't escape";
        }
        if (!passwordState.Equals("true")) {
            return "Keyboard: you don't know the password";
        }

        if (!vaccineState.Equals("true")) {
            return "Keyboard: find the vaccine!";
        }
        if (_typed) {
            return "Keyboard: password is correct";
        }
        
        return "Keyboard: enter the password";
    }

    public override void Use(CharacterManager characterManager) {
        if (_isActivated) return;
        
        ApplicationManager appMan = ApplicationManager.instance;
        string powerState = appMan.GetGameState("Power");
        string lockdownState = appMan.GetGameState("Lockdown");
        string passwordState = appMan.GetGameState("Password");
        string vaccineState = appMan.GetGameState("Vaccine");

        if (!powerState.Equals("true")) return; 
        if (!lockdownState.Equals("false")) return;
        if (!passwordState.Equals("true")) return;
        if (!vaccineState.Equals("true")) return;
        
        _isActivated = true;
        // start the activation (with delay)
        StartCoroutine(DelayedUsage(characterManager));
    }

    private IEnumerator DelayedUsage(CharacterManager cm) {
        // play the keypad sound
        AudioClip clip = _keyboardAudio[_bank];
        AudioManager.instance.PlaySound(_keyboardAudio.AudioGroup, clip, transform.position, _keyboardAudio.Volume, _keyboardAudio.SpatialBlend, _keyboardAudio.Priority);    
        
        // wait for desired amount
        yield return new WaitForSeconds(_activationDelay);
        
        Animator anim = _doors.GetComponent<Animator>();
        clip = _doorAudio[_bank];
        AudioManager.instance.PlaySound(_doorAudio.AudioGroup, clip, transform.position, _doorAudio.Volume, _doorAudio.SpatialBlend, _doorAudio.Priority);    
        _typed = true;
        anim.SetTrigger("Activate");
    }
}
