﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveKeycardReader : InteractiveSwitch {
    [Header("Keycard Terminal properties")] 
    [SerializeField] protected Renderer _powerLight;
    [SerializeField] protected Color _unPoweredColor = Color.black;
    [SerializeField] protected Color _lockedColor = Color.red;
    [SerializeField] protected Color _unlockedColor = Color.green;

    protected void OnEnable() {
        ApplicationManager.instance.OnStateChanged.AddListener(StateChange);
    }

    protected void OnDisable() {
        if (ApplicationManager.instance != null)
            ApplicationManager.instance.OnStateChanged.RemoveListener(StateChange);
    }

    protected override void Start() {
        base.Start();
        StateChange();
    }

    protected override void ProcessInventoryAfterActivation(CharacterManager characterManager = null) {
        characterManager.Inventory.Remove(_requiredItems[0]);
    }

    private void StateChange() {
        if (ApplicationManager.instance.AreStatesSet(_requiredStates)) {
            if (_activated) {
                _powerLight.material.SetColor("_EmissionColor", _unlockedColor);
            }
            else {
                _powerLight.material.SetColor("_EmissionColor", _lockedColor);
            }
        }
        else {
                _powerLight.material.SetColor("_EmissionColor", _unPoweredColor);
        }
    }
}
