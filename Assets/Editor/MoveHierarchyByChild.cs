﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MoveHierarchyByChild : EditorWindow {
    private static EditorWindow Window;
    private Transform _fromObject;
    private Transform _toObject;
    
    [MenuItem("GameObject/- Move Hierarchy By Child")]
    static void Init() {
        Window = EditorWindow.GetWindow<MoveHierarchyByChild>();
        Window.maxSize = new Vector2(350, 260);
        Window.minSize = new Vector2(350, 260);
        Window.titleContent = new GUIContent("Move Hierarchy By Child");
        Window.Show();
    }

    void OnGUI() {
        _fromObject = (Transform) EditorGUILayout.ObjectField("Child To Move", _fromObject, typeof(Transform), true);
        _toObject = (Transform) EditorGUILayout.ObjectField("Destination Transform", _toObject, typeof(Transform), true);

        if (_fromObject && _toObject) {
            if (GUILayout.Button("Perform Move Hierarchy", GUILayout.ExpandWidth(true))) {
                var targetPos = _toObject.position;
                var parentPos = _fromObject.root.position;
                var offset = targetPos - parentPos;

                parentPos += offset;
                _fromObject.root.position = parentPos;
            }
        }
    }
}
