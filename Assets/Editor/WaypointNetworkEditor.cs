﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.AI;

[CustomEditor(typeof(AIWaypointNetwork))]
public class WaypointNetworkEditor : Editor {
    private void OnSceneGUI() {
        AIWaypointNetwork network = (AIWaypointNetwork) target;

        for(int i = 0; i < network.Waypoints.Count; i++) {
            int next = i + 1 == network.Waypoints.Count ?  0 : i +1;
            if (network.Waypoints[i] != null && network.Waypoints[next] != null) {
                Handles.color = Color.cyan;
                Handles.Label(network.Waypoints[i].position, "Waypoint " + i); // draw labels bellow icons
                
                switch (network.DisplayMode) {
                    case PathDisplayMode.Connections:
                        Handles.DrawLine(network.Waypoints[i].position, network.Waypoints[next].position); // draw connections
                        break;
                    
                    case PathDisplayMode.Paths:
                        NavMeshPath path = new NavMeshPath();
                        NavMesh.CalculatePath(network.Waypoints[i].position, network.Waypoints[next].position, // calculate path for waypoints
                            NavMesh.AllAreas, path);
                        Handles.DrawPolyLine(path.corners); // draw lines between all corners
                        break;
                }
            }
        }
    }
}
