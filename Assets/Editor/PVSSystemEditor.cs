﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PVSSystem))]
public class PVSSystemEditor : Editor {
    public override void OnInspectorGUI() {
        PVSSystem script = target as PVSSystem;

        DrawDefaultInspector();

        if (GUI.changed && Application.isPlaying) {
            script.EnablePVS(script.PvsEnabled);
        }
    }
}
