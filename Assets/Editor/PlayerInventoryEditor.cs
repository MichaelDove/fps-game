﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

[CustomEditor(typeof(InventoryPlayer))]
public class PlayerInventoryEditor : Editor {
    public override void OnInspectorGUI() {
        var inventory = target as InventoryPlayer;
        
        serializedObject.Update();

        inventory.WeaponMountsExpanded = EditorGUILayout.Foldout(inventory.WeaponMountsExpanded, "Weapon Mounts");
        if (inventory.WeaponMountsExpanded)
            OutputWeaponMounts();
        
        EditorGUILayout.Separator();
        
        inventory.AmmoMountsExpanded = EditorGUILayout.Foldout(inventory.AmmoMountsExpanded, "Ammo Mounts");
        if (inventory.AmmoMountsExpanded)
            OutputAmmoMounts();
        
        EditorGUILayout.Separator();
        
        inventory.BackpackMountsExpanded = EditorGUILayout.Foldout(inventory.BackpackMountsExpanded, "Backpack Mounts");
        if (inventory.BackpackMountsExpanded)
            OutputBackpackMounts();
        
        EditorGUILayout.Separator();
        
        inventory.AudioLogsMountsExpanded = EditorGUILayout.Foldout(inventory.AudioLogsMountsExpanded, "Audio Recordings Mounts");
        if (inventory.AudioLogsMountsExpanded)
            OutputAudioLogMounts();
        
        EditorGUILayout.Separator();
        
        // Output shared variables
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_notificationQueue"));
        EditorGUILayout.Separator();
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_playerPosition"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("_playerDirection"));
        
        serializedObject.ApplyModifiedProperties();
        
        if (GUI.changed)
            EditorUtility.SetDirty(target);
    }

    private void OutputWeaponMounts() {
        List<WeaponMountInfo> mounts = (target as InventoryPlayer)._weaponMounts;

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("");
        if (mounts.Count > 1) {
            if (GUILayout.Button("-", GUILayout.Width(32), GUILayout.Height(32))) {
                Undo.RecordObject(target, "Remove Weapon Mount");
                mounts.RemoveAt(mounts.Count - 1);
            }
        }

        if (GUILayout.Button("+", GUILayout.Width(32), GUILayout.Height(32))) {
            Undo.RecordObject(target, "Add Weapon Mount");
            mounts.Add(new WeaponMountInfo());
        }
        EditorGUILayout.EndHorizontal();

        for (int i = 0; i < mounts.Count; i++) {
            InventoryItemWeapon newObject = null;
            EditorGUILayout.Separator();
            var mount = mounts[i];
            
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PrefixLabel("Mount " + (i+1));

            EditorGUI.indentLevel += 3;
            EditorGUILayout.BeginVertical();
            
            EditorGUILayout.BeginHorizontal();
            newObject = EditorGUILayout.ObjectField(mount.Weapon, typeof(InventoryItemWeapon), false) as InventoryItemWeapon;
            if (newObject)
                GUILayout.Label(AssetPreview.GetAssetPreview(newObject.InventoryImage), GUILayout.Width(100), GUILayout.Height(32));
            EditorGUILayout.EndHorizontal();

            if (EditorGUI.EndChangeCheck()) {
                if (newObject == null) {
                    Undo.RecordObject(target, "Clear Weapon Mount");
                    mount.Weapon = null;
                    mount.InGunAmmo = 0;
                }
                else {
                    Undo.RecordObject(target, "Assign Weapon Mount");
                    mount.Weapon = newObject;
                    mount.InGunAmmo = 0;
                }
            }

            if (newObject != null && mount.Weapon.AttackType == InventoryWeaponAttackType.Ammunition) {
                EditorGUI.BeginChangeCheck();

                EditorGUILayout.PrefixLabel("In Gun Rounds");
                EditorGUILayout.BeginHorizontal();

                int ammo = EditorGUILayout.IntSlider(mount.InGunAmmo, 0, mount.Weapon.AmmoCapacity);
                
                EditorGUILayout.EndHorizontal();
                if (EditorGUI.EndChangeCheck()) {
                    Undo.RecordObject(target, "Change Weapon Ammo");
                    mount.InGunAmmo = ammo;
                }
            }
            
            EditorGUILayout.EndVertical();
            EditorGUI.indentLevel -= 3;
        }
    }

    private void OutputAmmoMounts() {
        List<AmmoMountInfo> mounts = (target as InventoryPlayer)._ammoMounts;

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("");
        if (mounts.Count > 1) {
            if (GUILayout.Button("-", GUILayout.Width(32), GUILayout.Height(32))) {
                Undo.RecordObject(target, "Remove Ammo Mount");
                mounts.RemoveAt(mounts.Count - 1);
            }
        }

        if (GUILayout.Button("+", GUILayout.Width(32), GUILayout.Height(32))) {
            Undo.RecordObject(target, "Add Ammo Mount");
            mounts.Add(new AmmoMountInfo());
        }
        EditorGUILayout.EndHorizontal();

        for (int i = 0; i < mounts.Count; i++) {
            InventoryItemAmmo newObject = null;
            EditorGUILayout.Separator();
            var mount = mounts[i];

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PrefixLabel("Mount " + (i + 1));

            EditorGUI.indentLevel += 3;
            EditorGUILayout.BeginVertical();

            EditorGUILayout.BeginHorizontal();
            newObject = EditorGUILayout.ObjectField(mount.Ammo, typeof(InventoryItemAmmo), false) as InventoryItemAmmo;
            if (newObject)
                GUILayout.Label(AssetPreview.GetAssetPreview(newObject.InventoryImage), GUILayout.Width(64), GUILayout.Height(64));
            EditorGUILayout.EndHorizontal();

            if (EditorGUI.EndChangeCheck()) {
                if (newObject == null) {
                    Undo.RecordObject(target, "Clear Ammo Mount");
                    mount.Ammo = null;
                    mount.AmmoRounds = 0;
                }
                else {
                    Undo.RecordObject(target, "Assign Ammo Mount");
                    mount.Ammo = newObject;
                    mount.AmmoRounds = 0;
                }
            }

            if (newObject != null) {
                EditorGUI.BeginChangeCheck();

                EditorGUILayout.PrefixLabel("Rounds");
                EditorGUILayout.BeginHorizontal();

                int ammo = EditorGUILayout.IntSlider(mount.AmmoRounds, 0, mount.Ammo.Capacity);

                EditorGUILayout.EndHorizontal();
                if (EditorGUI.EndChangeCheck()) {
                    Undo.RecordObject(target, "Change Ammo Rounds");
                    mount.AmmoRounds = ammo;
                }
            }

            EditorGUILayout.EndVertical();
            EditorGUI.indentLevel -= 3;
        }
    }
    
    private void OutputBackpackMounts() {
        List<BackpackMountInfo> mounts = (target as InventoryPlayer)._backpackMounts;

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("");
        if (mounts.Count > 1) {
            if (GUILayout.Button("-", GUILayout.Width(32), GUILayout.Height(32))) {
                Undo.RecordObject(target, "Remove Backpack Mount");
                mounts.RemoveAt(mounts.Count - 1);
            }
        }

        if (GUILayout.Button("+", GUILayout.Width(32), GUILayout.Height(32))) {
            Undo.RecordObject(target, "Add Backpack Mount");
            mounts.Add(new BackpackMountInfo());
        }
        EditorGUILayout.EndHorizontal();

        for (int i = 0; i < mounts.Count; i++) {
            InventoryItem newObject = null;
            EditorGUILayout.Separator();
            var mount = mounts[i];

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PrefixLabel("Mount " + (i + 1));

            EditorGUI.indentLevel += 3;
            EditorGUILayout.BeginVertical();

            EditorGUILayout.BeginHorizontal();
            newObject = EditorGUILayout.ObjectField(mount.Item, typeof(InventoryItem), false) as InventoryItem;
            if (newObject)
                GUILayout.Label(AssetPreview.GetAssetPreview(newObject.InventoryImage), GUILayout.Width(64), GUILayout.Height(64));
            EditorGUILayout.EndHorizontal();

            if (EditorGUI.EndChangeCheck()) {
                if (newObject == null
                    || newObject is InventoryItemWeapon
                    || newObject is InventoryItemAmmo) {
                    Undo.RecordObject(target, "Clear Backpack Mount");
                    mount.Item = null;
                    newObject = null;
                }
                else {
                    Undo.RecordObject(target, "Assign Backpack Mount");
                    mount.Item = newObject;
                }

                // create context data
                CreateRemoveContextData(mount, newObject, newObject != null ? newObject.GetContextDataType() : null);
            }
            else {
                if (newObject != null) {
                    if (mount.Item.GetContextDataType() != null && string.IsNullOrEmpty(mount.ContextDataString)) {
                        CreateRemoveContextData(mount, newObject, newObject.GetContextDataType());
                    }
                }
                else {
                    mount.ContextData = null;
                    mount.ContextDataString = null;
                } 
            }

            if (newObject != null && !string.IsNullOrEmpty(mount.ContextDataString)) {
                RenderContextDataControls(mount, mount.Item.GetContextDataType());
            }

            EditorGUILayout.EndVertical();
            EditorGUI.indentLevel -= 3;
        }
    }

    private void CreateRemoveContextData(BackpackMountInfo mount, InventoryItem item, Type contextDataType = null) {
        if (item == null) {
            mount.ContextData = null;
            mount.ContextDataString = null;
            return;
        }

        if (contextDataType != null) {
            mount.ContextData = Activator.CreateInstance(contextDataType);
            mount.ContextDataString = JsonUtility.ToJson(mount.ContextData);
        }
        else {
            mount.ContextData = null;
            mount.ContextDataString = null;
        }
    }

    private void RenderContextDataControls(BackpackMountInfo mount, Type contextDataType) {
        mount.ContextData = JsonUtility.FromJson(mount.ContextDataString, contextDataType);
        FieldInfo[] fields = contextDataType.GetFields();
        
        EditorGUI.BeginChangeCheck();
        foreach (var info in fields) {
            DrawField(mount.ContextData, info);
        }

        if (EditorGUI.EndChangeCheck()) {
            mount.ContextDataString = JsonUtility.ToJson(mount.ContextData);
        }
    }

    private void DrawField(object data, FieldInfo info) {
        EditorGUILayout.BeginHorizontal();

        switch (info.FieldType.ToString()) {
            case "System.Single":
                EditorGUILayout.PrefixLabel(info.Name);
                float newfloat = EditorGUILayout.DelayedFloatField((float)info.GetValue(data));
                if (newfloat != (float) info.GetValue(data)) {
                    Undo.RecordObject(target, "Change " + info.Name);
                    info.SetValue(data, newfloat);
                }
                break;
            case "System.String":
                EditorGUILayout.PrefixLabel(info.Name);
                string newString = EditorGUILayout.DelayedTextField((string)info.GetValue(data));
                if (newString != (string)info.GetValue(data)) {
                    Undo.RecordObject(target, "Change " + info.Name);
                    info.SetValue(data, newString);
                }
                break;
            case "System.Boolean":
                EditorGUILayout.PrefixLabel(info.Name);
                bool newBool = EditorGUILayout.Toggle((bool)info.GetValue(data));
                if (newBool != (bool) info.GetValue(data)) {
                    Undo.RecordObject(target, "Change " + info.Name);
                    info.SetValue(data, newBool);
                }
                break;
            case "System.Int32":
                EditorGUILayout.PrefixLabel(info.Name);
                int newInt = EditorGUILayout.DelayedIntField((int)info.GetValue(data));
                if (newInt != (int) info.GetValue(data)) {
                    Undo.RecordObject(target, "Change " + info.Name);
                    info.SetValue(data, newInt);
                }
                break;
        }

        EditorGUILayout.EndHorizontal();
    }
    
    private void OutputAudioLogMounts() {
        List<InventoryItemAudio> mounts = (target as InventoryPlayer)._audioRecordings;

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("");
        if (mounts.Count > 1) {
            if (GUILayout.Button("-", GUILayout.Width(32), GUILayout.Height(32))) {
                Undo.RecordObject(target, "Remove Audio Slot");
                mounts.RemoveAt(mounts.Count - 1);
            }
        }

        if (GUILayout.Button("+", GUILayout.Width(32), GUILayout.Height(32))) {
            Undo.RecordObject(target, "Add Audio Slot");
            mounts.Add(null);
        }
        EditorGUILayout.EndHorizontal();

        for (int i = 0; i < mounts.Count; i++) {
            InventoryItemAudio newObject = null;
            EditorGUILayout.Separator();

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PrefixLabel("Mount " + (i + 1));

            EditorGUI.indentLevel += 3;
            EditorGUILayout.BeginVertical();

            EditorGUILayout.BeginHorizontal();
            newObject = EditorGUILayout.ObjectField(mounts[i], typeof(InventoryItemAudio), false) as InventoryItemAudio;
            if (newObject)
                GUILayout.Label(AssetPreview.GetAssetPreview(newObject.Image), GUILayout.Width(64), GUILayout.Height(64));
            EditorGUILayout.EndHorizontal();

            if (EditorGUI.EndChangeCheck()) {
                if (newObject == null) {
                    Undo.RecordObject(target, "Clear Audio Recording");
                    mounts[i] = null;
                }
                else {
                    Undo.RecordObject(target, "Assign Audio Recording");
                    mounts[i] = newObject;
                }
            }

            EditorGUILayout.EndVertical();
            EditorGUI.indentLevel -= 3;
        }
    }
    
}
