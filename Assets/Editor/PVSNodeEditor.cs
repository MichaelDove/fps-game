﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PVSNode))]
public class PVSNodeEditor : Editor {
    public override void OnInspectorGUI() {
        PVSNode script = target as PVSNode;

        DrawDefaultInspector();

        if (GUI.changed && !Application.isPlaying) {
            script.ShowObjects(script.EditorVisibility);
        }
    }
}
